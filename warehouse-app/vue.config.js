const webpack = require('webpack')
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' 
  ? process.env.PROJECT_NAME 
  : '/',
  chainWebpack: config => {
    config
        .plugin('html')
        .tap(args => {
            args[0].title = "SWIS APP";
            return args;
        })
},
    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
              $: 'jquery',
              jquery: 'jquery',
              'window.jQuery': 'jquery',
              jQuery: 'jquery',
            })
          ]
    },
    css: {
      loaderOptions: {
        sass: {
          additionalData : `
                        @import "@/assets/sass/_variables.scss";
                        @import "~handy.sass.mixins/scss/shared.scss";
                      `,
        }
      }
    },

    devServer: {
      proxy: {
        '/api': {
          target: 'http://swis.sarc-sy.rog:6060/',
          changeOrigin: true
      }
    }
  }
  
}

