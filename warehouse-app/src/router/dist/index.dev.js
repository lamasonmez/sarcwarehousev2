"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vueRouter = require("vue-router");

var _auth = _interopRequireDefault(require("@/router/modules/auth.js"));

var _home = _interopRequireDefault(require("@/router/modules/home.js"));

var _transactions = _interopRequireDefault(require("@/router/modules/transactions.js"));

var _store = _interopRequireDefault(require("@/store"));

var _jwt = _interopRequireDefault(require("@/services/jwt.service"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var history = (0, _vueRouter.createWebHistory)();
var all_routes = [];
var routes = all_routes.concat(_home["default"], _auth["default"], _transactions["default"]);
var router = (0, _vueRouter.createRouter)({
  history: history,
  routes: routes,
  scrollBehavior: function scrollBehavior(to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    };
  }
});
router.beforeEach(function (to, from, next) {
  if (to.matched.some(function (record) {
    return record.meta.requiresAuth;
  })) {
    _jwt["default"].getToken();

    if (_jwt["default"].getToken() == null || undefined) {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});
var _default = router;
exports["default"] = _default;