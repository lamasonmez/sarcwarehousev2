import InTransactionsPage from '@/views/transactions/transaction_ins/TransactionIns.vue';
import OutTransactionsPage from '@/views/transactions/transaction_outs/TransactionOuts.vue';
import TransactionInAddEdit from '@/views/transactions/transaction_in_addedit/TransactionInAddEdit.vue';
import TransactionOutAddEdit from '@/views/transactions/transaction_out_addedit/TransactionOutAddEdit.vue';

const routes = [
    {
        path:'/income',
        name:'Income',
        component:InTransactionsPage,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/outcome',
        name:'Outcome',
        component:OutTransactionsPage,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/income/addedit/:id?',
        name:'AddEditIncome',
        component:TransactionInAddEdit,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/outcome/addedit/:id?',
        name:'AddEditOutCome',
        component:TransactionOutAddEdit,
        meta: {
            requiresAuth: true
        }
    }
   
]
export default routes;