import UsersIndex from '@/views/users/users_index/UsersIndex.vue';
import UserAddEdit from '@/views/users/user_addedit/UserAddEdit.vue';

const routes = [
    {
        path:'/users',
        name:'UsersIndex',
        component:UsersIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/users/addedit/:id?',
        name:'UserAddEdit',
        component:UserAddEdit,
        meta: {
            requiresAuth: true
        }
    },
    
 
]
export default routes;