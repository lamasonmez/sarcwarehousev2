import BranchesIndex from '@/views/branches/branches_index/BranchesIndex.vue';

const routes = [
    {
        path:'/branches',
        name:'BranchesIndex',
        component:BranchesIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/branches/addedit/:id?',
        name:'BranchesAddEdit',
        component:BranchesIndex,
        meta: {
            requiresAuth: true
        }
    },
 
]
export default routes;