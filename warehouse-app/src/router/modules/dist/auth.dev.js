"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Login = _interopRequireDefault(require("@/views/auth/login/Login.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var routes = [{
  path: '/login',
  name: 'login',
  component: _Login["default"]
}];
var _default = routes;
exports["default"] = _default;