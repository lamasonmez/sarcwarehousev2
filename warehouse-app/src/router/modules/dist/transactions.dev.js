"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _TransactionIns = _interopRequireDefault(require("@/views/transactions/transaction_ins/TransactionIns.vue"));

var _TransactionOuts = _interopRequireDefault(require("@/views/transactions/transaction_outs/TransactionOuts.vue"));

var _TransactionInAddEdit = _interopRequireDefault(require("@/views/transactions/transaction_in_addedit/TransactionInAddEdit.vue"));

var _TransactionOutAddEdit = _interopRequireDefault(require("@/views/transactions/transaction_out_addedit/TransactionOutAddEdit.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var routes = [{
  path: '/income',
  name: 'Income',
  component: _TransactionIns["default"],
  meta: {
    requiresAuth: true
  }
}, {
  path: '/outcome',
  name: 'Outcome',
  component: _TransactionOuts["default"],
  meta: {
    requiresAuth: true
  }
}, {
  path: '/income/addedit/:id?',
  name: 'AddEditIncome',
  component: _TransactionInAddEdit["default"],
  meta: {
    requiresAuth: true
  }
}, {
  path: '/outcome/addedit/:id?',
  name: 'AddEditOutCome',
  component: _TransactionOutAddEdit["default"],
  meta: {
    requiresAuth: true
  }
}];
var _default = routes;
exports["default"] = _default;