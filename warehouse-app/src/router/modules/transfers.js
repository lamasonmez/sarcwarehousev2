import TransferInPage from '@/views/transfers/transfer_in/TransferIn.vue';

const routes = [
    {
        path:'/transfer-in/addedit/:id',
        name:'TransferInAddEdit',
        component:TransferInPage,
        meta: {
            requiresAuth: true
        }
    },
    
]
export default routes;