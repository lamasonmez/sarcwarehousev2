import DonorsIndex from '@/views/donors/donors_index/DonorsIndex.vue';
import WarehouseDonors from '@/views/donors/warehouse_donors/warehouse_donors.vue';

const routes = [
    {
        path:'/donors',
        name:'DonorsIndex',
        component:DonorsIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/donors/addedit/:id?',
        name:'DonorsAddEdit',
        component:DonorsIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/donor/warehouse_donors',
        name:'warehouse_donors',
        component:WarehouseDonors,
        meta: {
            requiresAuth: true
        }
    },
 
]
export default routes;