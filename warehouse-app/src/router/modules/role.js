import RolesIndex from '@/views/roles/roles_index/RolesIndex.vue';

const routes = [
    {
        path:'/roles',
        name:'RolesIndex',
        component:RolesIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/units/addedit/:id?',
        name:'RolesAddEdit',
        component:RolesIndex,
        meta: {
            requiresAuth: true
        }
    },
 
]
export default routes;