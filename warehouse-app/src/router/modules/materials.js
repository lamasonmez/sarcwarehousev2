import ViewMaterialPage from '@/views/materials/view-material/ViewMaterial.vue';
import MaterilIndexPage from '@/views/materials/material-index/MaterialIndex.vue';
import MaterilAddEditPage from '@/views/materials/material-addedit/MaterialAddEdit.vue';

const routes = [
    {
        path:'/view/material/:transaction_detail_id',
        name:'ViewMaterial',
        component:ViewMaterialPage,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/materials',
        name:'MaterialIndex',
        component:MaterilIndexPage,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/materials/addedit/:id?',
        name:'MaterialAddEdit',
        component:MaterilAddEditPage,
        meta: {
            requiresAuth: true
        }
    }
   
]
export default routes;