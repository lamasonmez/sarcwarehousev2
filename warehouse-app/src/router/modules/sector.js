import SectorsIndex from '@/views/sectors/sectors_index/SectorsIndex.vue';

const routes = [
    {
        path:'/sectors',
        name:'SectorsIndex',
        component:SectorsIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/sectors/addedit/:id?',
        name:'SectorsAddEdit',
        component:SectorsIndex,
        meta: {
            requiresAuth: true
        }
    },
 
]
export default routes;