import SubBranchesIndex from '@/views/sub_branches/subbranches_index/SubBranchesIndex.vue';

const routes = [
    {
        path:'/sub-branches',
        name:'SubBranchesIndex',
        component:SubBranchesIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/sub-branches/addedit/:id?',
        name:'SubBranchesAddEdit',
        component:SubBranchesIndex,
        meta: {
            requiresAuth: true
        }
    },
 
]
export default routes;