import UnitsIndex from '@/views/units/units_index/UnitsIndex.vue';

const routes = [
    {
        path:'/units',
        name:'UnitsIndex',
        component:UnitsIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/units/addedit/:id?',
        name:'UnitsAddEdit',
        component:UnitsIndex,
        meta: {
            requiresAuth: true
        }
    },
 
]
export default routes;