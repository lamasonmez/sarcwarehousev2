import MinimQuantityIndex from '@/views/minimum_quantity/minimum_quantity_index/MinimumQuantityIndex.vue';

const routes = [
    {
        path:'/minimum_quantity',
        name:'MinimQuantityIndex',
        component:MinimQuantityIndex,
        meta: {
            requiresAuth: true
        }
    },
    {
        path:'/minimum_quantity/addedit/:id?',
        name:'MinimQuantityAddEdit',
        component:MinimQuantityIndex,
        meta: {
            requiresAuth: true
        }
    },
 
]
export default routes;