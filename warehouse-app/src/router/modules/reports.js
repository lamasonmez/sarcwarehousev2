import WarehouseReports from '@/views/reports/warehouse_reports/WarehouseReports'
const routes = [
    {
        path:'/warehouse-reports',
        name:'WarehouseReports',
        component:WarehouseReports,
        meta: {
            requiresAuth: true
        }
    },
 
]
export default routes;