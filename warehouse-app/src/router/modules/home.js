import Home from '@/views/home/Home.vue';

const routes =[
    {
        path:'/',
        name:'home',
        component:Home,
        meta: {
            requiresAuth: true
        }
    }
]
export default  routes;