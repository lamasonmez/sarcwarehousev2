import { createRouter, createWebHistory } from 'vue-router'

import authRoutes from '@/router/modules/auth.js';
import HomeRoute from '@/router/modules/home.js';
import Transactions from '@/router/modules/transactions.js';
import Materials from '@/router/modules/materials.js';
import MinimumQuantites from '@/router/modules/minimum_quantity.js';
import ReportsRoutes from '@/router/modules/reports.js';
import MaterialRequestRoutes from '@/router/modules/material_request.js';
import TransfersRoutes from '@/router/modules/transfers.js';
import UnitsRoutes from '@/router/modules/unit.js';
import DonorsRoutes from '@/router/modules/donor.js';
import SectorsRoutes from '@/router/modules/sector.js';
import BranchesRoutes from '@/router/modules/branch.js';
import SubBranchesRoutes from '@/router/modules/sub_branch.js';
import RolesRoutes from '@/router/modules/role.js';
import UsersRoutes from '@/router/modules/user.js';
import JwtService from "@/services/jwt.service"

const history = createWebHistory()

let all_routes = [];
const routes = all_routes.concat(
    HomeRoute,
    authRoutes,
    Transactions,
    Materials,
    MinimumQuantites,
    ReportsRoutes,
    MaterialRequestRoutes,
    TransfersRoutes,
    UnitsRoutes,
    SectorsRoutes,
    DonorsRoutes,
    BranchesRoutes,
    SubBranchesRoutes,
    RolesRoutes,
    UsersRoutes
)
const router = createRouter({
    history,
    routes: routes,
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})

router.beforeEach((to, from, next) =>{
    if (to.matched.some(record => record.meta.requiresAuth)) {
        JwtService.getToken()
        if (JwtService.getToken() == null || undefined) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } 
    else {
        next() 
    }
});



export default router;