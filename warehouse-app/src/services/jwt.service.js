const ACCESS_TOKEN = 'ACCESS_TOKEN';
const TOKEN_TYPE = 'TOKEN_TYPE';
const EXPIRES_IN = 'EXPIRES_IN';
const REFRESH_TOKEN = "REFRESH_TOKEN"

export const getToken = () => {
    return window.localStorage.getItem(ACCESS_TOKEN)
};

export const setToken = token => {
    window.localStorage.setItem(ACCESS_TOKEN, token.access_token);
    window.localStorage.setItem(TOKEN_TYPE, token.token_type);
    window.localStorage.setItem(EXPIRES_IN, token.expires_in);
    window.localStorage.setItem(REFRESH_TOKEN, token.refresh_token);
};

export const unsetToken = () => {
    window.localStorage.removeItem(ACCESS_TOKEN);
    window.localStorage.removeItem(TOKEN_TYPE);
    window.localStorage.removeItem(EXPIRES_IN);
    window.localStorage.removeItem(REFRESH_TOKEN);
};

export default {getToken, setToken, unsetToken}