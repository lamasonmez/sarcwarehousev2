import {createStore} from 'vuex';

import auth from './modules/auth';
import statistics from './modules/statistics';
import inventory from './modules/inventory';
import transactions from './modules/transactions';
import warehouse from './modules/warehouse';
import donor from './modules/donors';
import donor_material from './modules/donor_material';
import unit from './modules/unit';
import ctn from './modules/ctn';
import minimum_quantity from './modules/minimum_quantity';
import Branches from './modules/branches';
import SubBranches from './modules/subbranches';
import Transfers from './modules/transfers';
import Sectors from './modules/sector';
import Materials from './modules/material';
import MaterialRequests from './modules/material_request';
import Notifications from './modules/notification';
import Roles from './modules/role';
import Users from './modules/user';

import createPersistedState from 'vuex-persistedstate';

import _ from 'lodash';

const modules = {
    auth,
    statistics,
    inventory,
    transactions,
    warehouse,
    donor,
    donor_material,
    unit,
    ctn,
    minimum_quantity,
    Branches,
    SubBranches,
    Transfers,
    Sectors,
    Materials,
    MaterialRequests,
    Notifications,
    Roles,
    Users
};

export default createStore({
    mutations: {
        resetState(state) {
            _.forOwn(modules, (value, key) => {
                state[key] = _.cloneDeep(value.state);
            });
        },
    },
    modules,
    plugins: [
        createPersistedState({
            paths: ['auth'],
        }),
    ]
});
