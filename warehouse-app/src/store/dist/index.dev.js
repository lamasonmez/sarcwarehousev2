"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vuex = require("vuex");

var _auth = _interopRequireDefault(require("./modules/auth"));

var _statistics = _interopRequireDefault(require("./modules/statistics"));

var _inventory = _interopRequireDefault(require("./modules/inventory"));

var _transactions = _interopRequireDefault(require("./modules/transactions"));

var _warehouse = _interopRequireDefault(require("./modules/warehouse"));

var _donors = _interopRequireDefault(require("./modules/donors"));

var _donor_material = _interopRequireDefault(require("./modules/donor_material"));

var _unit = _interopRequireDefault(require("./modules/unit"));

var _ctn = _interopRequireDefault(require("./modules/ctn"));

var _vuexPersistedstate = _interopRequireDefault(require("vuex-persistedstate"));

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var modules = {
  auth: _auth["default"],
  statistics: _statistics["default"],
  inventory: _inventory["default"],
  transactions: _transactions["default"],
  warehouse: _warehouse["default"],
  donor: _donors["default"],
  donor_material: _donor_material["default"],
  unit: _unit["default"],
  ctn: _ctn["default"]
};

var _default = (0, _vuex.createStore)({
  mutations: {
    resetState: function resetState(state) {
      _lodash["default"].forOwn(modules, function (value, key) {
        state[key] = _lodash["default"].cloneDeep(value.state);
      });
    }
  },
  modules: modules,
  plugins: [(0, _vuexPersistedstate["default"])({
    paths: ['auth']
  })]
});

exports["default"] = _default;