import auth from "../modules/auth";
import authConstants from  '@/store/modules/auth/const';
import statisticsConstants from  '@/store/modules/statistics/const';
import inventorysConstants from  '@/store/modules/inventory/const';
import transactionsConstants from  '@/store/modules/transactions/const';
import warehouseConstants from  '@/store/modules/warehouse/const';
import donorConstants from  '@/store/modules/donors/const';
import donorMaterialConstants from '@/store/modules/donor_material/const';
import unitConstants from '@/store/modules/unit/const';
import ctnConstants from '@/store/modules/ctn/const';
import MinimumQuantityConstants from '@/store/modules/minimum_quantity/const';
import BranchesConstants from '@/store/modules/branches/const';
import SubBranchesConstants from '@/store/modules/subbranches/const';
import TransfersConstants from '@/store/modules/transfers/const';
import sectorConstants from '@/store/modules/sector/const';
import materialConstants from '@/store/modules/material/const';
import materialRequestConstants from '@/store/modules/material_request/const';
import notificationsConstants from '@/store/modules/notification/const';
import UnitsConstants from '@/store/modules/unit/const';
import RolesConstants from '@/store/modules/role/const';
import UsersConstants from '@/store/modules/user/const';


const constants = {
    authConstants,
    statisticsConstants,
    inventorysConstants,
    transactionsConstants,
    warehouseConstants,
    donorConstants,
    donorMaterialConstants,
    unitConstants,
    ctnConstants,
    MinimumQuantityConstants,
    BranchesConstants,
    SubBranchesConstants,
    TransfersConstants,
    sectorConstants,
    materialConstants,
    materialRequestConstants,
    notificationsConstants,
    UnitsConstants,
    RolesConstants,
    UsersConstants
}

export default constants;
