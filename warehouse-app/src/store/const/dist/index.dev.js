"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _auth = _interopRequireDefault(require("../modules/auth"));

var _const = _interopRequireDefault(require("@/store/modules/auth/const"));

var _const2 = _interopRequireDefault(require("@/store/modules/statistics/const"));

var _const3 = _interopRequireDefault(require("@/store/modules/inventory/const"));

var _const4 = _interopRequireDefault(require("@/store/modules/transactions/const"));

var _const5 = _interopRequireDefault(require("@/store/modules/warehouse/const"));

var _const6 = _interopRequireDefault(require("@/store/modules/donors/const"));

var _const7 = _interopRequireDefault(require("@/store/modules/donor_material/const"));

var _const8 = _interopRequireDefault(require("@/store/modules/unit/const"));

var _const9 = _interopRequireDefault(require("@/store/modules/ctn/const"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var constants = {
  authConstants: _const["default"],
  statisticsConstants: _const2["default"],
  inventorysConstants: _const3["default"],
  transactionsConstants: _const4["default"],
  warehouseConstants: _const5["default"],
  donorConstants: _const6["default"],
  donorMaterialConstants: _const7["default"],
  unitConstants: _const8["default"],
  ctnConstants: _const9["default"]
};
var _default = constants;
exports["default"] = _default;