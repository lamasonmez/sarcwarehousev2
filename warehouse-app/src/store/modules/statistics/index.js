import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"
import {VUE_APP_API_URL} from "@/config";
import constants from  './const';

const state = {
   donor_materials_count:null,
   total_in_count:null,
   total_out_count:null
};

const getters = {
    [constants.GET_DONOR_MATERIAL_COUNT] : state => {
        return state.donor_materials_count;
    },
    [constants.GET_TOTAL_IN_COUNT] : state => {
        return state.total_in_count;
    },
    [constants.GET_TOTAL_OUT_COUNT] : state => {
        return state.total_out_count;
    },
    
   
};

const mutations = {
    [constants.SET_DONOR_MATERIAL_COUNT] (state,data) {
        state.donor_materials_count = data.value;
    },
    [constants.SET_TOTAL_IN_COUNT] (state,data) {
        state.total_in_count = data.value;
    },
    [constants.SET_TOTAL_OUT_COUNT] (state,data) {
        state.total_out_count = data.value;
    },
};

const actions = {
    [constants.FETCH_DONOR_MATERIAL_COUNT](context) {
        return new Promise((resolve, reject) => {
            //ApiService.setHeader();
            ApiService.get("api/statistics/materials/count")
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.SET_DONOR_MATERIAL_COUNT,{value:data[0].material_count});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_TOTAL_IN_COUNT](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/statistics/total_in/count")
                .then(({data}) => {
                    context.commit(constants.SET_TOTAL_IN_COUNT,{value:data[0].in_count});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_TOTAL_OUT_COUNT](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/statistics/total_out/count")
                .then(({data}) => {
                    context.commit(constants.SET_TOTAL_OUT_COUNT,{value:data[0].out_count});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
