export default {
/** Mutations */
SET_DONOR_MATERIAL_COUNT : "setDonorMaterialsCount",
SET_TOTAL_IN_COUNT : "setTotalInCount",
SET_TOTAL_OUT_COUNT : "setTotalOutCount",

/** Getters */
GET_DONOR_MATERIAL_COUNT : "getDonorMaterialsCount",
GET_TOTAL_IN_COUNT : "getTotalInCount",
GET_TOTAL_OUT_COUNT : "getTotalOutCount",

/** Actions */
FETCH_DONOR_MATERIAL_COUNT:"fetchDonorMaterialsCount",
FETCH_TOTAL_IN_COUNT:"fetchTotalInCount",
FETCH_TOTAL_OUT_COUNT:"fetchTotalOutCount",
}