"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _api = _interopRequireDefault(require("@/api/api.service"));

var _jwt = _interopRequireDefault(require("@/services/jwt.service"));

var _const = _interopRequireDefault(require("./const"));

var _getters, _mutations, _actions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var state = {
  donor_materials_count: null,
  total_in_count: null,
  total_out_count: null
};
var getters = (_getters = {}, _defineProperty(_getters, _const["default"].GET_DONOR_MATERIAL_COUNT, function (state) {
  return state.donor_materials_count;
}), _defineProperty(_getters, _const["default"].GET_TOTAL_IN_COUNT, function (state) {
  return state.total_in_count;
}), _defineProperty(_getters, _const["default"].GET_TOTAL_OUT_COUNT, function (state) {
  return state.total_out_count;
}), _getters);
var mutations = (_mutations = {}, _defineProperty(_mutations, _const["default"].SET_DONOR_MATERIAL_COUNT, function (state, data) {
  state.donor_materials_count = data.value;
}), _defineProperty(_mutations, _const["default"].SET_TOTAL_IN_COUNT, function (state, data) {
  state.total_in_count = data.value;
}), _defineProperty(_mutations, _const["default"].SET_TOTAL_OUT_COUNT, function (state, data) {
  state.total_out_count = data.value;
}), _mutations);
var actions = (_actions = {}, _defineProperty(_actions, _const["default"].FETCH_DONOR_MATERIAL_COUNT, function (context) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/statistics/materials/count").then(function (_ref) {
      var data = _ref.data;
      console.log('data', data);
      context.commit(_const["default"].SET_DONOR_MATERIAL_COUNT, {
        value: data[0].material_count
      });
      resolve(data);
    })["catch"](function (_ref2) {
      var response = _ref2.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].FETCH_TOTAL_IN_COUNT, function (context) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/statistics/total_in/count").then(function (_ref3) {
      var data = _ref3.data;
      context.commit(_const["default"].SET_TOTAL_IN_COUNT, {
        value: data[0].in_count
      });
      resolve(data);
    })["catch"](function (_ref4) {
      var response = _ref4.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].FETCH_TOTAL_OUT_COUNT, function (context) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/statistics/total_out/count").then(function (_ref5) {
      var data = _ref5.data;
      context.commit(_const["default"].SET_TOTAL_OUT_COUNT, {
        value: data[0].out_count
      });
      resolve(data);
    })["catch"](function (_ref6) {
      var response = _ref6.response;
      console.log(response);
      reject(response);
    });
  });
}), _actions);
var _default = {
  getters: getters,
  actions: actions,
  mutations: mutations,
  state: state
};
exports["default"] = _default;