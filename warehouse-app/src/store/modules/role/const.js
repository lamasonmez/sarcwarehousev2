export default {
    /** Mutations */
        SET_ROLES_DATA : "setRolesData",
        SET_ROLE_ITEM : "setRoleItem",
        CLEAR_ROLE_ITEM :"clearRoleItem",
    /** Getters */
        GET_ROLES_DATA : "getRolesData",
        GET_ROLE_ITEM : "getRoleItem",
    /** Actions */
        FETCH_ROLES_DATA : "fetchRolesData",
        DELETE_ROLE_ITEM :"deleteRoleItem",
        STORE_ROLE_ITEM: "storeRoleItem",
    }
    