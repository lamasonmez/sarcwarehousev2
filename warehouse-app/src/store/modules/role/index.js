import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"

import constants from  './const';

const state = {
   roles:null,
   role_item:{
    id:null,
    name:null,
}
};

const getters = {
    [constants.GET_ROLES_DATA] : state => {
        return state.roles;
    },
    [constants.GET_ROLE_ITEM] : state => {
        return state.role_item;
    },
};

const mutations = {
    [constants.SET_ROLES_DATA] (state,data) {
        state.roles = data.value;
    },
    [constants.SET_ROLE_ITEM] (state,data) {
        state.role_item = data.value;
    },
    [constants.CLEAR_ROLE_ITEM] (state) {
        state.role_item = {
            id:null,
            name:null,
        };
    },
};

const actions = {
    [constants.FETCH_ROLES_DATA](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/roles")
                .then(({data}) => {
                    context.commit(constants.SET_ROLES_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_ROLE_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/roles/store",data)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_ROLE_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_ROLE_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/roles/delete/"+data.id)
                .then(({data}) => {
                    context.commit(constants.CLEAR_ROLE_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
