import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"

import constants from  './const';

const state = {
   ctn_data:null,
};

const getters = {
    [constants.GET_CTN_DATA] : state => {
        return state.ctn_data;
    },
};

const mutations = {
    [constants.SET_CTN_DATA] (state,data) {
        state.ctn_data = data.value;
    },
};

const actions = {
    [constants.FETCH_CTN_DATA](context,donor_material_id=null) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/ctns/"+donor_material_id)
                .then(({data}) => {
                    context.commit(constants.SET_CTN_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
