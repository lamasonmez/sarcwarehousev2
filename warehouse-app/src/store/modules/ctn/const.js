export default {
    /** Mutations */
        SET_CTN_DATA : "setCTNData",
    /** Getters */
        GET_CTN_DATA : "getCTNData",
    /** Actions */
        FETCH_CTN_DATA : "fetchCTNData",
    }
    