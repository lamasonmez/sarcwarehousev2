"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _api = _interopRequireDefault(require("@/api/api.service"));

var _jwt = _interopRequireDefault(require("@/services/jwt.service"));

var _const = _interopRequireDefault(require("./const"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var state = {
  ctn_data: null
};

var getters = _defineProperty({}, _const["default"].GET_CTN_DATA, function (state) {
  return state.ctn_data;
});

var mutations = _defineProperty({}, _const["default"].SET_CTN_DATA, function (state, data) {
  state.ctn_data = data.value;
});

var actions = _defineProperty({}, _const["default"].FETCH_CTN_DATA, function (context) {
  var donor_material_id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/ctns/" + donor_material_id).then(function (_ref) {
      var data = _ref.data;
      context.commit(_const["default"].SET_CTN_DATA, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref2) {
      var response = _ref2.response;
      console.log(response);
      reject(response);
    });
  });
});

var _default = {
  getters: getters,
  actions: actions,
  mutations: mutations,
  state: state
};
exports["default"] = _default;