"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  /** Mutations */
  SET_CTN_DATA: "setCTNData",

  /** Getters */
  GET_CTN_DATA: "getCTNData",

  /** Actions */
  FETCH_CTN_DATA: "fetchCTNData"
};
exports["default"] = _default;