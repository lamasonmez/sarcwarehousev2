export default {
    /** Mutations */
        SET_BRANCHES_DATA : "setBranchesData",
        SET_BRANCH_ITEM : "setBranchItem",
        CLEAR_BRANCH_ITEM :"clearBranchItem",
    /** Getters */
        GET_BRANCHES_DATA : "getBranchesData",
        GET_BRANCHES_ITEM : "getBranchesItem",
    /** Actions */
        FETCH_BRANCHES_DATA : "fetchBranchesData",
        DELETE_BRANCH_ITEM :"deleteBranchItem",
        STORE_BRANCH_ITEM: "storeBranchItem",
    }
    