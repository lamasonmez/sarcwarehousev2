import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"

import constants from  './const';

const state = {
   branches_data:null,
   branch_item:{
    id:null,
    name:{
        ar:null,
        en:null
    }   
}
};

const getters = {
    [constants.GET_BRANCHES_DATA] : state => {
        return state.branches_data;
    },
    [constants.GET_BRANCHE_ITEM] : state => {
        return state.branch_item;
    },
};

const mutations = {
    [constants.SET_BRANCHES_DATA] (state,data) {
        state.branches_data = data.value;
    },
    [constants.SET_BRANCH_ITEM] (state,data) {
        state.branch_item = data.value;
    },
    [constants.CLEAR_BRANCH_ITEM] (state) {
        state.branch_item = {
            id:null,
            name:{
                ar:null,
                en:null
            }
        };
    },
};

const actions = {
    [constants.FETCH_BRANCHES_DATA](context,filters) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/branches")
                .then(({data}) => {
                    context.commit(constants.SET_BRANCHES_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_BRANCH_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/branches/store",data)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_BRANCH_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_BRANCH_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/branches/delete/"+data.id)
                .then(({data}) => {
                    context.commit(constants.CLEAR_BRANCH_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
