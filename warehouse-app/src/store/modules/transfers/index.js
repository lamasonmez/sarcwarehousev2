import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"

import constants from  './const';

const state = {
   transfers_data:null,
};

const getters = {
    [constants.GET_TRANSFERS_DATA] : state => {
        return state.transfers_data;
    },
};

const mutations = {
    [constants.SET_TRANSFERS_DATA] (state,data) {
        state.transfers_data = data.value;
    },
};

const actions = {
    [constants.FETCH_TRANSFERS_DATA](context,filters) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/transfers")
                .then(({data}) => {
                    context.commit(constants.SET_TRANSFERS_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
