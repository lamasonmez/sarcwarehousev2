export default {
    /** Mutations */
        SET_TRANSFERS_DATA : "setTransfersData",
    /** Getters */
        GET_TRANSFERS_DATA : "getTransfersData",
    /** Actions */
        FETCH_TRANSFERS_DATA : "fetchTransfersData",
    }
    