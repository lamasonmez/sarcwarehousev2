export default {
    /** Mutations */
        SET_SUBBRANCHES_DATA : "setSubBranchesData",
        SET_SUBBRANCH_ITEM : "setSubbranchItem",
        CLEAR_SUBRANCH_ITEM :"clearSubbranchItem",
    /** Getters */
        GET_SUBBRANCHES_DATA : "getSubBranchesData",
        GET_SUSBBRANCH_ITEM : "getSubbranchItem",
    /** Actions */
        FETCH_SUBBRANCHES_DATA : "fetchSubBranchesData",
        DELETE_SUBBRANCH_ITEM :"deleteSubbranchItem",
        STORE_SUBBRANCH_ITEM: "storeSubbranchItem",
    }
    