import ApiService from "@/api/api.service";

import constants from  './const';

const state = {
   sub_branches_data:null,
   subbranch_item:{
    id:null,
    type:'',
    branch_id:null,
    name:{
        ar:null,
        en:null
    }
   }
}

const getters = {
    [constants.GET_SUBBRANCHES_DATA] : state => {
        return state.sub_branches_data;
    },
    [constants.GET_SUSBBRANCH_ITEM] : state => {
        return state.subbranch_item;
    },
};

const mutations = {
    [constants.SET_SUBBRANCHES_DATA] (state,data) {
        state.sub_branches_data = data.value;
    },
    [constants.SET_SUBBRANCH_ITEM] (state,data) {
        state.subbranch_item = data.value;
    },
    [constants.CLEAR_SUBRANCH_ITEM] (state) {
        state.subbranch_item = {
            id:null,
            type:'',
            branch_id:null,
            name:{
                ar:null,
                en:null
            }
        };
    },

};

const actions = {
    [constants.FETCH_SUBBRANCHES_DATA](context,payload=null) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            let url = "api/sub-branches";
            payload?.id !=null ? url = url+'/'+payload?.id : '';
            ApiService.get(url)
                .then(({data}) => {
                    context.commit(constants.SET_SUBBRANCHES_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_SUBBRANCH_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/sub-branches/store",data)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_SUBRANCH_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_SUBBRANCH_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/sub-branches/delete/"+data.id)
                .then(({data}) => {
                    context.commit(constants.CLEAR_SUBRANCH_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
