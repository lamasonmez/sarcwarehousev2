export default {
    /** Mutations */
        SET_USERS_DATA : "setUsersData",
        SET_USER_ITEM : "setUserItem",
        CLEAR_USER_ITEM :"clearUserItem",
    /** Getters */
        GET_USERS_DATA : "getUsersData",
        GET_USER_ITEM : "getUserItem",
    /** Actions */
        FETCH_USERS_DATA : "fetchUsersData",
        DELETE_USER_ITEM :"deleteUserItem",
        STORE_USER_ITEM: "storeUserItem",
        CLEAR_USER:"clearUser",
        SET_USER:'setUser'
    }
    