import ApiService from "@/api/api.service";

import constants from './const';

const state = {
    users: null,
    user_item: {
        id: null,
        name: null,
        email: null,
        full_name: {
            ar: null,
            en: null
        },
        branchable_type: null,
        branchable_id: null,
        rolable_type: null,
        rolable_id: null,
        phone: null,
        role_id: null,
        warehouse_id: null
    }
};

const getters = {
    [constants.GET_USERS_DATA]: state => {
        return state.users;
    },
    [constants.GET_USER_ITEM]: state => {
        return state.user_item;
    },
};

const mutations = {
    [constants.SET_USERS_DATA] (state, data) {
        state.users = data.value;
    },
    [constants.SET_USER_ITEM] (state, data) {
        state.user_item = data.value;
    },
    [constants.CLEAR_USER_ITEM] (state) {
        state.user_item = {
            id: null,
            name: null,
            email: null,
            full_name: {
                ar: null,
                en: null
            },
            branchable_type: null,
            branchable_id: null,
            phone: null,
            role_id: null,
            warehouse_id: null
        };
    },
};

const actions = {
    [constants.FETCH_USERS_DATA](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/users")
                .then(({data}) => {
                    context.commit(constants.SET_USERS_DATA, {value: data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_USER_ITEM](context, data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/users/store", data)
                .then(({data}) => {
                    console.log('data', data);
                    context.commit(constants.CLEAR_USER_ITEM, data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_USER_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/users/delete/"+data.id)
                .then(({data}) => {
                    context.commit(constants.CLEAR_USER_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.CLEAR_USER](context) {
        return new Promise((resolve, reject)=>{
            context.commit(constants.CLEAR_USER_ITEM)
            resolve();
        })


    },
    [constants.SET_USER](context, data) {
        return new Promise((resolve, reject) => {
            context.commit(constants.SET_USER_ITEM, data)
            resolve();
        })

    }
};

export default {
    getters,
    actions,
    mutations,
    state
}
