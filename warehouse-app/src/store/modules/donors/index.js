import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"

import constants from './const';

const state = {
    donors_data: null,
    warehouse_donor_data: [],
    donor_item: {
        id: null,
        name: {
            ar: null,
            en: null
        }
    }
};

const getters = {
    [constants.GET_DONORS_DATA]: state => {
        return state.donors_data;
    },
    [constants.GET_DONOR_ITEM]: state => {
        return state.donor_item;
    },
    [constants.GET_WAREHOUSE_DONORS_DATA]: state => {
        return state.warehouse_donor_data
    }
};

const mutations = {
    [constants.SET_DONORS_DATA](state, data) {
        state.donors_data = data.value;
    },
    [constants.SET_DONOR_ITEM](state, data) {
        state.donor_item = data.value;
    },
    [constants.CLEAR_DONOR_ITEM](state) {
        state.donor_item = {
            id: null,
            name: {
                ar: null,
                en: null
            }
        };
    },
    [constants.SET_WAREHOUSE_DONORS_DATA](state, data) {
        state.warehouse_donor_data = data.value;
    },
};

const actions = {
    [constants.FETCH_DONORS_DATA](context, filters) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/donors")
                .then(({ data }) => {
                    context.commit(constants.SET_DONORS_DATA, { value: data });
                    resolve(data);
                })
                .catch(({ response }) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_DONOR_ITEM](context, data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/donors/store", data)
                .then(({ data }) => {
                    console.log('data', data);
                    context.commit(constants.CLEAR_DONOR_ITEM, data);
                    resolve(data);
                })
                .catch(({ error }) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_DONOR_ITEM](context, data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/donors/delete/" + data.id)
                .then(({ data }) => {
                    context.commit(constants.DELETE_DONOR_ITEM, data);
                    resolve(data);
                })
                .catch(({ error }) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.FETCH_WAREHOUSE_DONORS_DATA](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/donor/getWarehouseDonor")
                .then(({ data }) => {
                    context.commit(constants.SET_WAREHOUSE_DONORS_DATA, { value: data });
                    resolve(data);
                })
                .catch(({ response }) => {

                    reject(response);
                });
        });
    },

    [constants.EXPORT_WAREHOUSE_DONOR](context, data) {

        return new Promise((resolve, reject) => {
            ApiService.setHeader();

            ApiService.setResponseType("blob");
            ApiService.post("/api/donor/exportDonorMaterial/" + data.id)
                .then((response) => {
                    
                    let blob = new Blob([response.data]);
                    const url = window.URL.createObjectURL(blob);
                    const a = document.createElement('a');
                    // get file name and extension from header response
                    console.log(response.headers);
                    //let file = response.headers['content-disposition']?.filename;
                   
                   
                    let filename = data.name + '.zip';
                    // a.style.display = 'nofne';
                    a.href = url;
                    a.download = filename;
                    document.body.appendChild(a);
                    a.click();
                    window.URL.revokeObjectURL(url);

                })
                .catch((error) => {
                    console.log(error);
                    reject(error);
                });
        });
    },
};



export default {
    getters,
    actions,
    mutations,
    state
}
