"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  /** Mutations */
  SET_DONORS_DATA: "setDonorsData",

  /** Getters */
  GET_DONORS_DATA: "getDonorsData",

  /** Actions */
  FETCH_DONORS_DATA: "fetchDonorsData"
};
exports["default"] = _default;