"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _api = _interopRequireDefault(require("@/api/api.service"));

var _jwt = _interopRequireDefault(require("@/services/jwt.service"));

var _const = _interopRequireDefault(require("./const"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var state = {
  donors_data: null
};

var getters = _defineProperty({}, _const["default"].GET_DONORS_DATA, function (state) {
  return state.donors_data;
});

var mutations = _defineProperty({}, _const["default"].SET_DONORS_DATA, function (state, data) {
  state.donors_data = data.value;
});

var actions = _defineProperty({}, _const["default"].FETCH_DONORS_DATA, function (context, filters) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/donors").then(function (_ref) {
      var data = _ref.data;
      context.commit(_const["default"].SET_DONORS_DATA, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref2) {
      var response = _ref2.response;
      console.log(response);
      reject(response);
    });
  });
});

var _default = {
  getters: getters,
  actions: actions,
  mutations: mutations,
  state: state
};
exports["default"] = _default;