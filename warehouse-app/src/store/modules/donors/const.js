export default {
    /** Mutations */
    SET_DONORS_DATA: "setDonorsData",
    SET_WAREHOUSE_DONORS_DATA: "setWarehouseDonorsData",
    SET_DONOR_ITEM: "setDonorItem",
    CLEAR_DONOR_ITEM: "clearDonorItem",
    /** Getters */
    GET_DONORS_DATA: "getDonorsData",
    GET_WAREHOUSE_DONORS_DATA: "getWarehouseDonorsData",
    GET_DONOR_ITEM: "getDonorItem",
    /** Actions */
    FETCH_DONORS_DATA: "fetchDonorsData",
    FETCH_WAREHOUSE_DONORS_DATA: "fetchWarehouseDonorsData",
    EXPORT_WAREHOUSE_DONOR: "exportWarehouseDonor",
    DELETE_DONOR_ITEM: "deleteDonorItem",
    STORE_DONOR_ITEM: "storeDonorItem",
}
    