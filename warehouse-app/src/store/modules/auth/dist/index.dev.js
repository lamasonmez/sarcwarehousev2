"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _api = _interopRequireDefault(require("@/api/api.service"));

var _jwt = _interopRequireDefault(require("@/services/jwt.service"));

var _const = _interopRequireDefault(require("./const"));

var _getters, _mutations, _actions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var state = {
  token: null,
  errors: {
    login: [],
    register: []
  },
  user: null,
  isAuthenticated: false
};
var getters = (_getters = {}, _defineProperty(_getters, _const["default"].GET_LOGIN_ERRORS, function (state) {
  return state.errors.login;
}), _defineProperty(_getters, _const["default"].GET_ERRORS, function (state) {
  return state.errors;
}), _defineProperty(_getters, _const["default"].GET_CURRENT_USER, function (state) {
  return state.user;
}), _defineProperty(_getters, _const["default"].GET_IS_AUTHENTICATED, function (state) {
  return state.isAuthenticated;
}), _getters);
var mutations = (_mutations = {}, _defineProperty(_mutations, _const["default"].CLEAR_ERRORS_MUTATION, function (state) {
  state.errors = [];
}), _defineProperty(_mutations, _const["default"].RE_SET_STATE_MUTATION, function (state) {
  state.token = null;
  state.errors.login = [];
  state.errors.register = [];
  state.user = null, state.isAuthenticated = false;
}), _defineProperty(_mutations, _const["default"].SET_ERROR_MUTATION, function (state, _ref) {
  var target = _ref.target,
      errors = _ref.errors;

  for (var key in errors) {
    state.errors[target].push(errors[key][0]);
  }
}), _defineProperty(_mutations, _const["default"].SET_USER_MUTATION, function (state, data) {
  state.isAuthenticated = true;
  state.user = data.user;
  state.token = data.token;
  state.errors = {
    login: []
  };

  _jwt["default"].setToken(data.token);

  _api["default"].setHeader();
}), _defineProperty(_mutations, _const["default"].LOGOUT_MUTATION, function (state, payload) {
  _jwt["default"].unsetToken();
}), _mutations);
var actions = (_actions = {}, _defineProperty(_actions, _const["default"].LOGIN_ACTION, function (context, credentials) {
  return new Promise(function (resolve, reject) {
    _api["default"].post("api/login", credentials).then(function (_ref2) {
      var data = _ref2.data;
      context.commit(_const["default"].CLEAR_ERRORS_MUTATION);
      context.commit(_const["default"].SET_USER_MUTATION, {
        user: data.user,
        token: data.token
      });
      resolve(data);
    })["catch"](function (_ref3) {
      var response = _ref3.response;
      toastr.options.preventDuplicates = true;
      toastr.options.progressBar = true;
      toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].LOGOUT_ACTION, function (context, payload) {
  var _this = this;

  context.commit(_const["default"].LOGOUT_MUTATION);
  context.commit(_const["default"].RE_SET_STATE_MUTATION, null, {
    root: true
  });
  return new Promise(function (resolve, reject) {
    _api["default"].get("api/logout").then(function (_ref4) {
      var data = _ref4.data;
      context.commit(_const["default"].RE_SET_STATE_MUTATION);
      resolve(data);

      _this.$router.push({
        name: "login"
      });
    })["catch"](function (_ref5) {
      var response = _ref5.response;
      context.commit(_const["default"].SET_ERROR_MUTATION, {
        target: 'logout',
        message: response
      });
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].CHECK_AUTH_ACTION, function (context) {
  var _this2 = this;

  if (_jwt["default"].getToken()) {
    _api["default"].setHeader();

    _api["default"].get("api/token/validate")["catch"](function (_ref6) {
      var response = _ref6.response;
      context.commit(_const["default"].LOGOUT_MUTATION);

      _this2.$router.push({
        name: "login"
      });
    });
  } else {
    context.commit(_const["default"].LOGOUT_MUTATION); // this.$router.push({name: "login"});
  }
}), _actions);
var _default = {
  getters: getters,
  actions: actions,
  mutations: mutations,
  state: state
};
exports["default"] = _default;