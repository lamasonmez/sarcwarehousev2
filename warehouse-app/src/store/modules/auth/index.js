import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"
import constants from './const';


const state = {
    token: null,
    errors: {
        login: [],
        register: []
    },
    user: null,
    isAuthenticated: false,
};

const getters = {
    [constants.GET_LOGIN_ERRORS] : state => {
        return state.errors.login;
    },
    [constants.GET_ERRORS] : state => {
        return state.errors;
    },
    [constants.GET_CURRENT_USER] : state => {
        return state.user;
    },
    [constants.GET_IS_AUTHENTICATED] : state => {
        return state.isAuthenticated;
    }
};

const mutations = {
    [constants.CLEAR_ERRORS_MUTATION] (state) {
        state.errors = [];
    },
    [constants.RE_SET_STATE_MUTATION](state){
      state.token = null;
      state.errors.login=[];
      state.errors.register=[];
      state.user =  null,
      state.isAuthenticated= false;
    },
    [constants.SET_ERROR_MUTATION](state, {target, errors}) {


        for (let key in errors)
            state.errors[target].push(errors[key][0]);
    },
    [constants.SET_USER_MUTATION](state, data) {
        state.isAuthenticated = true;
        state.user = data.user;
        state.token = data.token;
        state.errors = {
            login: [],
        };
        JwtService.setToken(data.token);
        ApiService.setHeader()
    },
    [constants.LOGOUT_MUTATION] (state, payload) {
        JwtService.unsetToken();
    },
};

const actions = {
    [constants.LOGIN_ACTION](context, credentials) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/login", credentials)
                .then(({data}) => {
                    context.commit(constants.CLEAR_ERRORS_MUTATION);
                    context.commit(
                        constants.SET_USER_MUTATION, {user: data.user, token: data.token}
                    );
                    
                    resolve(data);
                })
                .catch(({response}) => {
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.LOGOUT_ACTION](context, payload) {
        context.commit(constants.LOGOUT_MUTATION);
        context.commit(constants.RE_SET_STATE_MUTATION, null, { root: true });
        return new Promise((resolve, reject) => {
            ApiService.get("api/logout")
                .then(({data}) => {
                    context.commit(constants.RE_SET_STATE_MUTATION);
                    resolve(data);
                    this.$router.push({name: "login"});
                })
                .catch(({response}) => {
                    context.commit(constants.
                        SET_ERROR_MUTATION,
                        {target: 'logout', message: response}
                    );
                    reject(response);
                });
        });
    },
    
    [constants.CHECK_AUTH_ACTION](context) {
        if (JwtService.getToken()) {
            ApiService.setHeader();
            ApiService.get("api/token/validate")
                .catch(({ response }) => {
                    context.commit(constants.LOGOUT_MUTATION);
                    this.$router.push({name: "login"});
                });
        } else {
            context.commit(constants.LOGOUT_MUTATION);
           // this.$router.push({name: "login"});
        }
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
