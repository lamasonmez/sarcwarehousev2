export default { 
/** Mutations */
CLEAR_ERRORS_MUTATION : "clearErrors",
SET_USER_MUTATION : "setUser",
SET_ERROR_MUTATION : "setError",
RE_SET_STATE_MUTATION : "resetState",
LOGOUT_MUTATION : "logout",



/** Getters */
GET_LOGIN_ERRORS : "getLoginErrors",
GET_ERRORS : "getErrors",
GET_CURRENT_USER:"currentUser", 
GET_IS_AUTHENTICATED:"isAuthenticated",

/** Actions */
LOGIN_ACTION: "login",
LOGOUT_ACTION: "logout",
CHECK_AUTH_ACTION:"checkAuth"

}
