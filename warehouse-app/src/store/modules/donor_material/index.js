import ApiService from "@/api/api.service";

import constants from  './const';

const state = {
   donor_materials_data:null,
   donor_material:{
       id:null,
       code:'0000',
       content:null,
       donor_id:null,
       material_id:null,
       donor:null,
       material:null,
       size:null,
       weight:null,
       unit_id:null,
       unit:null
   },
};

const getters = {
    [constants.GET_DONOR_MATERIALS_DATA] : state => {
        return state.donor_materials_data;
    },
    [constants.GET_DONOR_MATERIAL] : state => {
        return state.donor_material;
    },
    
};

const mutations = {
    [constants.SET_DONOR_MATERIALS_DATA] (state,data) {
        state.donor_materials_data = data.value;
    },
    [constants.SET_DONOR_MATERIAL] (state,data) {
        state.donor_material = data.value;
    },
    [constants.CLEAR_DONOR_MATERIAL] (state) {
        state.donor_material ={
            id:null,
            code:'',
            content:null,
            donor_id:null,
            material_id:null,
            donor:null,
            material:null,
            size:null,
            weight:null,
            unit_id:null,
            unit:null
        };
    },
};

const actions = {
    [constants.FETCH_DONOR_MATERIALS_DATA](context,payload=null) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            let url = "api/donor/materials";
            if(payload){
                url = url+'/'+payload ;
            }
            context.commit(constants.SET_DONOR_MATERIALS_DATA,{value:[]});
            ApiService.get(url)
                .then(({data}) => {
                    context.commit(constants.SET_DONOR_MATERIALS_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_DONOR_MATERIAL](context,donor_material_id) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/donor_materials/"+donor_material_id)
                .then(({data}) => {
                    context.commit(constants.SET_DONOR_MATERIAL,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
[constants.STORE_DONOR_MATERIAL](context,data) {
    return new Promise((resolve, reject) => {
        ApiService.setHeader();
        ApiService.post("api/donor_materials/store",data)
            .then(({data}) => {
                console.log('data',data);
                context.commit(constants.CLEAR_DONOR_MATERIAL);
                resolve(data);
            })
            .catch(({error}) => {
                console.log(error);
                reject(data);
            });
    });
},
[constants.SET_DONOR_MATERIAL_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            console.log('data',data);
            context.commit(constants.SET_DONOR_MATERIAL,data);
            resolve(data);
        });
},
[constants.DELETE_DONOR_MATERIAL_ITEM](context,data) {
    return new Promise((resolve, reject) => {
        ApiService.setHeader();
        ApiService.get("api/donor_materials/delete/"+data.id)
            .then(({data}) => {
                context.commit(constants.CLEAR_DONOR_MATERIAL,data);
                resolve(data);
            })
            .catch(({error}) => {
                console.log(error);
                reject(data);
            });
    });
},    
};

export default {
    getters,
    actions,
    mutations,
    state
}
