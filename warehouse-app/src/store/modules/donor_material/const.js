export default {
      /** Mutations */
        SET_DONOR_MATERIALS_DATA : "setDonorMaterialsData",
        SET_DONOR_MATERIAL : "setDonorMaterial",//for selection ,
        CLEAR_DONOR_MATERIAL:"clearMaterialItem",
      /** Getters */
        GET_DONOR_MATERIALS_DATA : "getDonorMaterialsData",
        GET_DONOR_MATERIAL : "getDonorMaterial",

      /** Actions */
        FETCH_DONOR_MATERIALS_DATA : "fetchDonorMaterialsData",
        FETCH_DONOR_MATERIAL : "fetchDonorMaterial",
        STORE_DONOR_MATERIAL:"storeDonorMaterialItem",
        SET_DONOR_MATERIAL_ITEM : "setDonorMaterialItem",//for selection ,
        DELETE_DONOR_MATERIAL_ITEM:"deleteDonorMaterialItem",

    }
    