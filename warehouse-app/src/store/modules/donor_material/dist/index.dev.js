"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _api = _interopRequireDefault(require("@/api/api.service"));

var _jwt = _interopRequireDefault(require("@/services/jwt.service"));

var _const = _interopRequireDefault(require("./const"));

var _getters, _mutations, _actions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var state = {
  donor_materials_data: null,
  donor_material: null
};
var getters = (_getters = {}, _defineProperty(_getters, _const["default"].GET_DONOR_MATERIALS_DATA, function (state) {
  return state.donor_materials_data;
}), _defineProperty(_getters, _const["default"].GET_DONOR_MATERIAL, function (state) {
  return state.donor_material;
}), _getters);
var mutations = (_mutations = {}, _defineProperty(_mutations, _const["default"].SET_DONOR_MATERIALS_DATA, function (state, data) {
  state.donor_materials_data = data.value;
}), _defineProperty(_mutations, _const["default"].SET_DONOR_MATERIAL, function (state, data) {
  state.donor_material = data.value;
}), _mutations);
var actions = (_actions = {}, _defineProperty(_actions, _const["default"].FETCH_DONOR_MATERIALS_DATA, function (context, donor_id) {
  return new Promise(function (resolve, reject) {
    console.log('fetch donor materials');

    _api["default"].setHeader();

    _api["default"].get("api/donor/materials/" + donor_id).then(function (_ref) {
      var data = _ref.data;
      context.commit(_const["default"].SET_DONOR_MATERIALS_DATA, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref2) {
      var response = _ref2.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].FETCH_DONOR_MATERIAL, function (context, donor_material_id) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/donor_materials/" + donor_material_id).then(function (_ref3) {
      var data = _ref3.data;
      context.commit(_const["default"].SET_DONOR_MATERIAL, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref4) {
      var response = _ref4.response;
      console.log(response);
      reject(response);
    });
  });
}), _actions);
var _default = {
  getters: getters,
  actions: actions,
  mutations: mutations,
  state: state
};
exports["default"] = _default;