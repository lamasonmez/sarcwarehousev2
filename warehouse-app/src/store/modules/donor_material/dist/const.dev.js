"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  /** Mutations */
  SET_DONOR_MATERIALS_DATA: "setDonorMaterialsData",
  SET_DONOR_MATERIAL: "setDonorMaterial",

  /** Getters */
  GET_DONOR_MATERIALS_DATA: "getDonorMaterialsData",
  GET_DONOR_MATERIAL: "getDonorMaterial",

  /** Actions */
  FETCH_DONOR_MATERIALS_DATA: "fetchDonorMaterialsData",
  FETCH_DONOR_MATERIAL: "fetchDonorMaterial"
};
exports["default"] = _default;