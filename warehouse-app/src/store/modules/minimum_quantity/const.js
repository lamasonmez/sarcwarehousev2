export default {
    /** Mutations */
        SET_MINIMUM_QUANTITY_DATA : "setMinimumQuantityData",
        SET_MINIMUM_QUANTITY_ITEM : "setMinimumQuantityItem",
        CLEAR_MINIMUM_QUANTITY_ITEM :"clearMinimumQuantityItem",

    /** Getters */
        GET_MINIMUM_QUANTITY_DATA : "getMinimumQuantityData",
        GET_MINIMUM_QUANTITY_ITEM : "getMinimumQuantityItem",
       


    /** Actions */
        FETCH_MINIMUM_QUANTITY_DATA : "fetchMinimumQuantityData",
        DELETE_MINIMUM_QUANTITY_ITEM :"deleteMinimumQuantityItem",
        STORE_MINIMUM_QUANTITY_ITEM: "storeMinimumQuantityItem",

    }
    