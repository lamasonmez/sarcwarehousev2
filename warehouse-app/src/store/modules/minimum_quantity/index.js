import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"
import {VUE_APP_API_URL} from "@/config";
import constants from  './const';

const state = {
   minimum_quantity_data:[],
   minimum_quantity_item:{
       id:null,
       donor_material_id:null,
       warehouse_id:null,
       quantity:0,
   },
  
};

const getters = {
    [constants.GET_MINIMUM_QUANTITY_DATA] : state => {
        return state.minimum_quantity_data;
    },
    [constants.GET_MINIMUM_QUANTITY_ITEM] : state => {
        return state.minimum_quantity_item;
    },
};

const mutations = {
    [constants.SET_MINIMUM_QUANTITY_DATA] (state,data) {
        state.minimum_quantity_data = data.value;
    },
    [constants.SET_MINIMUM_QUANTITY_ITEM] (state,data) {
        state.minimum_quantity_item = data.value;
    },
    
    
    [constants.CLEAR_MINIMUM_QUANTITY_ITEM] (state,data) {
        state.minimum_quantity_item = {
            id:null,
            warehouse_id:data.warehouse_id,
            donor_material_id:null,
        };
    },
    
};

const actions = {
    [constants.FETCH_MINIMUM_QUANTITY_DATA](context,{pagination={'link':VUE_APP_API_URL+'api/minimum_quantities','perPage':10}}) {
        return new Promise((resolve, reject) => {
            console.log('minimum_quantity_store , pagination is ',this.pagination);
           let url = pagination.link;
            if(pagination.link==VUE_APP_API_URL+"api/minimum_quantities"){
                url = pagination.link+"?page=1&perPage="+pagination.perPage;
        }
            else{
                url= pagination.link+'&perPage='+pagination.perPage
            }
            ApiService.setHeader();
            ApiService.post(url)
                .then(({data}) => {
                    context.commit(constants.SET_MINIMUM_QUANTITY_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_MINIMUM_QUANTITY_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/miniumu_quantities/store",data)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_MINIMUM_QUANTITY_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_MINIMUM_QUANTITY_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/miniumu_quantities/delete/"+data.id)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_MINIMUM_QUANTITY_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
  
};

export default {
    getters,
    actions,
    mutations,
    state
}
