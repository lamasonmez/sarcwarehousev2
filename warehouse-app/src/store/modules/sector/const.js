export default {
    /** Mutations */
        SET_SECTORS_DATA : "setSectorsData",
        SET_SECTOR_ITEM : "setSectorItem",
        CLEAR_SECTOR_ITEM :"clearSectorItem",
    /** Getters */
        GET_SECTORS_DATA : "getSectorsData",
        GET_SECTOR_ITEM : "getSectorItem",
    /** Actions */
        FETCH_SECTORS_DATA : "fetchSectorsData",
        DELETE_SECTOR_ITEM :"deleteSectorItem",
        STORE_SECTOR_ITEM: "storeSectorItem",
    }
    