import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"

import constants from  './const';

const state = {
   sectors:null,
   sector_item:{
    id:null,
    name:{
        ar:null,
        en:null
    }
}
};

const getters = {
    [constants.GET_SECTORS_DATA] : state => {
        return state.sectors;
    },
    [constants.GET_SECTOR_ITEM] : state => {
        return state.sector_item;
    },
};

const mutations = {
    [constants.SET_SECTORS_DATA] (state,data) {
        state.sectors = data.value;
    },
    [constants.SET_SECTOR_ITEM] (state,data) {
        state.sector_item = data.value;
    },
    [constants.CLEAR_SECTOR_ITEM] (state) {
        state.sector_item = {
            id:null,
            name:{
                ar:null,
                en:null
            }
        };
    },
};

const actions = {
    [constants.FETCH_SECTORS_DATA](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/sectors")
                .then(({data}) => {
                    context.commit(constants.SET_SECTORS_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_SECTOR_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/sectors/store",data)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_SECTOR_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_SECTOR_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/sectors/delete/"+data.id)
                .then(({data}) => {
                    context.commit(constants.CLEAR_SECTOR_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
