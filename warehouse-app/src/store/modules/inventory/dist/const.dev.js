"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  /** Mutations */
  SET_INVENTORY_DATA: "setInventoryData",

  /** Getters */
  GET_INVENTORY_DATA: "getInventoryData",

  /** Actions */
  FETCH_INVENTORY_DATA: "fetchInventoryData"
};
exports["default"] = _default;