import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"
import {VUE_APP_API_URL} from "@/config";
import constants from  './const';

const state = {
   inventory_data:null,
   material_transactions:[]
};

const getters = {
    [constants.GET_INVENTORY_DATA] : state => {
        return state.inventory_data;
    },
    [constants.GET_MATERIAL_TRANSACTIONS] : state => {
        return state.material_transactions;
    },
};

const mutations = {
    [constants.SET_INVENTORY_DATA] (state,data) {
        state.inventory_data = data.value;
    },
    [constants.SET_MATERIAL_TRANSACTIONS] (state,data) {
        state.material_transactions = data.value;
    },
};
const actions = {
    [constants.FETCH_INVENTORY_DATA](context,{pagination={'link':VUE_APP_API_URL+'api/inventory','perPage':50}}) {
        return new Promise((resolve, reject) => {
            let url = pagination.link;
            if(pagination.link==VUE_APP_API_URL+'api/inventory'){
                url = pagination.link+"?page=1&perPage="+pagination.perPage;
            }
            else{
                url= pagination.link+'&perPage='+pagination.perPage
            }
            ApiService.setHeader();
            ApiService.get(url)
                .then(({data}) => {
                    context.commit(constants.SET_INVENTORY_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
                    console.log(response);
                    reject(response);
                });
        });  
    },
    [constants.FILTER_INVENTORY](context,filters) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
        
            let parsfilter=filters;
            let url = VUE_APP_API_URL+'api/inventory/filter';
            if (filters.pagination!=null){
                url = filters.pagination.link+'&perPage='+filters.pagination.perPage;
            }
            else {
                url=url+'?page=1&perPage=10';
           }
            ApiService.post(url,parsfilter)

                .then(({data}) => {

                    context.commit(constants.SET_INVENTORY_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
                    console.log(response);
                    reject(response);
                });
        });  
    },
    [constants.FETCH_MATERIAL_TRANSACTIONS](context,donor_material_id) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/inventory/material_transactions/"+donor_material_id)
                .then(({data}) => {
                    context.commit(constants.SET_MATERIAL_TRANSACTIONS,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
                    console.log(response);
                    reject(response);
                });
        });  
    },
    [constants.FILTER_MATERIAL_TRANSACTIONS](context,{donor_material_id,filters}) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/inventory/material_transactions/filter/"+donor_material_id,filters)
                .then(({data}) => {
                    context.commit(constants.SET_MATERIAL_TRANSACTIONS,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
                    console.log(response);
                    reject(response);
                });
        });  

        
    },
    [constants.EXPRORT_INVENTORY](context,payload) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/exports/inventory/"+payload.type,{filters:payload.filters})
                .then(({data}) => {
                    var fileLink = document.createElement('a');
                    fileLink.href = data.path;
                    //fileLink.setAttribute('download', 'inventory.'+type);
                     document.body.appendChild(fileLink);
                     fileLink.click();
                    resolve(data);
                })
                .catch(({response}) => {
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
                    console.log('error = ',response);
                    reject(response);
                });
        });
    },
    [constants.EXPRORT_MATERIAL_TRANSACTIONS](context,donor_material_id) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/exports/material_transactions/"+donor_material_id+"/xlsx")
                .then(({data}) => {
                    var fileLink = document.createElement('a');
                    fileLink.href = data.path;
                    fileLink.setAttribute('download', 'material_transactions.xlsx');
                    document.body.appendChild(fileLink);
                    fileLink.click();
                    resolve(data);
                })
                .catch(({response}) => {
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
                    console.log('error = ',response);
                    reject(response);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
