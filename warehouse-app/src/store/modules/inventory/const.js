export default {
/** Mutations */
    SET_INVENTORY_DATA : "setInventoryData",
    SET_MATERIAL_TRANSACTIONS:"setMaterialTransactions",
/** Getters */
    GET_INVENTORY_DATA : "getInventoryData",
    GET_MATERIAL_TRANSACTIONS:"getMaterialTransactions",
/** Actions */
    FETCH_INVENTORY_DATA:"fetchInventoryData",
    FETCH_MATERIAL_TRANSACTIONS:"fetchMaterialTransactions",
    EXPRORT_INVENTORY:"exportInventory",
    FILTER_INVENTORY:"filterInventory",
    EXPRORT_MATERIAL_TRANSACTIONS:"exportMaterialTransactions",
    FILTER_MATERIAL_TRANSACTIONS:"filterMaterialTransactions",

}
