import ApiService from "@/api/api.service";
import constants from  './const';

const state = {
  minimum_quantitity_notifications:[],
  pending_transactions:[]
  
};

const getters = {
    [constants.GET_MINIMUM_QUANTITY_NOTIFICATIONS] : state => {
        return state.minimum_quantitity_notifications;
    },
    [constants.GET_PENDING_TRANSACTIONS] : state => {
        return state.pending_transactions;
    },
};

const mutations = {
    [constants.SET_MINIMUM_QUANTITY_NOTIFICATIONS] (state,data) {
        state.minimum_quantitity_notifications = data.value;
    },
    [constants.SET_PENDING_TRANSACTIONS] (state,data) {
        state.pending_transactions = data.value;
    },
};

const actions = {
    [constants.FETCH_MINIMUM_QUANTITY_NOTIFICATIONS](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/notifications/minimum_quantities")
                .then(({data}) => {
                    context.commit(constants.SET_MINIMUM_QUANTITY_NOTIFICATIONS,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    reject();
                });
        });
    },
    [constants.FETCH_PENDING_TRANSACTIONS](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/notifications/pending_transactions")
                .then(({data}) => {
                    context.commit(constants.SET_PENDING_TRANSACTIONS,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    reject();
                });
        });
    },
  
};

export default {
    getters,
    actions,
    mutations,
    state
}
