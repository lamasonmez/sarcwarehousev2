export default {
    /** Mutations */
        SET_MINIMUM_QUANTITY_NOTIFICATIONS : "setMinimumQuantityNotifications",
        SET_PENDING_TRANSACTIONS : "setPendingTransactions",

    /** Getters */
        GET_MINIMUM_QUANTITY_NOTIFICATIONS : "getMinimumQuantityNotifications",
        GET_PENDING_TRANSACTIONS : "getPendingTransactions",

    /** Actions */
        FETCH_MINIMUM_QUANTITY_NOTIFICATIONS : "fetchMinimumQuantityNotifications",
        FETCH_PENDING_TRANSACTIONS : "fetchPendingTransactions",

    }
    