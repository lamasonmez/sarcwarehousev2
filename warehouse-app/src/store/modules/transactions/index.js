import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"
import {VUE_APP_API_URL} from "@/config";
import constants from  './const';

const state = {
   transaction_in_data:null,
   transaction_in:{
       id:null,
       waybill_no:null,
       fromable_type:null,
       fromable_id:null,
       warehouse_id:null,
       date:null,
       driver_name:null,
       vehicle_number:null,
       driver_national_id:null,
       transportation_company_name:null,
       donor_waybill_no:null,
       weight:null,

   },
   transaction_out:{
    id:null,
    waybill_no:null,
    toable_type:null,
    toable:null,
    toable_id:null,
    warehouse_id:null,
    date:null,
    weight:null,
   donor_id:null,
   distribution:null,

   
    },
   transaction_details:[],
   transaction_out_details:[],
   transaction_out_data:null,
   transaction_detail_item:{
       id:null,
       quantity:null,
       notes:null,
       is_convoy:false,
       loss:null,
       reason_for_loss:null,
       reason_for_damage:null,
       damage:null,
       expiration_date:null,
       transactionable_type:null,
       transactionable_id:null,
       transaction_type:null,
       ctn_id:null,
       donor_material_id:null,
       warehouse_id:null,
   }
};

const getters = {
    [constants.GET_TRANSACTION_IN_DATA] : state => {
        return state.transaction_in_data;
    },
    [constants.GET_TRANSACTION_OUT_DATA] : state => {
        return state.transaction_out_data;
    },
    [constants.GET_TRANSACTION_IN] : state => {
        return state.transaction_in;
    },
    [constants.GET_TRANSACTION_OUT] : state => {
        return state.transaction_out;
    },
    [constants.GET_TRANSACTION_DETAILS] : state => {
        return state.transaction_details;
    },
    [constants.GET_TRANSACTION_OUT_DETAILS] : state => {
        return state.transaction_out_details;
    },
    [constants.GET_TRANSACTION_DETAIL_ITEM] : state => {
        return state.transaction_detail_item;
    },
};

const mutations = {
    [constants.SET_TRANSACTION_IN_DATA] (state,data) {
        state.transaction_in_data = data.value;
    },
    [constants.SET_TRANSACTION_OUT_DATA] (state,data) {
        state.transaction_out_data = data.value;
    },
    [constants.SET_TRANSACTION_IN] (state,data) {
        state.transaction_in = data.value;
    },
    [constants.SET_TRANSACTION_OUT] (state,data) {
        state.transaction_out = data.value;
    },
    [constants.SET_TRANSACTION_DETAILS] (state,data) {
        state.transaction_details = data.value;
    },
    [constants.SET_TRANSACTION_OUT_DETAILS] (state,data) {
        state.transaction_out_details = data.value;
    },
    [constants.SET_TRANSACTION_DETAIL_ITEM] (state,data) {
        state.transaction_detail_item = data.value;
    },
    [constants.CLEAR_TRANSACTION_DETAIL_ITEM] (state,data) {
        state.transaction_detail_item = {
            id:null,
            quantity:null,
            notes:null,
            is_convoy:false,
            loss:null,
            reason_for_loss:null,
            reason_for_damage:null,
            damage:null,
            expiration_date:null,
            transactionable_type:data.transactionable_type,
            transactionable_id:data.transactionable_id,
            warehouse_id:data.warehouse_id,
            ctn_id:null,
            donor_material_id:null,
        };
    },
    
};

const actions = {
    [constants.FETCH_TRANSACTION_IN_DATA](context,{filters,pagination={'link':VUE_APP_API_URL+'api/transactions/in','perPage':10}}) {
        return new Promise((resolve, reject) => {
           let url = pagination.link;
            if(pagination.link==VUE_APP_API_URL+"api/transactions/in"){
                url = pagination.link+"?page=1&perPage="+pagination.perPage;
            }
            else{
                url= pagination.link+'&perPage='+pagination.perPage
            }
            ApiService.setHeader();
            ApiService.post(url,filters)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_IN_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_TRANSACTION_OUT_DATA](context,{filters,pagination={'link':VUE_APP_API_URL+'api/transactions/out','perPage':10}}) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            let url = pagination.link;
            if(pagination.link==VUE_APP_API_URL+"api/transactions/out"){
                url = pagination.link+"?page=1&perPage="+pagination.perPage;
            }
            else{
                url= pagination.link+'&perPage='+pagination.perPage
            }
            ApiService.post(url,filters)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_OUT_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_TRANSACTION_IN](context,id) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/transactions/in/"+id)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_IN,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_TRANSACTION_OUT](context,id) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/transactions/out/"+id)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_OUT,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_TRANSACTION_DETAILS](context,id) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/transactions_in/details/"+id)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_DETAILS,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_TRANSACTION_OUT_DETAILS](context,id) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/transactions_out/details/"+id)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_DETAILS,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_TRANSACTION_DETAIL_ITEM](context,id) {
        return new Promise((resolve, reject) => {
            console.log('fetch details id',id);
            ApiService.setHeader();
            ApiService.get("api/transaction_details/"+id)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_DETAIL_ITEM,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },

    [constants.STORE_TRANSACTION_IN](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/transactions_in/store",data)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_IN,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_TRANSACTION_OUT](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/transactions_out/store",data)
                .then(({data}) => {
                    context.commit(constants.SET_TRANSACTION_OUT,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log('response',response);
                    reject(response);
                });
        });
    },
    [constants.CLEAR_TRANSACTION_IN](context,transaction) {
        return new Promise((resolve, reject) => {
            if(transaction){
                let  data={
                    id:null,
                    waybill_no:null,
                    fromable_type:null,
                    fromable_id:null,
                    warehouse_id:transaction.warehouse_id ,
                    date:null
                };
                context.commit(constants.SET_TRANSACTION_IN,{value:data});
                context.commit(constants.SET_TRANSACTION_DETAILS,[]);
            }
       
            resolve();
            reject();

        });
    },
    [constants.CLEAR_TRANSACTION_OUT](context,transaction) {
        return new Promise((resolve, reject) => {
            if(transaction){
                let  data={
                    id:null,
                    waybill_no:null,
                    toable_type:null,
                    toable_id:null,
                    warehouse_id:transaction.warehouse_id,
                    date:null
                };
                context.commit(constants.SET_TRANSACTION_OUT,{value:data});
                context.commit(constants.SET_TRANSACTION_OUT_DETAILS,[]);
            }
       
            resolve();
            reject();

        });
    },
    [constants.STORE_TRANSACTION_DETAIL_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/transaction_details/store",data)
                .then(({data}) => {
                    if(data.error){
                        toastr.options.preventDuplicates = true;
                        toastr.options.progressBar = true;
                        toastr.error(data.error);
                        reject(response);
                    }
                    else{
                        context.commit(constants.CLEAR_TRANSACTION_DETAIL_ITEM,data);
                    }
                    resolve(data);
                })
                .catch((response) => {
                    reject(response);
                });
        });
    },
    [constants.DELETE_TRANSACTION_DETAIL_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/transaction_details/delete/"+data.id)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_TRANSACTION_DETAIL_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    
    [constants.DELETE_TRANSACTION_IN](context,data) {
        console.log('delte transaction in data id',data.id);
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/transactions_in/delete/"+data.id)
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_TRANSACTION_OUT](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/transactions_out/delete/"+data.id)
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.CHECK_WAYBILL](context,wabyill) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/transactions/check-waybill",{'waybill_no':wabyill})
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },

};

export default {
    getters,
    actions,
    mutations,
    state
}
