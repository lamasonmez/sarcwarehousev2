export default {
    /** Mutations */
        SET_TRANSACTION_IN_DATA : "setTransactionInData",
        SET_TRANSACTION_OUT_DATA : "setTransactionOutData",
        SET_TRANSACTION_IN : "setTransactionIn",
        SET_TRANSACTION_OUT : "setTransactionOut",
        SET_TRANSACTION_DETAILS : "setTransactionDetails",
        SET_TRANSACTION_OUT_DETAILS : "setTransactionOutDetails",
        SET_TRANSACTION_DETAIL_ITEM : "setTransactionDetailItem",
        CLEAR_TRANSACTION_DETAIL_ITEM :"clearTransactionDetailItem",

    /** Getters */
        GET_TRANSACTION_IN_DATA : "getTransactionInData",
        GET_TRANSACTION_OUT_DATA : "getTransactionOutData",
        GET_TRANSACTION_IN : "getTransactionIn",
        GET_TRANSACTION_OUT : "getTransactionOut",
        GET_TRANSACTION_DETAILS : "getTransactionDetails",
        GET_TRANSACTION_OUT_DETAILS : "getTransactionOutDetails",
        GET_TRANSACTION_DETAIL_ITEM : "getTransactionDetailItem",


    /** Actions */
        FETCH_TRANSACTION_IN_DATA : "fetchTransactionInData",
        FETCH_TRANSACTION_OUT_DATA : "fetchTransactionOutData",
        FETCH_TRANSACTION_IN : "fetchTransactionIn",
        FETCH_TRANSACTION_OUT : "fetchTransactionOut",
        FETCH_TRANSACTION_DETAILS : "fetchTransactionDetails",
        FETCH_TRANSACTION_OUT_DETAILS : "fetchTransactionOutDetails",
        FETCH_TRANSACTION_DETAIL_ITEM : "fetchTransactionDetailItem",
        STORE_TRANSACTION_IN:"storeTransactionIn",
        STORE_TRANSACTION_OUT:"storeTransactionOut",
        STORE_TRANSACTION_DETAIL_ITEM : "storeTransactionDetailItem",
        CLEAR_TRANSACTION_IN:"clearTransactionInData",
        CLEAR_TRANSACTION_OUT:"clearTransactionOutData",
        DELETE_TRANSACTION_DETAIL_ITEM:"deleteTransactionDetailItem",
        DELETE_TRANSACTION_IN:"deleteTransactionIn",
        DELETE_TRANSACTION_OUT:"deleteTransactionOut",
        CHECK_WAYBILL:"checkWaybill",



    }
    