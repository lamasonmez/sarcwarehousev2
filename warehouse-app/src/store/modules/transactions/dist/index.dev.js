"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _api = _interopRequireDefault(require("@/api/api.service"));

var _jwt = _interopRequireDefault(require("@/services/jwt.service"));

var _const = _interopRequireDefault(require("./const"));

var _getters, _mutations, _actions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var state = {
  transaction_in_data: null,
  transaction_in: {
    id: null,
    waybill_no: null,
    fromable_type: null,
    fromable_id: null,
    warehouse_id: null,
    date: null
  },
  transaction_out: {
    id: null,
    waybill_no: null,
    toable_type: null,
    toable_id: null,
    warehouse_id: null,
    date: null
  },
  transaction_details: [],
  transaction_out_details: [],
  transaction_out_data: null,
  transaction_detail_item: {
    id: null,
    quantity: null,
    notes: null,
    is_convoy: false,
    loss: null,
    reason_for_loss: null,
    reason_for_damage: null,
    damage: null,
    expiration_date: null,
    transactionable_type: null,
    transactionable_id: null,
    transaction_type: null,
    ctn_id: null,
    donor_material_id: null,
    warehouse_id: null
  }
};
var getters = (_getters = {}, _defineProperty(_getters, _const["default"].GET_TRANSACTION_IN_DATA, function (state) {
  return state.transaction_in_data;
}), _defineProperty(_getters, _const["default"].GET_TRANSACTION_OUT_DATA, function (state) {
  return state.transaction_out_data;
}), _defineProperty(_getters, _const["default"].GET_TRANSACTION_IN, function (state) {
  return state.transaction_in;
}), _defineProperty(_getters, _const["default"].GET_TRANSACTION_OUT, function (state) {
  return state.transaction_out;
}), _defineProperty(_getters, _const["default"].GET_TRANSACTION_DETAILS, function (state) {
  return state.transaction_details;
}), _defineProperty(_getters, _const["default"].GET_TRANSACTION_OUT_DETAILS, function (state) {
  return state.transaction_out_details;
}), _defineProperty(_getters, _const["default"].GET_TRANSACTION_DETAIL_ITEM, function (state) {
  return state.transaction_detail_item;
}), _getters);
var mutations = (_mutations = {}, _defineProperty(_mutations, _const["default"].SET_TRANSACTION_IN_DATA, function (state, data) {
  state.transaction_in_data = data.value;
}), _defineProperty(_mutations, _const["default"].SET_TRANSACTION_OUT_DATA, function (state, data) {
  state.transaction_out_data = data.value;
}), _defineProperty(_mutations, _const["default"].SET_TRANSACTION_IN, function (state, data) {
  state.transaction_in = data.value;
}), _defineProperty(_mutations, _const["default"].SET_TRANSACTION_OUT, function (state, data) {
  state.transaction_out = data.value;
  if (data.value.toable != null) state.transaction_out.toable_id = data.value.toable.name;
}), _defineProperty(_mutations, _const["default"].SET_TRANSACTION_DETAILS, function (state, data) {
  state.transaction_details = data.value;
}), _defineProperty(_mutations, _const["default"].SET_TRANSACTION_OUT_DETAILS, function (state, data) {
  state.transaction_out_details = data.value;
}), _defineProperty(_mutations, _const["default"].SET_TRANSACTION_DETAIL_ITEM, function (state, data) {
  state.transaction_detail_item = data.value;
}), _defineProperty(_mutations, _const["default"].CLEAR_TRANSACTION_DETAIL_ITEM, function (state, data) {
  state.transaction_detail_item = {
    id: null,
    quantity: null,
    notes: null,
    is_convoy: false,
    loss: null,
    reason_for_loss: null,
    reason_for_damage: null,
    damage: null,
    expiration_date: null,
    transactionable_type: data.transactionable_type,
    transactionable_id: data.transactionable_id,
    warehouse_id: data.warehouse_id,
    ctn_id: null,
    donor_material_id: null
  };
}), _mutations);
var actions = (_actions = {}, _defineProperty(_actions, _const["default"].FETCH_TRANSACTION_IN_DATA, function (context, filters) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].post("api/transactions/in", filters).then(function (_ref) {
      var data = _ref.data;
      context.commit(_const["default"].SET_TRANSACTION_IN_DATA, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref2) {
      var response = _ref2.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].FETCH_TRANSACTION_OUT_DATA, function (context, filters) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].post("api/transactions/out", filters).then(function (_ref3) {
      var data = _ref3.data;
      context.commit(_const["default"].SET_TRANSACTION_OUT_DATA, {
        value: data
      });
      console.log(_const["default"].GET_TRANSACTION_OUT_DATA, data);
      resolve(data);
    })["catch"](function (_ref4) {
      var response = _ref4.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].FETCH_TRANSACTION_IN, function (context, id) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/transactions/in/" + id).then(function (_ref5) {
      var data = _ref5.data;
      context.commit(_const["default"].SET_TRANSACTION_IN, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref6) {
      var response = _ref6.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].FETCH_TRANSACTION_OUT, function (context, id) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/transactions/out/" + id).then(function (_ref7) {
      var data = _ref7.data;
      context.commit(_const["default"].SET_TRANSACTION_OUT, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref8) {
      var response = _ref8.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].FETCH_TRANSACTION_DETAILS, function (context, id) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/transactions_in/details/" + id).then(function (_ref9) {
      var data = _ref9.data;
      context.commit(_const["default"].SET_TRANSACTION_DETAILS, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref10) {
      var response = _ref10.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].FETCH_TRANSACTION_OUT_DETAILS, function (context, id) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/transactions_out/details/" + id).then(function (_ref11) {
      var data = _ref11.data;
      context.commit(_const["default"].SET_TRANSACTION_DETAILS, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref12) {
      var response = _ref12.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].STORE_TRANSACTION_IN, function (context, data) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].post("api/transactions_in/store", data).then(function (_ref13) {
      var data = _ref13.data;
      context.commit(_const["default"].SET_TRANSACTION_IN, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref14) {
      var response = _ref14.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].STORE_TRANSACTION_OUT, function (context, data) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].post("api/transactions_out/store", data).then(function (_ref15) {
      var data = _ref15.data;
      context.commit(_const["default"].SET_TRANSACTION_OUT, {
        value: data
      });
      resolve(data);
    })["catch"](function (_ref16) {
      var response = _ref16.response;
      console.log(response);
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].CLEAR_TRANSACTION_IN, function (context) {
  return new Promise(function (resolve, reject) {
    var data = {
      id: null,
      waybill_no: null,
      fromable_type: null,
      fromable_id: null,
      warehouse_id: null,
      date: null
    };
    context.commit(_const["default"].SET_TRANSACTION_IN, {
      value: data
    });
    context.commit(_const["default"].SET_TRANSACTION_DETAILS, []);
    resolve();
    reject();
  });
}), _defineProperty(_actions, _const["default"].CLEAR_TRANSACTION_OUT, function (context) {
  return new Promise(function (resolve, reject) {
    var data = {
      id: null,
      waybill_no: null,
      toable_type: null,
      toable_id: null,
      warehouse_id: null,
      date: null
    };
    context.commit(_const["default"].SET_TRANSACTION_OUT, {
      value: data
    });
    context.commit(_const["default"].SET_TRANSACTION_OUT_DETAILS, []);
    resolve();
    reject();
  });
}), _defineProperty(_actions, _const["default"].STORE_TRANSACTION_DETAIL_ITEM, function (context, data) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].post("api/transaction_details/store", data).then(function (_ref17) {
      var data = _ref17.data;

      if (data.error) {
        toastr.options.preventDuplicates = true;
        toastr.options.progressBar = true;
        toastr.error(data.error);
        reject(response);
      } else {
        context.commit(_const["default"].CLEAR_TRANSACTION_DETAIL_ITEM, data);
      }

      resolve(data);
    })["catch"](function (response) {
      reject(response);
    });
  });
}), _defineProperty(_actions, _const["default"].DELETE_TRANSACTION_DETAIL_ITEM, function (context, data) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/transaction_details/delete/" + data.id).then(function (_ref18) {
      var data = _ref18.data;
      console.log('data', data);
      context.commit(_const["default"].CLEAR_TRANSACTION_DETAIL_ITEM, data);
      resolve(data);
    })["catch"](function (_ref19) {
      var error = _ref19.error;
      console.log(error);
      reject(data);
    });
  });
}), _defineProperty(_actions, _const["default"].DELETE_TRANSACTION_IN, function (context, data) {
  console.log('delte transaction in data id', data.id);
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/transactions_in/delete/" + data.id).then(function (_ref20) {
      var data = _ref20.data;
      resolve(data);
    })["catch"](function (_ref21) {
      var error = _ref21.error;
      console.log(error);
      reject(data);
    });
  });
}), _defineProperty(_actions, _const["default"].DELETE_TRANSACTION_OUT, function (context, data) {
  return new Promise(function (resolve, reject) {
    _api["default"].setHeader();

    _api["default"].get("api/transactions_out/delete/" + data.id).then(function (_ref22) {
      var data = _ref22.data;
      resolve(data);
    })["catch"](function (_ref23) {
      var error = _ref23.error;
      console.log(error);
      reject(data);
    });
  });
}), _actions);
var _default = {
  getters: getters,
  actions: actions,
  mutations: mutations,
  state: state
};
exports["default"] = _default;