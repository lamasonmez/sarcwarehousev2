export default {
    /** Mutations */
        SET_MATERIALS_DATA:"setMaterialData",
        SET_MATERIAL_ITEM : "setMaterialItem",
        CLEAR_MATERIAL_ITEM :"clearMaterialItem",

    /** Getters */
        GET_MATERIAL_DATA:"getMaterialData",
        GET_MATERIAL_ITEM : "getMaterialItem",

    /** Actions */
        FETCH_MATERIAL_DATA:"fetchMaterialData",
        DELETE_MATERIAL_ITEM :"deleteMaterialItem",
        STORE_MATERIAL_ITEM: "storeMaterialItem",
}
    