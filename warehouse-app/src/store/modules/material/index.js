import ApiService from "@/api/api.service";
import constants from  './const';

const state = {
   materials:[],
   material_item:{
       id:null,
       sector_id:null,
       name:{
           en:null,
           ar:null
       },
       code:'0000',
   },
  
};

const getters = {
    [constants.GET_MATERIAL_DATA] : state => {
        return state.materials;
    },
    [constants.GET_MATERIAL_ITEM] : state => {
        return state.material_item;
    },
};

const mutations = {
    [constants.SET_MATERIALS_DATA] (state,data) {
        state.materials = data.value;
    },
    [constants.SET_MATERIAL_ITEM] (state,data) {
        state.material_item = data.value;
    },
    
    [constants.CLEAR_MATERIAL_ITEM ] (state,data) {
        state.material_item = {
            id:null,
            sector_id:null,
            name:{
                en:null,
                ar:null
            },
            code:' ',
        };
    },
};

const actions = {
    [constants.FETCH_MATERIAL_DATA](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/materials")
            .then(({data}) => {
                context.commit(constants.SET_MATERIALS_DATA,{value:data});
                resolve(data);
            })
            .catch(({response}) => {
                console.log(response);
                reject(response);
            });
        });
    },
    [constants.STORE_MATERIAL_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/materials/store",data)
                .then(({data}) => {
                    if(data.error || data.errors){
                        toastr.options.preventDuplicates = true;
                        toastr.options.progressBar = true;
                        toastr.error(data.error);
                        reject(response);
                    }
                    else{
                        context.commit(constants.CLEAR_MATERIAL_ITEM);
                        resolve(data);
                    }
                    
                })
                .catch(function(error){
                    if(error.response.status==422){
                        toastr.options.preventDuplicates = true;
                        toastr.options.progressBar = true;
                        console
                        for(let e in error.response.data.errors){
                            toastr.error(error.response.data.errors[e]);
                        }
                    }
                    reject(data);
                });
        });
    },
    [constants.DELETE_MATERIAL_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/materials/delete/"+data.id)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_MATERIAL_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
  
};

export default {
    getters,
    actions,
    mutations,
    state
}
