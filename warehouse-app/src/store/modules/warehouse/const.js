export default {
    /** Mutations */
        SET_WAREHOUSES_DATA : "setWarehousesData",
        SET_WAREHOUSE_REPORT : "setWarehouseReport",
    /** Getters */
        GET_WAREHOUSES_DATA : "getWarehousesData",
        GET_WAREHOUSE_REPORT : "getWarehouseReport",

    /** Actions */
        FETCH_WAREHOUSES_DATA : "fetchWarehousesData",
        EXPORT_WAREHOUSE_REPORTS: "exportWarehouseReport",
        FETCH_WAREHOUSE_REPORT : "fetchWarehouseReport",

    }
    