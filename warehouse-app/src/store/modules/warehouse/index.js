import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"

import constants from  './const';

const state = {
   warehouses_data:null,
   warehouse_report:[],
};

const getters = {
    [constants.GET_WAREHOUSES_DATA] : state => {
        return state.warehouses_data;
    },
    [constants.GET_WAREHOUSE_REPORT] : state=>{
        return state.warehouse_report;
    }
};

const mutations = {
    [constants.SET_WAREHOUSES_DATA] (state,data) {
        state.warehouses_data = data.value;
    },
    [constants.SET_WAREHOUSE_REPORT] (state,data){
        state.warehouse_report  = data.value;
    }
};

const actions = {
    [constants.FETCH_WAREHOUSES_DATA](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            let url = "api/warehouses";
            data!=null ? url = url+"/"+data.type+"/"+data.id:'';
            ApiService.get(url)
                .then(({data}) => {
                    context.commit(constants.SET_WAREHOUSES_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.EXPORT_WAREHOUSE_REPORTS](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/exports/warehouse-report/"+data.type,data.filters)
                .then(({data}) => {
                    var fileLink = document.createElement('a');
                    console.log('data',data)
                    fileLink.href = data.path;
                    fileLink.setAttribute('download', 'warehouse_report.'+data.type);
                    document.body.appendChild(fileLink);
                    console.log('fileLink',fileLink);
                    fileLink.click();
                    resolve(data);
                })
                .catch(({response}) => {
                    toastr.options.preventDuplicates = true;
                    toastr.options.progressBar = true;
                    toastr.error("لقد حدث خطأ ما , الرجاء التواصل مع مدير الموقع");
                    console.log('error = ',response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_WAREHOUSE_REPORT](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            let url = "api/warehouse-report";
            ApiService.post(url,data)
                .then(({data}) => {
                    context.commit(constants.SET_WAREHOUSE_REPORT,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
