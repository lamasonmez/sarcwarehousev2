"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  /** Mutations */
  SET_WAREHOUSES_DATA: "setWarehousesData",

  /** Getters */
  GET_WAREHOUSES_DATA: "getWarehousesData",

  /** Actions */
  FETCH_WAREHOUSES_DATA: "fetchWarehousesData"
};
exports["default"] = _default;