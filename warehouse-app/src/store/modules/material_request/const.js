export default {
    /** Mutations */
        SET_MATERIAL_PENDING_REQUESTS: "setMaterialPendingRequests",
        SET_MATERIAL_REQUEST_ITEM : "setMaterialRequestItem",
        CLEAR_MATERIAL_REQUEST_ITEM :"clearMaterialRequestItem",

    /** Getters */
        GET_MATERIAL_PENDING_REQUESTS: "getMaterialPendingRequests",
        GET_MATERIAL_REQUEST_ITEM : "getMaterialRequestItem",

    /** Actions */
        FETCH_MATERIAL_PENDING_REQUESTS: "fetchMaterialPendingRequests",
        FETCH_MATERIAL_REQUEST_ITEM: "fetchMaterialRequestItem",
        STORE_MATERIAL_REQUEST_ITEM: "storeMaterialRequestItem",
}
    