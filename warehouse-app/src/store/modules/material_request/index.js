import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"
import {VUE_APP_API_URL} from "@/config";
import constants from  './const';
import { toDisplayString } from "vue";

const state = {
   material_request_item:{
       id:null,
       donor_material_id:null,
       user_id:null,
       status:null,
       donor_material:null,
       user:null
   },
   pending_material_requests:[]
  
};

const getters = {
    [constants.GET_MATERIAL_REQUEST_ITEM] : state => {
        return state.material_request_item;
    },
    [constants.GET_MATERIAL_PENDING_REQUESTS] : state => {
        return state.pending_material_requests;
    },
};

const mutations = {
    [constants.SET_MATERIAL_REQUEST_ITEM] (state,data) {
        state.material_request_item = data;
    },
    [constants.SET_MATERIAL_PENDING_REQUESTS] (state,data) {
        state.pending_material_requests = data.value;
    },
    [constants.CLEAR_MATERIAL_REQUEST_ITEM ] (state,data) {
        status.material_request_item={
            id:null,
            donor_material_id:null,
            user_id:null,
            status:null,
            donor_material:null,
            user:null
        };
    },
};

const actions = {
    [constants.STORE_MATERIAL_REQUEST_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/material_request/store",data)
                .then(({data}) => {
                        //context.commit(constants.CLEAR_MATERIAL_REQUEST_ITEM);
                        resolve(data);
                    
                })
                .catch(function(error){
                    toastr.error(error);
                    reject(data);
                });
        });
    },
    [constants.FETCH_MATERIAL_PENDING_REQUESTS](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/material_request/pending")
                .then(({data}) => {
                    context.commit(constants.SET_MATERIAL_PENDING_REQUESTS,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.FETCH_MATERIAL_REQUEST_ITEM](context,id) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/material_request/"+id)
                .then(({data}) => {
                    context.commit(constants.SET_MATERIAL_REQUEST_ITEM,data)
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    
  
};

export default {
    getters,
    actions,
    mutations,
    state
}
