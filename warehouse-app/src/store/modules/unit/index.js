import ApiService from "@/api/api.service";
import JwtService from "@/services/jwt.service"

import constants from  './const';

const state = {
   units:null,
   unit_item:{
       id:null,
       name:{
           ar:null,
           en:null
       }
   }
};

const getters = {
    [constants.GET_UNITS_DATA] : state => {
        return state.units;
    },
    [constants.GET_UNIT_ITEM] : state => {
        return state.unit_item;
    },
};

const mutations = {
    [constants.SET_UNITS_DATA] (state,data) {
        state.units = data.value;
    },
    [constants.SET_UNIT_ITEM] (state,data) {
        state.unit_item = data.value;
    },
    [constants.CLEAR_UNIT_ITEM] (state) {
        state.unit_item = {
            id:null,
            name:{
                ar:null,
                en:null
            }
        };
    },
};

const actions = {
    [constants.FETCH_UNITS_DATA](context) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/units")
                .then(({data}) => {
                    context.commit(constants.SET_UNITS_DATA,{value:data});
                    resolve(data);
                })
                .catch(({response}) => {
                    console.log(response);
                    reject(response);
                });
        });
    },
    [constants.STORE_UNIT_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.post("api/units/store",data)
                .then(({data}) => {
                    console.log('data',data);
                    context.commit(constants.CLEAR_UNIT_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
    [constants.DELETE_UNIT_ITEM](context,data) {
        return new Promise((resolve, reject) => {
            ApiService.setHeader();
            ApiService.get("api/units/delete/"+data.id)
                .then(({data}) => {
                    context.commit(constants.CLEAR_UNIT_ITEM,data);
                    resolve(data);
                })
                .catch(({error}) => {
                    console.log(error);
                    reject(data);
                });
        });
    },
};

export default {
    getters,
    actions,
    mutations,
    state
}
