export default {
    /** Mutations */
        SET_UNITS_DATA : "setUnitsData",
        SET_UNIT_ITEM : "setUnitItem",
        CLEAR_UNIT_ITEM :"clearUnitItem",
    /** Getters */
        GET_UNITS_DATA : "getUnitsData",
        GET_UNIT_ITEM : "getUnitItem",

    /** Actions */
        FETCH_UNITS_DATA : "fetchUnitsData",
        DELETE_UNIT_ITEM :"deleteUnitItem",
        STORE_UNIT_ITEM: "storeUnitItem",
    }
    