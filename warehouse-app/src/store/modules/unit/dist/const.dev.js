"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  /** Mutations */
  SET_UNITS_DATA: "setUnitsData",

  /** Getters */
  GET_UNITS_DATA: "getUnitsData",

  /** Actions */
  FETCH_UNITS_DATA: "fetchUnitsData"
};
exports["default"] = _default;