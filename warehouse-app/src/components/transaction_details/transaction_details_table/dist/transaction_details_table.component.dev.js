"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vuex = require("vuex");

var _const = _interopRequireDefault(require("@/store/const"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = {
  name: "TransactionDetailsTable",
  components: {},
  props: ['id', 'refreshDetails', 'type'],
  data: function data() {
    return {
      loading: false
    };
  },
  computed: _objectSpread({}, (0, _vuex.mapGetters)({
    user: _const["default"].authConstants.GET_CURRENT_USER,
    data: _const["default"].transactionsConstants.GET_TRANSACTION_DETAILS
  })),
  watch: {
    id: function id(newVal, oldVal) {
      var _this = this;

      if (this.type == 'in') {
        if (newVal != 0 && newVal != null && newVal != '') {
          this.loading = true;
          this.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_DETAILS, newVal).then(function () {
            return _this.loading = false;
          });
        }
      } else if (this.type == 'out') {
        if (newVal != 0 && newVal != null && newVal != '') {
          this.loading = true;
          this.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS, newVal).then(function () {
            return _this.loading = false;
          });
        }
      }
    },
    refreshDetails: function refreshDetails(newVal, oldVal) {
      var _this2 = this;

      if (newVal == true) {
        this.loading = true;

        if (this.type == "in") {
          this.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_DETAILS, this.id).then(function () {
            _this2.loading = false;

            _this2.$emit('refreshed');
          });
        } else if (this.type == 'out') {
          this.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS, this.id).then(function () {
            _this2.loading = false;

            _this2.$emit('refreshed');
          });
        }
      }
    }
  },
  methods: {
    showDetailsForm: function showDetailsForm() {
      this.$emit('show-detail-form');
    },
    editDetailsItem: function editDetailsItem(item) {
      this.$store.commit(_const["default"].transactionsConstants.SET_TRANSACTION_DETAIL_ITEM, {
        value: item
      });
      this.showDetailsForm();
    },
    deleteDetailsItem: function deleteDetailsItem(item) {
      var vm = this;
      Swal.fire({
        title: "هل انت متأكد؟",
        text: "لن تستطيع التراجع عن هذه العملية",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "حذف",
        cancelButtonText: "إلفاء"
      }).then(function (result) {
        if (result.value) {
          vm.$store.dispatch(_const["default"].transactionsConstants.DELETE_TRANSACTION_DETAIL_ITEM, item).then(function () {
            toastr.options.progressBar = true;
            toastr.success('تم حذف المادة بنجاح');
            vm.loading = true;

            if (vm.type == 'in') {
              vm.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_DETAILS, item.transactionable_id).then(function () {
                return vm.loading = false;
              })["catch"](function () {
                return console.log('error in delete details item');
              });
            } else if (vm.type == "out") {
              vm.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS, item.transactionable_id).then(function () {
                return vm.loading = false;
              })["catch"](function () {
                return console.log('error in delete details item');
              });
            }
          })["catch"](function () {
            toastr.options.progressBar = true;
            toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
          });
        }
      });
    }
  },
  mounted: function mounted() {}
};
exports["default"] = _default;