import { mapGetters } from 'vuex'
import constants from '@/store/const'

export default {
    name:"TransactionDetailsTable",
    components:{
       
    },
    props:{
        isTransferIn:{
            type:Boolean,
            required:false,
            default:false
        },
        id:{
            type:Number,
            required:true,
        },
        refreshDetails:{
            type:Boolean,
            required:false,
        },
        type:{
            type:String,
            required:true
        }
    },
    data() {
        return {
            loading:false,
        }
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            data:constants.transactionsConstants.GET_TRANSACTION_DETAILS
        })
      },

      watch: { 
        id: function(newVal, oldVal) {
            if(this.type=='in'){
                if(newVal!=0 && newVal!=null && newVal!=''){
                    this.loading = true;
                    this.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_DETAILS,newVal)
                                .then(()=>this.loading=false);
                }
            }
            else if(this.type=='out'){
                if(newVal!=0 && newVal!=null && newVal!=''){
                    this.loading = true;
                    this.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS,newVal)
                                .then(()=>this.loading=false);
                }
            }
            
           
      },
      refreshDetails:function(newVal,oldVal){
        if(newVal==true){
          this.loading = true;
          if(this.type=="in"){
            this.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_DETAILS,this.id)
            .then(()=>{
                this.loading=false;
                this.$emit('refreshed')
            });
          }
          else if(this.type=='out'){
            this.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS,this.id)
            .then(()=>{
                this.loading=false;
                this.$emit('refreshed')
            });
          }
          
        }
    }
    },
    methods:{
        showDetailsForm:function(){
            this.$emit('show-detail-form');
        },
        editDetailsItem:function(item){
            this.$store.commit(constants.transactionsConstants.SET_TRANSACTION_DETAIL_ITEM,{value:item});
            this.showDetailsForm();
        },
        deleteDetailsItem:function(item){
            let vm =this;
            Swal.fire({
                title: "هل انت متأكد؟",
                text: "لن تستطيع التراجع عن هذه العملية",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "حذف",
                cancelButtonText: "إلفاء"
            }).then(function(result) {
                if(result.value){
                    vm.$store.dispatch(constants.transactionsConstants.DELETE_TRANSACTION_DETAIL_ITEM,item).then(()=>{
                        toastr.options.progressBar = true;
                        toastr.success('تم حذف المادة بنجاح');
                        vm.loading = true;
                        if(vm.type=='in'){
                            vm.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_DETAILS,item.transactionable_id)
                            .then(()=>vm.loading=false)
                            .catch(()=>console.log('error in delete details item'));
                        }
                        else if(vm.type=="out"){
                            vm.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS,item.transactionable_id)
                                    .then(()=>vm.loading=false)
                                    .catch(()=>console.log('error in delete details item'));
                        }
                    }).catch(()=>{
                        toastr.options.progressBar = true;
                        toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
                    });
                }
               
            });
        }
    },
    mounted(){
       
    },
}