"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _validators = require("@vuelidate/validators");

var _default = {
  transaction_detail: {
    quantity: {
      required: _validators.required,
      numeric: _validators.numeric,
      $autoDirty: true
    },
    donor_material_id: {
      required: _validators.required,
      $autoDirty: true
    }
  }
};
exports["default"] = _default;