"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

require("toastr");

var _DonorsSelect = _interopRequireDefault(require("@/components/shared/donors-select/DonorsSelect.vue"));

var _DonorMaterialsSelect = _interopRequireDefault(require("@/components/shared/donor-materials-select/DonorMaterialsSelect.vue"));

var _UnitSelect = _interopRequireDefault(require("@/components/shared/unit-select/UnitSelect.vue"));

var _CtnSelect = _interopRequireDefault(require("@/components/shared/ctn-select/CtnSelect.vue"));

var _vuex = require("vuex");

var _const = _interopRequireDefault(require("@/store/const"));

var _transaction_details_form = _interopRequireDefault(require("./transaction_details_form.validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = {
  name: 'TransactionDetailsForm',
  components: {
    DonorsSelect: _DonorsSelect["default"],
    DonorMaterialsSelect: _DonorMaterialsSelect["default"],
    UnitSelect: _UnitSelect["default"],
    CtnSelect: _CtnSelect["default"]
  },
  data: function data() {
    return {
      donor: null,
      loading: false,
      loading_store: false
    };
  },
  validations: function validations() {
    return _transaction_details_form["default"];
  },
  props: {
    transaction: {
      type: Object,
      required: false
    },
    transaction_type: {
      type: String,
      required: false
    }
  },
  computed: _objectSpread({}, (0, _vuex.mapGetters)({
    user: _const["default"].authConstants.GET_CURRENT_USER,
    donor_material: _const["default"].donorMaterialConstants.GET_DONOR_MATERIAL,
    transaction_detail: _const["default"].transactionsConstants.GET_TRANSACTION_DETAIL_ITEM,
    ctns: _const["default"].ctnConstants.GET_CTN_DATA
  })),
  watch: {
    transaction: function transaction(newVal, oldVal) {
      if (newVal != 0) {
        this.transactionId = newVal.id;
        this.transaction_detail.transactionable_id = newVal.id;
      }
    },
    transaction_type: function transaction_type(newVal, oldVal) {
      if (newVal != null) {
        this.transaction_detail.transactionable_type = newVal;
      }
    }
  },
  methods: {
    storeDetail: function storeDetail(transaction_detail) {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$error) {
        toastr.options.preventDuplicates = true;
        toastr.error('Please Fill in the required fileds');
        return;
      }

      this.loading_store = true;
      this.$store.dispatch(_const["default"].transactionsConstants.STORE_TRANSACTION_DETAIL_ITEM, transaction_detail).then(function (response) {
        _this.loading_store = false;
        toastr.options.preventDuplicates = true;
        toastr.success('تم الحفظ بنجاح');

        if (_this.transaction_type == 'App\\Models\\TransactionIn') {
          _this.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_DETAILS, response.transactionable_id);
        } else {
          _this.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS, response.transactionable_id);
        }
      })["catch"](function () {
        _this.loading_store = false;
      });
    },
    donorChagned: function donorChagned(param) {
      this.$store.dispatch(_const["default"].donorMaterialConstants.FETCH_DONOR_MATERIALS_DATA, param);
    },
    donorMaterialChagned: function donorMaterialChagned(param) {
      this.$v.transaction_detail.donor_material_id.$model = param;
      this.$v.transaction_detail.donor_material_id.$touch();
      var vm = this;
      this.$store.dispatch(_const["default"].donorMaterialConstants.FETCH_DONOR_MATERIAL, param).then(function (response) {
        vm.transaction_detail.material_donor = response;
        vm.$store.commit(_const["default"].ctnConstants.SET_CTN_DATA, response.ctns);
      });
      this.$store.dispatch(_const["default"].ctnConstants.FETCH_CTN_DATA, param);
      this.transaction_detail.donor_material_id = param;
    },
    ctnChanged: function ctnChanged(param) {
      this.transaction_detail.ctn_id = param;
    },
    hideDetailsForm: function hideDetailsForm() {
      this.$emit('hide-detail-form');
    }
  },
  mounted: function mounted() {
    var vm = this;
    var card = new KTCard('kt_card_2');
    $('#kt_datepicker_2').datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      format: 'yyyy-mm-dd',
      orientation: "bottom left"
    }).on('changeDate', function (selected) {
      vm.transaction_detail.expiration_date = moment(selected.date).format('YYYY-MM-DD');
      $('#expiration_date').val(moment(selected.date).format('YYYY-MM-DD'));
    });
    if (this.transaction_detail.material_donor != null) this.$store.commit(_const["default"].ctnConstants.SET_CTN_DATA, {
      value: this.transaction_detail.material_donor.ctns
    });
    this.transaction_detail.warehouse_id = this.user.warehouse_id;
    this.transaction_detail.transactionable_type = this.transaction_type;
    this.transaction_detail.transactionable_id = this.transaction.id;
  }
};
exports["default"] = _default;