import 'toastr';
import DonorsSelect from '@/components/shared/donors-select/DonorsSelect.vue'
import DonorMaterialsSelect from '@/components/shared/donor-materials-select/DonorMaterialsSelect.vue'
import UnitSelect from '@/components/shared/unit-select/UnitSelect.vue'
import CtnSelect from '@/components/shared/ctn-select/CtnSelect.vue'
import { mapActions, mapGetters } from 'vuex'
import constants from '@/store/const'
import rules from  './transaction_details_form.validator';

export default {
    name:'TransactionDetailsForm',

    components:{
        DonorsSelect,
        DonorMaterialsSelect,
        UnitSelect,
        CtnSelect,
        
    },

    data() {
        return {
            donor:null,
            loading:false,
            loading_store:false,
            displayMaterialModal:false,
           
        }
    },
    validations() {
        return rules
    },
    props:{
        transaction:{
            type:Object,
            required:false
        },
        transaction_type:{
            type:String,
            required:false
        },
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            donor_material: constants.donorMaterialConstants.GET_DONOR_MATERIAL,
            transaction_detail:constants.transactionsConstants.GET_TRANSACTION_DETAIL_ITEM,
            ctns: constants.ctnConstants.GET_CTN_DATA,
        })
      },
      watch:{
        transaction: function(newVal, oldVal) {
            if(newVal!=0){
                this.transactionId = newVal.id;
                this.transaction_detail.transactionable_id = newVal.id;
            }
        },
        transaction_type: function(newVal, oldVal) {
            if(newVal!=null){
                this.transaction_detail.transactionable_type = newVal;
            }
        }
      },
    methods:{
        storeDetail:function(transaction_detail){
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
            this.$store.dispatch(constants.transactionsConstants.STORE_TRANSACTION_DETAIL_ITEM,transaction_detail)
            .then((response)=>{
                this.loading_store=false;
                toastr.options.preventDuplicates = true;
                toastr.success('تم الحفظ بنجاح');
                if(this.transaction_type=='App\\Models\\TransactionIn'){
                    this.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_DETAILS,response.transactionable_id);
                }else{
                    this.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS,response.transactionable_id);

                }
            }).catch(()=>{
                this.loading_store=false;
            });
        },
        donorChagned:function(param){
            this.$store.dispatch(constants.donorMaterialConstants.FETCH_DONOR_MATERIALS_DATA,param)
        },
        donorMaterialChagned:function(param){
            this.$v.transaction_detail.donor_material_id.$model = param;
            this.$v.transaction_detail.donor_material_id.$touch();
            let vm = this;
            this.$store.dispatch(constants.donorMaterialConstants.FETCH_DONOR_MATERIAL,param)
                        .then((response)=>{
                            vm.transaction_detail.material_donor = response;
                            vm.$store.commit(constants.ctnConstants.SET_CTN_DATA,response.ctns);
                        });
            this.$store.dispatch(constants.ctnConstants.FETCH_CTN_DATA,param);
            this.transaction_detail.donor_material_id = param;
            
        },
        ctnChanged:function(param){
            this.transaction_detail.ctn_id = param;
        },
        hideDetailsForm:function(){
            this.$emit('hide-detail-form');

        }
       
    },

    mounted(){
        let vm=this;
        var card = new KTCard('kt_card_2');
        $('#kt_datepicker_2').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            orientation: "bottom left",
        })
        .on('changeDate',function(selected) {
            vm.transaction_detail.expiration_date=moment(selected.date).format('YYYY-MM-DD');
            $('#expiration_date').val(moment(selected.date).format('YYYY-MM-DD'));
        });
        if(this.transaction_detail.material_donor!=null)
            this.$store.commit(constants.ctnConstants.SET_CTN_DATA,{value:this.transaction_detail.material_donor.ctns});
        this.transaction_detail.warehouse_id = this.user.warehouse_id;
        this.transaction_detail.transactionable_type = this.transaction_type;
        this.transaction_detail.transactionable_id = this.transaction.id;
    }

}