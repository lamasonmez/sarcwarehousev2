import {required,numeric,minValue ,withParams,helpers} from '@vuelidate/validators'
import * as moment from 'moment';

const currentDate = moment(new Date()).startOf('day').format('YYYY-MM-DD');
//const minDate = minValue.withParams({minDate: currentDate.format('YYYY-MM-DD')}, value => moment(value, 'YYYY-MM-DD', true).isSameOrAfter(currentDate))
export default {
    transaction_detail:{
        quantity:{
            required:helpers.withMessage('The quantity field is required', required),
            numeric,
            $autoDirty:true,
           

        },
        donor_material_id:{
            required,
            $autoDirty:true
        },
        expiration_date:{
            minValue:helpers.withMessage(' الرجاء ادخال تاريخ صلاحية صالح ', value => value > currentDate) 

        }
    }
    
 
};
