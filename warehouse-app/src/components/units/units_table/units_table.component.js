import { mapGetters } from 'vuex'
import constants from '@/store/const'


export default {
    name:"UnitsTable",
    
    components:{
    },
    props:{
        data: Object,
        refreshData:Boolean
    },
    data() {
        return {
            loading:false,
        }
    },
    watch:{
        refreshData:function(newVal,oldVal){
            if(newVal==true){
              this.loading = true;
                //dispatch actions
                this.$store.dispatch({
                    type:constants.UnitsConstants.FETCH_UNITS_DATA,
                }
                )
            .then(()=>{
                this.loading=false;
                this.$emit('refreshed')
            });
            }
        }
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
        })
      },

    methods:{
        editItem:function(item){
            this.$store.commit(constants.UnitsConstants.SET_UNIT_ITEM,{value:item});
        },
        deleteItem:function(item){
            let vm =this;
            Swal.fire({
                title: "هل انت متأكد؟",
                text: "لن تستطيع التراجع عن هذه العملية",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "حذف",
                cancelButtonText: "إلفاء"
            }).then(function(result) {
                if(result.value){
                    vm.$store.dispatch(constants.UnitsConstants.DELETE_UNIT_ITEM,item).then(()=>{
                        toastr.options.progressBar = true;
                        toastr.success('تم حذف الوحدة بنجاح');
                        vm.$emit('get-data');
                    }).catch(()=>{
                        toastr.options.progressBar = true;
                        toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
                    });
                }
               
            });
        },
       
    },
    mounted(){
       
    },
}