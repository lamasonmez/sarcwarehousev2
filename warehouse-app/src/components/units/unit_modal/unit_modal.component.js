import { mapGetters } from 'vuex'
import constants from '@/store/const'
import rules from  './unit_form.validator';


export default {
    name:"UnitModal",
    data() {
        return {
            loading_store:false
        }
    },
    props:['showModal'],
    validations() {
        return rules
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            unit: constants.UnitsConstants.GET_UNIT_ITEM,
          
        })
      },
    methods:{
        storeItem:function(unit){
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
            let vm = this;
            this.$store.dispatch(constants.UnitsConstants.STORE_UNIT_ITEM,unit)
            .then((response)=>{
                $("#closeUnit").click();
                this.loading_store=false;
                toastr.options.preventDuplicates = true;
                toastr.success('تم الحفظ بنجاح');
                vm.$emit('refresh-data');
            }).catch(()=>{
                this.loading_store=false;
            });
            //dispatch store action
          
            
        },
    }
}