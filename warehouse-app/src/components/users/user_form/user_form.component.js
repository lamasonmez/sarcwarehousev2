import 'bootstrap-datepicker';
import 'toastr';
import AllWarehousesSelect from '@/components/shared/all-warehouses-select/AllWarehousesSelect.vue'
import { mapActions, mapGetters } from 'vuex'
import BranchesSelect from '@/components/shared/branches-select/BranchesSelect.vue'
import SubBranchesSelect from '@/components/shared/subbranches-select/SubBranchesSelect.vue'
import RolesSelect from '@/components/shared/roles-select/RolesSelect.vue'
import DonorsSelect from '@/components/shared/donors-select/DonorsSelect.vue'

import constants from '@/store/const'

import rules from  './user_form.validator';

export default {
    name:'UserForm',
   
    components:{
        AllWarehousesSelect,
        BranchesSelect,
        SubBranchesSelect,
        RolesSelect,
        DonorsSelect
    },

    data() {
        return {
            warehouse:null,
            loading:false,
            loading_store:false,
        }
    },

    computed: {
        ...mapGetters({
            user:constants.UsersConstants.GET_USER_ITEM,
            
        }),
        
      },

    methods:{
        ...mapActions({
            clearUSER:constants.UsersConstants.CLEAR_USER,
            getUser:constants.UsersConstants.FETCH_USER_ITEM,
            storeUser:constants.UsersConstants.STORE_USER_ITEM
        }),
        branch:function(param){
            this.user.branchable_id=param;
        },
        subBranch:function(param){
            this.user.branchable_id=param;
        },
        warehouseChanged:function(param){
            // this.user.warehouse_id = param;
            this.user.rolable_id=param;
            this.user.rolable_type='App\\Models\\Warehouse'
        },
        RoleChanged:function(param){
            this.user.role_id = param;
        },
        donor:function(param){
            this.user.rolable_id=param;
            this.user.rolable_type='App\\Models\\Donor'
        },
        store:function(user){
            let vm = this;
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
            this.storeUser(user)
            .then(()=>{
                this.loading_store=false;
                toastr.options.preventDuplicates = true;
                toastr.options.progressBar = true;
                toastr.success('تم الحفظ بنجاح');
            }).catch(()=>{
                toastr.options.preventDuplicates = true;
                toastr.options.progressBar = true;
                this.loading_store=false;
                toastr.error('لقد حدث خطأ ما يرجى المحاولة لاحقاً');
            });
        },
       
       
        resetUserForm:function(){
            this.loading=true;
            this.clearUSER(this.user).then(()=>{
                this.loading = false;
                
            });
        },
       
    },
    validations() {
        return rules
    },
    mounted(){
        let vm =this;
        var card = new KTCard('kt_card_1');
        if(this.$route.params.id){
            this.loading = true;
            this.getUser(this.$route.params.id)
                        .then(()=>{
                            this.loading=false;
                        });
        }
    }

}