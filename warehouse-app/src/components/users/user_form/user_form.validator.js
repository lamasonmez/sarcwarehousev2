import {required,maxLength,sameAs,requiredIf} from '@vuelidate/validators'

export default {
    user:{
        name:{
            required,
            $autoDirty:true
        },
        email:{
            required,
            $autoDirty:true
        },
        branchable_type:{
            required,
            $autoDirty:true
        },
        // password:{
        //     required: requiredIf(function(user){
        //         return this.user.id!=null
        //     }),
        //     $autoDirty:true
        // },
        // password_confirmation:{
        //     required: requiredIf(function(user){
        //         return this.password!=null
        //     }),
        //     sameAsPassword: sameAs(function() {
        //         return this.user.password.model;
        //     }),
        //     $autoDirty:true

        // }
    }
    
 
};
