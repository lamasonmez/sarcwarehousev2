import { mapGetters } from 'vuex'
import constants from '@/store/const'
import rules from  './subbranch_form.validator';
import BranchesSelect from '@/components/shared/branches-select/BranchesSelect.vue'


export default {
    name:"SubBranchModal",
    components:{
        BranchesSelect
    },
    data() {
        return {
            loading_store:false
        }
    },
    
    props:['showModal'],
    validations() {
        return rules
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            subbranch: constants.SubBranchesConstants.GET_SUBBRANCH_ITEM,
          
        })
      },
    methods:{
        storeItem:function(branch){
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
            let vm = this;
            this.$store.dispatch(constants.SubBranchesConstants.STORE_SUBBRANCH_ITEM,branch)
            .then((response)=>{
                $("#closeSubBranch").click();
                this.loading_store=false;
                toastr.options.preventDuplicates = true;
                toastr.success('تم الحفظ بنجاح');
                vm.$emit('refresh-data');
            }).catch(()=>{
                this.loading_store=false;
            });
            //dispatch store action    
        },
        branch:function(param){
            this.subbranch.branch_id = param;
        },
    }
}