import {required} from '@vuelidate/validators'

export default {
    subbranch:{
        code:{
            required,
            $autoDirty:true
        },
        type:{
            required,
            $autoDirty:true
        },
        branch_id:{
            required,
            $autoDirty:true
        },
       
    }
    
 
};
