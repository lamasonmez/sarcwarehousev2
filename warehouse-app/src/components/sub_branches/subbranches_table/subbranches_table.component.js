import { mapGetters } from 'vuex'
import constants from '@/store/const'


export default {
    name:"SubBranchesTable",
    
    components:{
    },
    props:{
        data: Object,
        refreshData:Boolean
    },
    data() {
        return {
            loading:false,
        }
    },
    watch:{
        refreshData:function(newVal,oldVal){
            if(newVal==true){
              this.loading = true;
                //dispatch actions
                this.$store.dispatch({
                    type:constants.SubBranchesConstants.FETCH_SUBBRANCHES_DATA,
                }
                )
            .then(()=>{
                this.loading=false;
                this.$emit('refreshed')
            });
            }
        }
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
        })
      },

    methods:{
        addItem:function(){
            this.$store.commit(constants.SubBranchesConstants.CLEAR_SUBRANCH_ITEM);
        },
        editItem:function(item){
            this.$store.commit(constants.SubBranchesConstants.SET_SUBBRANCH_ITEM,item);
        },
        deleteItem:function(item){
            let vm =this;
            Swal.fire({
                title: "هل انت متأكد؟",
                text: "لن تستطيع التراجع عن هذه العملية",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "حذف",
                cancelButtonText: "إلفاء"
            }).then(function(result) {
                if(result.value){
                    vm.$store.dispatch(constants.SubBranchesConstants.DELETE_SUBBRANCH_ITEM,item).then(()=>{
                        toastr.options.progressBar = true;
                        toastr.success('تم الحذف  بنجاح');
                        vm.$emit('get-data');
                    }).catch(()=>{
                        toastr.options.progressBar = true;
                        toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
                    });
                }
               
            });
        },
       
    },
    mounted(){
       
    },
}