"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

require("bootstrap-datepicker");

require("toastr");

var _DonorsSelect = _interopRequireDefault(require("@/components/shared/donors-select/DonorsSelect.vue"));

var _AllWarehousesSelect = _interopRequireDefault(require("@/components/shared/all-warehouses-select/AllWarehousesSelect.vue"));

var _vuex = require("vuex");

var _const = _interopRequireDefault(require("@/store/const"));

var _transaction_in_form = _interopRequireDefault(require("./transaction_in_form.validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = {
  name: 'TransactionInForm',
  components: {
    DonorsSelect: _DonorsSelect["default"],
    AllWarehousesSelect: _AllWarehousesSelect["default"]
  },
  data: function data() {
    return {
      donor: null,
      warehouse: null,
      loading: false,
      loading_store: false
    };
  },
  computed: _objectSpread({}, (0, _vuex.mapGetters)({
    user: _const["default"].authConstants.GET_CURRENT_USER,
    transaction: _const["default"].transactionsConstants.GET_TRANSACTION_IN
  })),
  methods: {
    donorChagned: function donorChagned(param) {
      this.transaction.fromable_id = param;
      this.transaction.fromable_type = 'App\\Models\\Donor';
    },
    warehouseChanged: function warehouseChanged(param) {
      this.transaction.fromable_id = param;
      this.transaction.fromable_type = 'App\\Models\\Warehouse';
    },
    store: function store(transaction) {
      var _this = this;

      var vm = this;
      this.$v.$touch();

      if (this.$v.$error) {
        toastr.options.preventDuplicates = true;
        toastr.error('Please Fill in the required fileds');
        return;
      }

      this.loading_store = true;
      this.$store.dispatch(_const["default"].transactionsConstants.STORE_TRANSACTION_IN, transaction).then(function () {
        _this.loading_store = false;
        _this.transaction.waybill_image = _this.transaction.src;
        toastr.options.preventDuplicates = true;
        toastr.options.progressBar = true;
        toastr.success('تم الحفظ بنجاح');

        _this.$emit('refresh-details');
      })["catch"](function () {
        toastr.options.preventDuplicates = true;
        toastr.options.progressBar = true;
        _this.loading_store = false;
        toastr.error('لقد حدث خطأ ما يرجى المحاولة لاحقاً');
      });
    },
    onImageChange: function onImageChange(e) {
      var files = e.target.files || e.dataTransfer.files;
      if (!files.length) return;
      this.createImage(files[0]);
    },
    createImage: function createImage(file) {
      var image = new Image();
      var reader = new FileReader();
      var vm = this;

      reader.onload = function (e) {
        vm.transaction.waybill_image = e.target.result;
        vm.transaction.image = e.target.result;
        vm.transaction.src = e.target.result;
      };

      reader.readAsDataURL(file);
    },
    removeImage: function removeImage() {
      this.transaction.waybill_image = null;
      this.transaction.src = null;
      this.transaction.image = null;
    },
    showDetailsForm: function showDetailsForm() {
      this.$emit('show-detail-form');
    },
    resetTransactionInForm: function resetTransactionInForm() {
      var _this2 = this;

      this.loading = true;
      this.$store.dispatch(_const["default"].transactionsConstants.CLEAR_TRANSACTION_IN).then(function () {
        _this2.loading = false;
      });
    }
  },
  validations: function validations() {
    return _transaction_in_form["default"];
  },
  mounted: function mounted() {
    var _this3 = this;

    var vm = this;
    var card = new KTCard('kt_card_1');
    var avatar1 = new KTImageInput('kt_image_1');

    if (this.$route.params.id) {
      this.loading = true;
      this.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_IN, this.$route.params.id).then(function () {
        _this3.loading = false;
        _this3.transaction.waybill_image = _this3.transaction.src;
      });
    }

    this.transaction.warehouse_id = this.user.warehouse_id;
    $('#kt_datepicker_1').datepicker({
      rtl: KTUtil.isRTL(),
      todayHighlight: true,
      format: 'yyyy-mm-dd',
      orientation: "bottom left"
    }).on('changeDate', function (selected) {
      vm.transaction.date = moment(selected.date).format('YYYY-MM-DD');
      $('#date').val(moment(selected.date).format('YYYY-MM-DD'));
    });
  }
};
exports["default"] = _default;