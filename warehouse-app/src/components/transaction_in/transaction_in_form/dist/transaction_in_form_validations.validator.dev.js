"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = require("vue");

var _core = _interopRequireDefault(require("@vuelidate/core"));

var _validators = require("@vuelidate/validators");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var waybill_no = (0, _vue.ref)('');
var date = (0, _vue.ref)('');
var warehouse = (0, _vue.ref)('');
var donor = (0, _vue.ref)('');
var from = (0, _vue.ref)('');
var rules = {
  waybill_no: {
    required: _validators.required,
    maxLength: (0, _validators.maxLength)(6),
    autoDirty: true
  },
  date: {
    required: {
      $message: 'some stuff'
    },
    autoDirty: true
  },
  from: {
    required: _validators.required,
    autoDirty: true
  },
  donor: {
    required: (0, _validators.requiredIf)(function (model) {
      console.log('model from donor', model);
      return model.from == 'App\\Models\\Donor';
    }),
    autoDirty: true
  },
  warehouse: {
    required: (0, _validators.requiredIf)(function (model) {
      console.log('model from Warehouse', model);
      return model.from == 'App\\Models\\Warehouse';
    }),
    autoDirty: true
  }
};
var inFormValidator$ = (0, _core["default"])(rules, {
  waybill_no: waybill_no,
  date: date,
  from: from,
  donor: donor,
  warehouse: warehouse
});
var _default = {
  inFormValidator$: inFormValidator$,
  waybill_no: waybill_no,
  date: date,
  from: from,
  donor: donor,
  warehouse: warehouse
};
exports["default"] = _default;