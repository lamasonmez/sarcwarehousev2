import {required,maxLength,minLength,numeric} from '@vuelidate/validators'

export default {
    transaction:{
        waybill_no:{
            required,
            maxLength:maxLength(6),
            minLength:minLength(6),
            numeric,
            $autoDirty:true

        },
        date:{
            required,
            autoDirty:true

        },
        fromable_type:{
            required,
            autoDirty:true
        },
        fromable_id:{
            required,
            dirty:true,
        }
    }
    
 
};
