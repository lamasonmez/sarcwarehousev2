import 'bootstrap-datepicker';
import 'toastr';
import DonorsSelect from '@/components/shared/donors-select/DonorsSelect.vue'
import AllWarehousesSelect from '@/components/shared/all-warehouses-select/AllWarehousesSelect.vue'
import { mapActions, mapGetters } from 'vuex'
import constants from '@/store/const'

import rules from  './transaction_in_form.validator';

export default {
    name:'TransactionInForm',
    props:{
        isTransferIn:{
            type:Boolean,
            required:false,
            default:false,
        }
    },
    components:{
        DonorsSelect,
        AllWarehousesSelect
    },

    data() {
        return {
            donor:null,
            warehouse:null,
            loading:false,
            loading_store:false,
            check_waybill_loader:false,
            chek_waybill_messgae:'',
            waybill_valid:false
        }
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            transaction:constants.transactionsConstants.GET_TRANSACTION_IN
        }),
        
      },

    methods:{
        ...mapActions({
            clearTransaction:constants.transactionsConstants.CLEAR_TRANSACTION_IN,
            checkWaybillNumber:constants.transactionsConstants.CHECK_WAYBILL,
            getTransaction:constants.transactionsConstants.FETCH_TRANSACTION_IN,
            storeTransaction:constants.transactionsConstants.STORE_TRANSACTION_IN
        }),
        donorChagned:function(param){
                this.transaction.fromable_id = param;
                this.transaction.fromable_type = 'App\\Models\\Donor';
        },
        warehouseChanged:function(param){
            this.transaction.fromable_id = param;
            this.transaction.fromable_type = 'App\\Models\\Warehouse';
            
        },
        store:function(transaction){
            let vm = this;
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
                transaction.waybill_no = this.$v.transaction.waybill_no.$model;
                transaction.warehouse_id = this.user.warehouse_id;
            this.storeTransaction(transaction)
            .then(()=>{
                this.loading_store=false;
                this.transaction.waybill_image= this.transaction.src;
                toastr.options.preventDuplicates = true;
                toastr.options.progressBar = true;
                toastr.success('تم الحفظ بنجاح');
                this.$emit('refresh-details');
            }).catch(()=>{
                toastr.options.preventDuplicates = true;
                toastr.options.progressBar = true;
                this.loading_store=false;
                toastr.error('لقد حدث خطأ ما يرجى المحاولة لاحقاً');
            });
        },
        onImageChange:function(e) {
            let files = e.target.files || e.dataTransfer.files;
            if (!files.length)
              return;
            this.createImage(files[0]);
          },
          createImage:function(file) {
            let image = new Image();
            let reader = new FileReader();
            let vm = this;
      
            reader.onload = (e) => {
              vm.transaction.waybill_image = e.target.result;
              vm.transaction.image = e.target.result;
              vm.transaction.src = e.target.result;
            };
            reader.readAsDataURL(file);
          },
          removeImage:function(){
              this.transaction.waybill_image = null;
              this.transaction.src = null;
              this.transaction.image = null;
          },
          showDetailsForm:function(){
            this.$emit('show-detail-form');
        },
        resetTransactionInForm:function(){
            this.loading=true;
            this.clearTransaction(this.transaction).then(()=>{
                this.loading = false;
                
            });
        },
        checkWaybill:function(){
            if(this.$v.transaction.waybill_no.$invalid==false){
                this.check_waybill_loader = true;
                this.chek_waybill_messgae = '';
                this.checkWaybillNumber(this.$v.transaction.waybill_no.$model).then((response)=>{
                    this.check_waybill_loader = false;
                    this.chek_waybill_messgae = response.message;
                    response.status==500 ? this.waybill_valid=false : this.waybill_valid=true;
                })
                .catch(()=>{
                    this.check_waybill_loader = false;

                });
            }
        }
    },
    validations() {
        return rules
    },
    mounted(){
        let vm =this;
        var card = new KTCard('kt_card_1');
        var avatar1 = new KTImageInput('kt_image_1');
        if(this.$route.params.id){
            this.loading = true;
            this.getTransaction(this.$route.params.id)
                        .then(()=>{
                            this.loading=false;
                            this.transaction.waybill_image= this.transaction.src
                        });
        }
        this.transaction.warehouse_id = this.user? this.user.warehouse_id : null;
        $('#kt_datepicker_1').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            orientation: "bottom left",
           
            
        }).on('changeDate',function(selected) {
            vm.transaction.date=moment(selected.date).format('YYYY-MM-DD');
            $('#date').val(moment(selected.date).format('YYYY-MM-DD'));

        });

    }

}