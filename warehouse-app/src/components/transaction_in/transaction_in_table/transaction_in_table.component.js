import { mapActions, mapGetters } from 'vuex'
import constants from '@/store/const'
import Pagination from '@/components/shared/pagination/Pagination.vue'
export default {
    name:'TransactionInTable',
    components:{
        Pagination
    },
    data() {
        return {
        filters:[],
          imgs: '', // Img Url , string or Array of string
          visible: false,
          index: 0 // default: 0
        }
      },
    props:{
        loading: Boolean,
        data: Object
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,

        }),
        
      },
    methods:{
        AddNewTransactionIn:function(){
            this.$store.dispatch(constants.transactionsConstants.CLEAR_TRANSACTION_IN).then(()=>{
                this.$router.push({name: "AddEditIncome"});
            });
        },
        deleteTransactionIn:function(item){
            let vm =this;
            Swal.fire({
                title: "هل انت متأكد؟",
                text: "لن تستطيع التراجع عن هذه العملية",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "حذف",
                cancelButtonText: "إلفاء"
            }).then(function(result) {
                if(result.value){
                    vm.$store.dispatch(constants.transactionsConstants.DELETE_TRANSACTION_IN,item).then(()=>{
                        toastr.options.progressBar = true;
                        toastr.success('تم حذف الحركة بنجاح');
                        vm.$emit('get-transations');
                    }).catch(()=>{
                        toastr.options.progressBar = true;
                        toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
                    });
                }
               
            });
           
        },
        pagechanged(param){
                if(param.link){
                    this.$emit('get-transations',param);
                }
        },
        perpagechanged(param){
            if(param.link){
                this.$emit('get-transations',param);
            }
        },
        showSingle(src) {
            this.imgs = src
           
            this.show();
            console.log('this.imgs',this.imgs);
        },
        show() {
            this.visible = true
        },
        handleHide() {
            this.visible = false
        }
    }
}

