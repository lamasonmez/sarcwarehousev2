import 'select2'
import 'bootstrap-daterangepicker';
import constants from '@/store/const'
import { mapGetters } from 'vuex'

import DonorsSelect from '@/components/shared/donors-select/DonorsSelect.vue'
import AllWarehousesSelect from '@/components/shared/all-warehouses-select/AllWarehousesSelect.vue'

export default {
    name:'TransactionInFilters',
    
    data() {
        return {
            loading:true,
            filters:{
                waybill_no:'',
                warehouse:'',
                date_range:'',
                donor:'',
                donor_waybill_no:'',
            },
            
        }
    },

    components:{
        DonorsSelect,
        AllWarehousesSelect
    },

    computed: {
        ...mapGetters({
            warehouses: constants.warehouseConstants.GET_WAREHOUSES_DATA,
            donors: constants.donorConstants.GET_DONORS_DATA,
        })
      },

    methods:{
          search: function(){
              this.$emit("search",this.filters);
          },
          donor:function(param){
              this.filters.donor = param;
          },
          warehouse:function(param){
              this.filters.warehouse = param;
          },
      },
    
    mounted(){
        let vm = this;
        
        $('#kt_daterangepicker_2').daterangepicker({
            buttonClasses: 'btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
            }, function(start, end, label) {
                vm.filters.date_range=[start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD')]
            $('#kt_daterangepicker_2 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });

    
      }
   
}

