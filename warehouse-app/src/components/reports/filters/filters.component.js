import 'select2'
import 'bootstrap-daterangepicker';
import constants from '@/store/const'
import { mapGetters } from 'vuex'

import DonorsSelect from '@/components/shared/donors-select/DonorsSelect.vue'
import AllWarehousesSelect from '@/components/shared/all-warehouses-select/AllWarehousesSelect.vue'
import DonorMaterialsSelect from '@/components/shared/donor-materials-select/DonorMaterialsSelect.vue'
import UnitSelect from '@/components/shared/unit-select/UnitSelect.vue'
import BranchesSelect from '@/components/shared/branches-select/BranchesSelect.vue'
import SubBranchesSelect from '@/components/shared/subbranches-select/SubBranchesSelect.vue'
import TransfersSelect from '@/components/shared/transfers-select/TransferSelect.vue'
export default {
    name:'Filters',
    
    data() {
        return {
            export_loading:false,
            filters:{
                warehouse:'',
                date_range:'',
                donor:'',
                branch:'',
                transfer:'',
                unit:'',
                material:'',
                subbranch:'',
            },
            
        }
    },
    props:['loading'],
    components:{
        DonorsSelect,
        AllWarehousesSelect,
        DonorMaterialsSelect,
        UnitSelect,
        BranchesSelect,
        SubBranchesSelect,
        TransfersSelect

    },

    computed: {
        ...mapGetters({
            warehouses: constants.warehouseConstants.GET_WAREHOUSES_DATA,
            donors: constants.donorConstants.GET_DONORS_DATA,
            user:constants.authConstants.GET_CURRENT_USER,

        })
      },

    methods:{
          search: function(){
              this.$emit("search",this.filters);
          },
          donor:function(param){
              this.filters.donor = param;
          },
          material:function(param){
            this.filters.material = param;
          },
          warehouse:function(param){
              this.filters.warehouse = param;
          },
          branch:function(param){
              this.filters.branch = param;
            //  / this.$store.dispatch(constants.SubBranchesConstants.FETCH_SUBBRANCHES_DATA,param)  
          },
          subbranch:function(param){
              this.filters.subbranch = param;
          },
          transfer:function(param){
              this.filters.transfer = param;
          },
          export_report:function(type){
            let filters = this.filters;
            this.export_loading = true;
            this.$emit("search",this.filters);
            this.$store
            .dispatch(constants.warehouseConstants.EXPORT_WAREHOUSE_REPORTS,{
                type:type,
                filters:filters
            })
            .then(() => {
                this.export_loading = false;
            }).catch((error) => {
                this.export_loading = false;
            })
        },
      },
    
    mounted(){
        let vm = this;
        
        $('#kt_daterangepicker_2').daterangepicker({
            buttonClasses: 'btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
            }, function(start, end, label) {
                vm.filters.date_range=[start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD')]
            $('#kt_daterangepicker_2 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });

    
      }
   
}

