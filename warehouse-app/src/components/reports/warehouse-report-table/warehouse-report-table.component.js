import { mapGetters } from 'vuex'
import constants from '@/store/const'


export default {
    name:"WarehouseReportTable",
    
    components:{
    },
    props:{
        data: Object,
        loading:Boolean,
        reportData:Array
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            warehouse_report:constants.warehouseConstants.GET_WAREHOUSE_REPORT,
        })
      },

    methods:{
       
    },
    mounted(){
       
    },
}