import { mapGetters } from 'vuex'
import constants from '@/store/const'
import Pagination from '@/components/shared/pagination/Pagination.vue'


export default {
    name:"MinimumQuantityTable",
    
    components:{
       Pagination
    },
    props:{
        data: Object,
        refreshData:Boolean
    },
    data() {
        return {
            loading:false,
        }
    },
    watch:{
        refreshData:function(newVal,oldVal){
            if(newVal==true){
              this.loading = true;
                this.$store.dispatch({
                        type:constants.MinimumQuantityConstants.FETCH_MINIMUM_QUANTITY_DATA,
                    }
                    )
                .then(()=>{
                    this.loading=false;
                    this.$emit('refreshed')
                });
            }
        }
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
        })
      },

    methods:{
        editItem:function(item){
            this.$store.commit(constants.MinimumQuantityConstants.SET_MINIMUM_QUANTITY_ITEM,{value:item});
        },
        deleteItem:function(item){
            let vm =this;
            Swal.fire({
                title: "هل انت متأكد؟",
                text: "لن تستطيع التراجع عن هذه العملية",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "حذف",
                cancelButtonText: "إلفاء"
            }).then(function(result) {
                if(result.value){
                    vm.$store.dispatch(constants.MinimumQuantityConstants.DELETE_MINIMUM_QUANTITY_ITEM,item).then(()=>{
                        toastr.options.progressBar = true;
                        toastr.success('تم حذف الحركة بنجاح');
                        vm.$emit('get-data');
                    }).catch(()=>{
                        toastr.options.progressBar = true;
                        toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
                    });
                }
               
            });
        },
        pagechanged(param){
            if(param.link){
                this.$emit('get-data',param);
            }
        },
        perpagechanged(param){
            if(param.link){
                this.$emit('get-data',param);
            }
        }
    },
    mounted(){
       
    },
}