import {required,numeric} from '@vuelidate/validators'

export default {
    minimum_quantity:{
        minimum_quantity:{
            required,
            numeric,
            $autoDirty:true

        },
        donor_material_id:{
            required,
            $autoDirty:true
        },
       
    }
    
 
};
