import { mapActions, mapGetters } from 'vuex'
import constants from '@/store/const'
import rules from  './minimum_quantity_form.validator';

import DonorsSelect from '@/components/shared/donors-select/DonorsSelect.vue'
import DonorMaterialsSelect from '@/components/shared/donor-materials-select/DonorMaterialsSelect.vue'

export default {
    name:"MinimumQuantityModal",
    components:{
        DonorsSelect,
        DonorMaterialsSelect
    },
    props:['showModal'],
    validations() {
        return rules
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            donor_material: constants.donorMaterialConstants.GET_DONOR_MATERIAL,
            minimum_quantity: constants.MinimumQuantityConstants.GET_MINIMUM_QUANTITY_ITEM,
        })
      },
    methods:{
        donorChagned:function(param){
            console.log('donor change minium aquantity form with param =', param);
            this.$store.dispatch(constants.donorMaterialConstants.FETCH_DONOR_MATERIALS_DATA,param)
        },
        donorMaterialChagned:function(param){
            this.$v.minimum_quantity.donor_material_id.$model = param;
            this.$v.minimum_quantity.donor_material_id.$touch();
            let vm = this;
            this.$store.dispatch(constants.donorMaterialConstants.FETCH_DONOR_MATERIAL,param)
                        .then((response)=>{
                            vm.minimum_quantity.donor_material = response;
                        });
            this.minimum_quantity.donor_material_id = param;
            if(this.minimum_quantity.warehouse_id==null) {
                this.minimum_quantity.warehouse_id = this.user.warehouse_id;
            }

            
        },
        storeItem:function(minimum_quantity){
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
            let vm = this;
            this.$store.dispatch(constants.MinimumQuantityConstants.STORE_MINIMUM_QUANTITY_ITEM,minimum_quantity)
            .then((response)=>{
                
                $("#closeMinimumQuantity").click();
                this.loading_store=false;
                toastr.options.preventDuplicates = true;
                toastr.success('تم الحفظ بنجاح');
                vm.$emit('refresh-data');
            }).catch(()=>{
                this.loading_store=false;
            });
            
        },
    }
}