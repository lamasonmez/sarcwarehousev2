import { mapGetters } from 'vuex'
import constants from '@/store/const'
import rules from  './donor_form.validator';


export default {
    name:"DonorModal",
    data() {
        return {
            loading_store:false
        }
    },
    props:['showModal'],
    validations() {
        return rules
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            donor: constants.donorConstants.GET_DONOR_ITEM,
          
        })
      },
    methods:{
        storeItem:function(donor){
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
            let vm = this;
            this.$store.dispatch(constants.donorConstants.STORE_DONOR_ITEM,donor)
            .then((response)=>{
                $("#closeDonor").click();
                this.loading_store=false;
                toastr.options.preventDuplicates = true;
                toastr.success('تم الحفظ بنجاح');
                vm.$emit('refresh-data');
            }).catch(()=>{
                this.loading_store=false;
            });
            //dispatch store action
          
            
        },
    }
}