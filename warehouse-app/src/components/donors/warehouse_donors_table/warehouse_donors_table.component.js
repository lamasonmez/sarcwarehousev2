import {mapActions, mapGetters} from 'vuex'
import constants from '@/store/const'


export default {
    name: "WarehousDonorsTable",
    data() {
        return {
            loading: false,

        }
    },
    computed: {
        ...mapGetters({
            data: constants.donorConstants.GET_WAREHOUSE_DONORS_DATA
        })

    },
    methods: {
        ...mapActions({
            getdata: constants.donorConstants.FETCH_WAREHOUSE_DONORS_DATA

        }),
        export_werahouse_donors: function (donor_id,name) {
            this.$store.dispatch(constants.donorConstants.EXPORT_WAREHOUSE_DONOR, {
                id: donor_id,name:name
                
            })
                .then(() => {
                    this.loading = false;
                }).catch((error) => {
                this.loading = false;
            })
        },
    },
    mounted() {
        this.loading = true
        this.getdata().then(() => {
            this.loading = false
        })
    }

}