import { mapGetters } from 'vuex'
import constants from '@/store/const'


export default {
    name:"SectorsTable",
    
    components:{
    },
    props:{
        data: Object,
        refreshData:Boolean
    },
    data() {
        return {
            loading:false,
        }
    },
    watch:{
        refreshData:function(newVal,oldVal){
            if(newVal==true){
              this.loading = true;
                //dispatch actions
                this.$store.dispatch({
                    type:constants.sectorConstants.FETCH_SECTORS_DATA,
                }
                )
            .then(()=>{
                this.loading=false;
                this.$emit('refreshed')
            });
            }
        }
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
        })
      },

    methods:{
        editItem:function(item){
            this.$store.commit(constants.sectorConstants.SET_SECTOR_ITEM,{value:item});
        },
        deleteItem:function(item){
            let vm =this;
            Swal.fire({
                title: "هل انت متأكد؟",
                text: "لن تستطيع التراجع عن هذه العملية",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "حذف",
                cancelButtonText: "إلفاء"
            }).then(function(result) {
                if(result.value){
                    vm.$store.dispatch(constants.sectorConstants.DELETE_SECTOR_ITEM,item).then(()=>{
                        toastr.options.progressBar = true;
                        toastr.success('تم حذف القطاع بنجاح');
                        vm.$emit('get-data');
                    }).catch(()=>{
                        toastr.options.progressBar = true;
                        toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
                    });
                }
               
            });
        },
       
    },
    mounted(){
       
    },
}