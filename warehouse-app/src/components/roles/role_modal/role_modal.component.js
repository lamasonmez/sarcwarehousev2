import { mapGetters } from 'vuex'
import constants from '@/store/const'
import rules from  './role_form.validator';


export default {
    name:"RoleModal",
    data() {
        return {
            loading_store:false
        }
    },
    props:['showModal'],
    validations() {
        return rules
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            role: constants.RolesConstants.GET_ROLE_ITEM,
          
        })
      },
    methods:{
        storeItem:function(sector){
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
            let vm = this;
            this.$store.dispatch(constants.RolesConstants.STORE_ROLE_ITEM,sector)
            .then((response)=>{
                $("#closeRole").click();
                this.loading_store=false;
                toastr.options.preventDuplicates = true;
                toastr.success('تم الحفظ بنجاح');
                vm.$emit('refresh-data');
            }).catch(()=>{
                this.loading_store=false;
            });
            //dispatch store action
          
            
        },
    }
}