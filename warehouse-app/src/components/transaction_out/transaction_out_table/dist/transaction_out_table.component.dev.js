"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _const = _interopRequireDefault(require("@/store/const"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  name: 'TransactionOutTable',
  data: function data() {
    return {
      filters: []
    };
  },
  props: {
    loading: Boolean,
    data: Object
  },
  methods: {
    AddNewTransactionOut: function AddNewTransactionOut() {
      var _this = this;

      this.$store.dispatch(_const["default"].transactionsConstants.CLEAR_TRANSACTION_OUT).then(function () {
        _this.$router.push({
          name: "AddEditOutCome"
        });
      });
    },
    deleteTransactionOut: function deleteTransactionOut(item) {
      var vm = this;
      Swal.fire({
        title: "هل انت متأكد؟",
        text: "لن تستطيع التراجع عن هذه العملية",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "حذف",
        cancelButtonText: "إلفاء"
      }).then(function (result) {
        if (result.value) {
          vm.$store.dispatch(_const["default"].transactionsConstants.DELETE_TRANSACTION_OUT, item).then(function () {
            toastr.options.progressBar = true;
            toastr.success('تم حذف الحركة بنجاح');
            vm.$emit('get-transations');
          })["catch"](function () {
            toastr.options.progressBar = true;
            toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
          });
        }
      });
    }
  }
};
exports["default"] = _default;