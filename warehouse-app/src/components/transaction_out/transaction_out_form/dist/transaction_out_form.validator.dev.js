"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _validators = require("@vuelidate/validators");

var _default = {
  transaction: {
    waybill_no: {
      required: _validators.required,
      maxLength: (0, _validators.maxLength)(6),
      minLength: (0, _validators.minLength)(6),
      numeric: _validators.numeric,
      $autoDirty: true
    },
    date: {
      required: _validators.required,
      autoDirty: true
    },
    toable_type: {
      required: _validators.required,
      autoDirty: true
    },
    toable_id: {
      required: _validators.required,
      dirty: true
    }
  }
};
exports["default"] = _default;