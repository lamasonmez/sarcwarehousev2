import 'bootstrap-datepicker';
import 'toastr';
import DonorsSelect from '@/components/shared/donors-select/DonorsSelect.vue'
import AllWarehousesSelect from '@/components/shared/all-warehouses-select/AllWarehousesSelect.vue'
import { mapActions, mapGetters } from 'vuex'
import constants from '@/store/const'

import rules from  './transaction_out_form.validator';
import { getTransitionRawChildren } from 'vue';

export default {
    name:'TransactionOutForm',

    components:{
        DonorsSelect,
        AllWarehousesSelect
    },

    data() {
        return {
            donor:null,
            warehouse:null,
            loading:false,
            loading_store:false,
            check_waybill_loader:false,
            chek_waybill_messgae:'',
            waybill_valid:false,
        }
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            transaction:constants.transactionsConstants.GET_TRANSACTION_OUT
        }),
        
      },
    methods:{
        warehouseChanged:function(param){
            this.transaction.toable_id = param;
            this.transaction.toable_type = 'App\\Models\\Warehouse';
            
        },
        donorChagned:function(param){
            this.transaction.donor_id = param;
    },
        store:function(transaction){
            let vm = this;
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            
            this.loading_store = true;
            this.$store.dispatch(constants.transactionsConstants.STORE_TRANSACTION_OUT,transaction)
            .then(()=>{
                this.loading_store=false;
                this.transaction.waybill_image= this.transaction.src;
                toastr.success('تم الحفظ بنجاح');
                this.$emit('refresh-details');
                this.getTransaction(transaction.id);
            }).catch(()=>{
                console.log('ther is wrong')
                this.loading_store=false;
                toastr.error('لقد حدث خطأ ما يرجى المحاولة لاحقاً');
            });
        },
        onImageChange:function(e) {
            let files = e.target.files || e.dataTransfer.files;
            if (!files.length)
              return;
            this.createImage(files[0]);
          },
          createImage:function(file) {
            let image = new Image();
            let reader = new FileReader();
            let vm = this;
      
            reader.onload = (e) => {
              vm.transaction.waybill_image = e.target.result;
              vm.transaction.image = e.target.result;
              vm.transaction.src = e.target.result;
            };
            reader.readAsDataURL(file);
          },
          removeImage:function(){
              this.transaction.waybill_image = null;
              this.transaction.src = null;
              this.transaction.image = null;
          },
          showDetailsForm:function(){
            this.$emit('show-detail-form');
        },
        resetTransactionOutForm:function(){
            this.loading=true;
            this.$store.dispatch(constants.transactionsConstants.CLEAR_TRANSACTION_OUT,this.transaction).then(()=>{
                this.loading = false;
            });
        },
        checkWaybill:function(){
            if(this.$v.transaction.waybill_no.$invalid==false){
                this.check_waybill_loader = true;
                this.chek_waybill_messgae = '';
                this.$store.dispatch(constants.transactionsConstants.CHECK_WAYBILL,this.$v.transaction.waybill_no.$model).then((response)=>{
                    this.check_waybill_loader = false;
                    this.chek_waybill_messgae = response.message;
                    response.status==500 ? this.waybill_valid=false : this.waybill_valid=true;
                })
                .catch(()=>{
                    this.check_waybill_loader = false;

                });
            }
        },
        getTransaction:function(id){
            this.loading = true;
            this.$store.dispatch(constants.transactionsConstants.FETCH_TRANSACTION_OUT,id)
                       .then(()=>{
                           this.loading=false;
                           this.transaction.waybill_image= this.transaction.src
                       });
        }
    },
    validations() {
        return rules
    },
    mounted(){
        let vm = this;
        var card = new KTCard('kt_card_1');
        var avatar1 = new KTImageInput('kt_image_1');
        if(this.$route.params.id){
            this.getTransaction(this.$route.params.id);
        }
        this.transaction.warehouse_id = this.user.warehouse_id;
        $('#kt_datepicker_1').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            orientation: "bottom left",
           
            
        }).on('changeDate',function(selected) {
            vm.transaction.date=moment(selected.date).format('YYYY-MM-DD');
            $('#date').val(moment(selected.date).format('YYYY-MM-DD'));

        });

    }

}