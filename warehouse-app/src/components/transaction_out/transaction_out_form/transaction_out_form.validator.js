import {required,maxLength,minLength,numeric, requiredIf} from '@vuelidate/validators'


export default {
    transaction:{
        waybill_no:{
            required,
            maxLength:maxLength(6),
            minLength:minLength(6),
            numeric,
            $autoDirty:true

        },
        date:{
            required,
            autoDirty:true

        },
        toable_type:{
            required,
            autoDirty:true
        },
       
        donor_id:{
            required,
            dirty:true,
        },
        
    }
    
 
};
