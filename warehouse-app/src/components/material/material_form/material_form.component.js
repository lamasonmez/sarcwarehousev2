                import SectorSelect from '@/components/shared/sector-select/SectorSelect.vue';
                import DonorsSelect from '@/components/shared/donors-select/DonorsSelect.vue'
                import UnitSelect from '@/components/shared/unit-select/UnitSelect.vue'
                import {mapGetters,mapActions} from 'vuex'
                import constants from '@/store/const'
                import { toDisplayString } from 'vue';
                
                export default {
                    name:'MaterialForm',
                    components:{
                        SectorSelect,
                        DonorsSelect,
                        UnitSelect
                    },
                    props:{
                        donor_disabled:{
                            type:Boolean,
                            required:true
                
                        },
                        donor_id:{
                            type:Number,
                            required:false,
                        },
                        is_material_request:{
                            type:Boolean,
                            required:false,
                            default:false,
                        },
                        material_object:{
                            type:Object,
                            required:false,
                        },
                        donor_material_object:{
                            type:Object,
                            required:false,
                        }
                    },
                    emits:['saved'],
                    watch:{
                        donor_id:function(newVal,oldVal){
                            if(newVal!=null){
                                this.donor_material.donor_id=newVal;
                            }
                        }
                    },
                    data(){
                        return {
                            loading_store:false,
                            donor_material:{
                                code:'0000',
                                id:null,
                                donor_id:null,
                                unit_id:null,
                                size:null,
                                weight:null,
                                content:null,
                                material_id:null,
                            },
                            material:{
                                id:null,
                                sector_id:null,
                                name:{
                                    en:null,
                                    ar:null
                                },
                                code:'0000',
                            },
                        }
                    },
                    computed:{
                        ...mapGetters({
                            user:constants.authConstants.GET_CURRENT_USER,
                            material: constants.materialConstants.GET_MATERIAL_ITEM,
                            donor_material:constants.donorMaterialConstants.GET_DONOR_MATERIAL
                        }),
                    },
                    methods:{
                        ...mapActions({
                            storeMaterial :constants.materialConstants.STORE_MATERIAL_ITEM,
                            storeDonorMaterial :constants.donorMaterialConstants.STORE_DONOR_MATERIAL,
                            storMaterialRequest :constants.materialRequestConstants.STORE_MATERIAL_REQUEST_ITEM,
                            getDonorMaterialData:constants.donorMaterialConstants.FETCH_DONOR_MATERIALS_DATA
                        }),
                        resetDonorMaterial:function(){
                            this.donor_material={
                                code:'0000',
                                id:null,
                                donor_id:null,
                                unit_id:null,
                                size:null,
                                weight:null,
                                content:null,
                                material_id:null,
                            }
                        },
                        store:function(){
                            this.loading_store = true;
                            this.storeMaterial(this.material).
                                then((response)=>{
                                    this.donor_material.material_id = response.id;
                                    if(this.donor_id!=null){
                                        this.donor_material.donor_id =  this.donor_id;
                                    }else{
                                        this.donor_material.donor_id =  this.user.rolable_id;
                                    }
                                    console.log(this.donor_material);
                                    this.storeDonorMaterial(this.donor_material)
                                        .then((resp)=>{
                                            if(this.is_material_request){
                                                this.getDonorMaterialData(this.donor_material.donor_id).then(()=>{
                                                    this.loading_store=false;
                                                    this.$emit('saved');
                                                }).catch(()=>this.loading_store=false);
                
                                                
                                                // this.storMaterialRequest({"user_id":this.user.id,"donor_material_id":resp.id,"status":"approved"}).then(()=>{
                                                //     this.loading_store=false;
                                                //     this.$emit('saved');
                                                // }).catch(()=> this.loading_store=false);
                                            }
                                            else{
                                                this.loading_store=false;
                                                toastr.success('تمت عملية الحفظ بنجاح')
                                                this.$emit('saved')
                
                                            }
                                        }).catch(()=>{
                                            this.loading_store=false;
                                             //this.resetDonorMaterial();
                
                                        });
                                }).catch(()=>{
                                    this.loading_store=false;
                                    toastr.error('حدث خطأ ما الرجاء المحاولة لاحقاً');
                                    
                                });
                        }
                    },
                    created(){
                        if(this.material_object)this.material = this.material_object;
                        if(this.donor_material_object)this.donor_material = this.donor_material_object;
                        this.donor_material.donor_id=this.donor_id;
                    }
                }