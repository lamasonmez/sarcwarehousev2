import { mapActions, mapGetters } from 'vuex'
import constants from '@/store/const'
import 'bootstrap-daterangepicker';

export default {
    name:"FMaterialTransactioNTable",
    
    components:{
    },
    props:['data'],
    data() {
        return {
            loading:false,
            date_range:''
        }
    },
   
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            transaction_detail:constants.transactionsConstants.GET_TRANSACTION_DETAIL_ITEM,

        })
      },

    methods:{
        ...mapActions({
            export_material:constants.inventorysConstants.EXPRORT_MATERIAL_TRANSACTIONS,
            filter_material:constants.inventorysConstants.FILTER_MATERIAL_TRANSACTIONS
        }),
        search(){
            this.loading  = true;
            this.filter_material({donor_material_id:this.transaction_detail.donor_material_id,filters:{
                dateRange:this.date_range
            }}).then(()=>{
                this.loading=false;
            })
            .catch(()=>this.loading=false)
            ;
        },
        initailizeDateRangePicker(){
             let vm = this;
        $('#kt_daterangepicker_2').daterangepicker({
            buttonClasses: 'btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
            }, function(start, end, label) {
                vm.date_range=[start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD')]
            $('#kt_daterangepicker_2 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });
        }

       
       
    },
    mounted(){
       this.initailizeDateRangePicker();
    },
}