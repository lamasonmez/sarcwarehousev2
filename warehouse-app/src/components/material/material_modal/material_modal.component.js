import MaterialForm from '@/components/material/material_form/MaterialForm.vue'

export default {
    name:'MaterialModal',
    components:{
        MaterialForm
    },
    props:{
        donor_id:{
            type:Number,
            required:false,
        },
        donor_diabled:{
            type:Number,
            required:false
        },
        is_material_request:{
            type:Boolean,
            required:false,
            default:false,
        }
    },
    emits:['saved'],
    methods:{
        closeModal:function(){
            $(this.$refs.materialModal).hide();
            $('.modal-backdrop').remove();
        }
    }
}