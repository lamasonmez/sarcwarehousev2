import {mapGetters,mapActions} from 'vuex'
import constants from '@/store/const'
import {NOTIFICATIONS_PERIOD} from '@/config'
 
export default {
    name:"PendingTransactionNoficiation",
    data() {
        return {
         //   pending_transactions:[],
        }
    },
    computed:{
        ...mapGetters({
            pending_transactions: constants.notificationsConstants.GET_PENDING_TRANSACTIONS,
            user:constants.authConstants.GET_CURRENT_USER
        })
    },
    methods:{
        ...mapActions({
            fetchPendingTransactions:constants.notificationsConstants.FETCH_PENDING_TRANSACTIONS
        }),
    },
    created() {
       this.fetchPendingTransactions();
       //check if authenticated first
       if(this.user && this.user.role.length>0 && this.user.role[0]!='donor' && this.user.role[0]!='admin'){
        this.interval = setInterval(() => this.fetchPendingTransactions(), NOTIFICATIONS_PERIOD);
       }
    },
}