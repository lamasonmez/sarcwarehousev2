import {mapGetters,mapActions} from 'vuex'
import constants from '@/store/const'
import {NOTIFICATIONS_PERIOD} from '@/config'
export default {
    name:"MinimumQuantityNoficiation",
    data() {
        return {
            minimum_quantity_notifications:[],
        }
    },
    computed:{
        ...mapGetters({
            notificatoins: constants.notificationsConstants.GET_MINIMUM_QUANTITY_NOTIFICATIONS,
            user:constants.authConstants.GET_CURRENT_USER

        })
    },
    methods:{
        ...mapActions({
            fetchMinimumQauntityNotifications:constants.notificationsConstants.FETCH_MINIMUM_QUANTITY_NOTIFICATIONS
        }),
    },
    created() {
       this.fetchMinimumQauntityNotifications();
        if(this.user && this.user.role.length>0 && this.user.role[0]!='donor' && this.user.role[0]!='admin'){
            this.interval = setInterval(() => this.fetchMinimumQauntityNotifications()
            .then((response)=>{this.minimum_quantity_notifications=response})
, NOTIFICATIONS_PERIOD);
        }
       
    },
}