import {mapGetters,mapActions} from 'vuex'
import constants from '@/store/const'
import {NOTIFICATIONS_PERIOD} from '@/config'
export default {
    name:"MaterialRequestNotification",
    data() {
        return {
            pending_material_requests:[],
           
        }
    },
    computed:{
        ...mapGetters({
            //pending_material_requests: constants.materialRequestConstants.GET_MATERIAL_PENDING_REQUESTS,
        })
    },
    methods:{
        ...mapActions({
            fetchPendingMaterialRequests:constants.materialRequestConstants.FETCH_MATERIAL_PENDING_REQUESTS
        }),
    },
    created() {
        
        this.fetchPendingMaterialRequests();
        this.interval = setInterval(() => this.fetchPendingMaterialRequests()
                                                .then((response)=>{this.pending_material_requests=response})
                        , NOTIFICATIONS_PERIOD);
    },
}