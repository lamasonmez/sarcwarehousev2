import {mapGetters,mapActions} from 'vuex'
import constants from '@/store/const'
import MaterialRequestNotification from '@/components/layout/notifications/material_request_notification/MaterialRequestNotification';
import MinimumQuantityNotificiation from '@/components/layout/notifications/minimum_quantity_notification/MinimumQuantityNotification';
import PendingTransactionNotifications from '@/components/layout/notifications/pending_transaction_notifications/PendingTransactionNotifications';

export default {
    name:"HeaderNotification",
    components:{
        MaterialRequestNotification,
        MinimumQuantityNotificiation,
        PendingTransactionNotifications
    },
    computed:{
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            pending_material_requests: constants.materialRequestConstants.GET_MATERIAL_PENDING_REQUESTS,
            mimimum_quantity_notificactions: constants.notificationsConstants.GET_MINIMUM_QUANTITY_NOTIFICATIONS,
            pending_transactions: constants.notificationsConstants.GET_PENDING_TRANSACTIONS

        }),
        notifications_count:function(){
            return this.pending_material_requests.length + this.mimimum_quantity_notificactions.length+this.pending_transactions.length;
        }
    },
}