import HeaderNotifications from '@/components/layout/header/header-notification/header-notification'
import HeaderUserBar from '@/components/layout/header/header-user/header-user'
export default {
    name:"HeaderTop",
    components:{
        HeaderNotifications,
        HeaderUserBar
    }
}