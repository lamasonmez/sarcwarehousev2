import { mapActions, mapGetters } from 'vuex'
import constants from '@/store/const'
export default {
    name:"HeaderUser",
    methods:{
        logout(){

            let vm = this;

            this.$store
                .dispatch("logout")
                .then((response) => {

                    if(vm.$route.name !== 'login')
                        vm.$router.push({name: "login"})
                });
        },
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
        }),
        
      },
      watch:{
        user: function(newVal, oldVal) {
            if(newVal!=null){
                this.user = newVal;
            }
        },
    },  
      mounted(){
      },
      

}