"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _const = _interopRequireDefault(require("@/store/const"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  name: 'HeaderMenu',
  methods: {
    AddNewTransactionIn: function AddNewTransactionIn() {
      var _this = this;

      this.$store.dispatch(_const["default"].transactionsConstants.CLEAR_TRANSACTION_IN).then(function () {
        _this.$router.push({
          name: "AddEditIncome"
        });
      });
    },
    AddNewTransactionOut: function AddNewTransactionOut() {
      var _this2 = this;

      this.$store.dispatch(_const["default"].transactionsConstants.CLEAR_TRANSACTION_OUT).then(function () {
        _this2.$router.push({
          name: "AddEditOutCome"
        });
      });
    }
  }
};
exports["default"] = _default;