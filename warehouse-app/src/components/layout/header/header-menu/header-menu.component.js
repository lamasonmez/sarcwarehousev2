import constants from '@/store/const'
import { mapActions, mapGetters } from 'vuex'
export default {
    name:'HeaderMenu',
    computed: {
        ...mapGetters({
            transaction_in:constants.transactionsConstants.GET_TRANSACTION_IN,
            transaction_out:constants.transactionsConstants.GET_TRANSACTION_OUT,
            user:constants.authConstants.GET_CURRENT_USER,

        }),
        
      },
      
    methods:{
        AddNewTransactionIn:function(){
            this.transaction_in.warehouse_id = this.user.id;
            this.$store.dispatch(constants.transactionsConstants.CLEAR_TRANSACTION_IN,this.transaction_in).then(()=>{
                this.$router.push({name: "AddEditIncome"});
            });
        },
        AddNewTransactionOut:function(){
            this.$store.dispatch(constants.transactionsConstants.CLEAR_TRANSACTION_OUT,this.transaction_out).then(()=>{
                this.$router.push({name: "AddEditOutCome"});
            });
        },
      
    }
}

