import { mapGetters } from 'vuex'
import constants from '@/store/const'
export default {
    name: 'TotalMaterialsCount',
    
    data() {
        return {
            loading:true,
        }
    },
    computed: {
        ...mapGetters({
            materials_count: constants.statisticsConstants.GET_DONOR_MATERIAL_COUNT,
            user:constants.authConstants.GET_CURRENT_USER
        })
      },
      mounted(){
             this.$store
            .dispatch(constants.statisticsConstants.FETCH_DONOR_MATERIAL_COUNT)
            .then(() => this.loading = false);
      }
   
}