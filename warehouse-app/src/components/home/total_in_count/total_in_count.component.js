import { mapGetters } from 'vuex'
import constants from '@/store/const'

export default {
    name: 'TotalInCount',
    
    data() {
        return {
            loading:true,
        }
    },
    computed: {
        ...mapGetters({
            in_count: constants.statisticsConstants.GET_TOTAL_IN_COUNT,
            user:constants.authConstants.GET_CURRENT_USER
        })
      },
      mounted(){
             this.$store
            .dispatch(constants.statisticsConstants.FETCH_TOTAL_IN_COUNT)
            .then(() => this.loading = false);
      }
   
}