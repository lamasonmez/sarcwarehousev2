import { mapGetters } from 'vuex'
import constants from '@/store/const'

export default {
    name: 'TotalOutCount',
    
    data() {
        return {
            loading:true,
        }
    },
    computed: {
        ...mapGetters({
            out_count: constants.statisticsConstants.GET_TOTAL_OUT_COUNT,
            user:constants.authConstants.GET_CURRENT_USER

        })
      },
      mounted(){
             this.$store
            .dispatch(constants.statisticsConstants.FETCH_TOTAL_OUT_COUNT)
            .then(() => this.loading = false);
      }
   
}