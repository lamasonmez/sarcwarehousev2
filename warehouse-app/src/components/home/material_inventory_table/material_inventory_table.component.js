import {mapActions, mapGetters} from 'vuex'
import constants from '@/store/const'
import 'bootstrap-daterangepicker';

import Pagination from '@/components/shared/pagination/Pagination.vue'

export default {
    name: 'MaterialInventoryTable',
    data() {
        return {
            loading: true,
            date_range: '',
            keyword: '',
         filters:null,


        }
    },
    components: {
        Pagination
    },
    computed: {
        ...mapGetters({
            inventory_data: constants.inventorysConstants.GET_INVENTORY_DATA,
            user: constants.authConstants.GET_CURRENT_USER

        })
    },
    methods: {
        ...mapActions({
            //export_inventory: constants.inventorysConstants.EXPRORT_INVENTORY,
            filter_inventory: constants.inventorysConstants.FILTER_INVENTORY
        }),
        search(param = null) {
            this.loading = true;
            this.filters={
                dateRange: this.date_range,
                keyword: this.keyword,
                pagination: param
            };
            this.filter_inventory({
                filters:this.filters,
                pagination:param
            }).then(() => {
                this.loading = false;
            })
                .catch(() => this.loading = false)
            ;
        },
        export_inventory(type){
            if(this.filters){
                this.$store.dispatch(constants.inventorysConstants.EXPRORT_INVENTORY,{type:type,filters:this.filters})
            }
            else{
                this.$store.dispatch(constants.inventorysConstants.EXPRORT_INVENTORY,{type:type,filters:null})

            }
        },
        pagechanged(param) {
            if (param.link) {
                console.log(param.link)
                if (param.link.includes('filter'))
                    this.search(param)
                else
                    this.getData(param);
            }
        },
        perpagechanged(param) {
            if (param.link) {
                if (param.link.contain('filter'))
                    this.search(param)
                else
                    this.getData(param);
            }
        },
        initailizeDateRangePicker() {

            let vm = this;
            $('#kt_daterangepicker_2').daterangepicker({
                buttonClasses: 'btn',
                applyClass: 'btn-primary',
                cancelClass: 'btn-secondary'
            }, function (start, end, label) {

                vm.date_range = [start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD')]
                $('#kt_daterangepicker_2 .form-control').val(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
            });
        },
        getData: function (param) {
            this.loading = true;
            this.$store
                .dispatch(
                    {
                        type: constants.inventorysConstants.FETCH_INVENTORY_DATA,
                        pagination: param

                    })
                .then((result) => {
                    this.loading = false
                })
                .catch(() => {
                    this.loading = false;
                    console.log('error in fetching inventory data')
                });
        }
    },
    mounted() {
        this.initailizeDateRangePicker();
        this.getData();
    }

}

