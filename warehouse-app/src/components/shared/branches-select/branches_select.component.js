import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:'BranchesSelect',
    
    data() {
        return {
            loading:true,
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        },
        dataRef:{
            type:String,
            required:false
        },
        validator:{
            type:Object,
            required:false,
        },
        label:{
            type:String,
            required:false
        }
    },
   
    computed: {
        ...mapGetters({
            branches: constants.BranchesConstants.GET_BRANCHES_DATA,
        })
      },

      watch:{
        selected: function(newVal, oldVal) {
            if(newVal!=0){
               $('#'+this.dataRef).val(newVal).trigger('change');
            }
        },
       
      },
    methods:{
        getBranches: function(){
            this.loading = true;
            let vm = this;
            this.$store
                .dispatch(constants.BranchesConstants.FETCH_BRANCHES_DATA)
                .then(() => {
                    this.loading = false;
                })
                .catch((response)=>console.log('error',response)); 
        },
      },
    
    mounted(){
        let vm = this;
        $('#'+this.dataRef).select2({
            dir:"rtl",
            placeholder: "اختر فرع",
            allowClear: false
        }).on('change',function(){
            vm.$emit("branch",this.value);
        });
        this.getBranches();       
      }
   
}

