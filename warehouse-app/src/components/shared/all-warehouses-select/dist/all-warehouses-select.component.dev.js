"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

require("select2");

var _const = _interopRequireDefault(require("@/store/const"));

var _vuex = require("vuex");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = {
  name: 'AllWarehousesSelect',
  data: function data() {
    return {
      loading: false,
      warehouses_branches: []
    };
  },
  props: {
    selected: {
      type: Number,
      required: false
    }
  },
  computed: _objectSpread({}, (0, _vuex.mapGetters)({
    warehouses: _const["default"].warehouseConstants.GET_WAREHOUSES_DATA
  })),
  methods: {
    getWarehouses: function getWarehouses() {
      var _this = this;

      this.loading = true;
      this.$store.dispatch(_const["default"].warehouseConstants.FETCH_WAREHOUSES_DATA).then(function (data) {
        data.forEach(function (item) {
          if (item.branchable_type == 'App\\Models\\Branch') {
            if (!_this.warehouses_branches.includes(item.branchable.name.ar)) {
              _this.warehouses_branches.push(item.branchable.name.ar);
            }
          }
        });
        _this.loading = false;
      })["catch"](function (response) {
        return console.log('error', response);
      });
    }
  },
  mounted: function mounted() {
    var vm = this;
    $('#warehouses_selector').select2({
      dir: "rtl",
      placeholder: "اختر مستودع",
      allowClear: false
    }).on('change', function () {
      vm.$emit('warehouse', this.value);
    });
    this.getWarehouses();
  }
};
exports["default"] = _default;