import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'


export default {
    name:'AllWarehousesSelect',
    
    data() {
        return {
            loading:false,
            warehouses_branches:[],
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        branchID:{
            type:Number,
            required:false,
        },
        SubBranchID:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false,
            default:false
        }
    },
    watch:{
        branchID:function(newVal,oldVal){
            if(newVal!=null){
                this.warehouses_branches = [];
                this.getWarehouses({'id':newVal,'type':'branch'});
            }
        },
        SubBranchID:function(newVal,oldVal){
            if(newVal){
                this.warehouses_branches = [];
                this.getWarehouses({'id':newVal,'type':'subBranch'});
            }
            else{
                this.warehouses_branches = [];
                if(this.branchID){
                    this.getWarehouses({'id':this.branchID,'type':'branch'});
                }
                else{
                    this.getWarehouses();
                }
            }
        },
      
        selected: function(newVal, oldVal) {
            if(newVal!=0){
               $('#warehouses_selector').val(newVal).trigger('change');
            }
        },

       
      },
    computed: {
        ...mapGetters({
            warehouses: constants.warehouseConstants.GET_WAREHOUSES_DATA,
        })
      },

    methods:{
          getWarehouses: function(data = null){
            this.loading = true;
            this.$store
                .dispatch(constants.warehouseConstants.FETCH_WAREHOUSES_DATA,data)
                .then((data) => {
                    data.forEach((item)=>{
                        if(item.branchable_type=='App\\Models\\Branch'){
                            if(!this.warehouses_branches.includes(item.branchable.name.ar)){
                                this.warehouses_branches.push(item.branchable.name.ar)
                            }

                        }
                        else if(item.branchable!=null && item.branchable.branch !=null){
                            if(!this.warehouses_branches.includes(item.branchable.branch.name.ar)){
                                this.warehouses_branches.push(item.branchable.branch.name.ar)
                            }
                        }
                    })
                    this.loading = false;
                 
                    
        })
        .catch((response)=>{
            this.loading=false;
            console.log('error',response);
        }); 
             
        },
       
      },
    
    mounted(){
        let vm = this;
        $('#warehouses_selector').select2({
            dir:"rtl",
            placeholder: "اختر مستودع",
            allowClear: false,
        }).on('change',function(){
            vm.$emit('warehouse',this.value);
        });
        
      },
      created(){
        this.getWarehouses() ;
      }
   
}

