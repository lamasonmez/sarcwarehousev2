import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:'DonorsSelect',
    
    data() {
        return {
            loading:true,
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        },
        dataRef:{
            type:String,
            required:false
        },
        validator:{
            type:Object,
            required:false,
        },
        label:{
            type:String,
            required:false,
            default:"الشركاء"
        }
    },
    emits: ['donor'],
    computed: {
        ...mapGetters({
            donors: constants.donorConstants.GET_DONORS_DATA,
        })
      },

      watch:{
        selected: function(newVal, oldVal) {
            if(newVal!=0){
               $('#'+this.dataRef).val(newVal).trigger('change');
            }
        },
       
      },
    methods:{
        getDonors: function(){
            this.loading = true;
            let vm = this;
            this.$store
                .dispatch(constants.donorConstants.FETCH_DONORS_DATA)
                .then(() => {
                    this.loading = false;
                })
                .catch((response)=>console.log('error',response)); 
        },
      },
    
    mounted(){
        let vm = this;
        $('#'+this.dataRef).select2({
            dir:"rtl",
            placeholder: "اختر شريك",
            allowClear: false
        }).on('change',function(){
            console.log('donor id change = ',this.value);
            vm.$emit("donor",this.value);
        });
            this.getDonors();       
    
      }
   
}

