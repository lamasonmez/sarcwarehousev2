import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'
import { stringifyQuery } from 'vue-router'



export default {
    name:'DonorMaterialsSelect',
    
    data() {
        return {
            loading:false,
            
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        donor_id:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        },
        invalideClass:{
            type:String,
            required:false
        },
        invalid:{
            type:Boolean,
            required:false,
        },
        validationErrors:{
            type:Array,
            required:false
        }
    },

    watch:{
        donor_id: function(newVal, oldVal) {
            if(newVal!=0 || newVal!=null){
               this.getDonorMaterials(newVal);
            }
        },
        selected: function(newVal, oldVal) {
            if(newVal!=0 || newVal!==""){
                $('#donor_materials_selector').val(newVal).trigger('change');
            }
        },
      },
    computed: {
        ...mapGetters({
            donor_materials: constants.donorMaterialConstants.GET_DONOR_MATERIALS_DATA,
        })
      },

    methods:{
        getDonorMaterials: function(donor_id){
            this.loading = true;
            this.$store
                .dispatch(constants.donorMaterialConstants.FETCH_DONOR_MATERIALS_DATA,donor_id)
                .then(() => {this.loading = false;})
                .catch((response)=>{
                    console.log('error',response);
                    this.loading=false;
                }); 
        },
      },
    
    mounted(){
        let vm = this;
        $('#donor_materials_selector').select2({
            dir:"rtl",
            placeholder: "اختر مادة",
            allowClear: false
        }).on('change',function(){
            vm.$emit("material",this.value);

        });
        if(this.donor_id!=null){
            this.getDonorMaterials(this.donor_id);       
        }
    
      }
   
}

