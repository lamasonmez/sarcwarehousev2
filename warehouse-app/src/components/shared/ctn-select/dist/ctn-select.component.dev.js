"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

require("select2");

var _const = _interopRequireDefault(require("@/store/const"));

var _vuex = require("vuex");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = {
  name: 'CtnSelected',
  data: function data() {
    return {
      loading: false
    };
  },
  props: {
    selected: {
      type: Number,
      required: false
    },
    donor_material: {
      type: Object,
      required: false
    },
    disabled: {
      type: Boolean,
      required: false
    }
  },
  watch: {
    donor_material: function donor_material(newVal, oldVal) {
      if (newVal != 0 || newVal != null) {
        console.log('donorMaterial changes', newVal);
        this.$store.commit(_const["default"].ctnConstants.SET_CTN_DATA, newVal.ctns);
      }
    }
  },
  computed: _objectSpread({}, (0, _vuex.mapGetters)({
    ctns: _const["default"].ctnConstants.GET_CTN_DATA
  })),
  methods: {
    getCTNS: function getCTNS() {
      var _this = this;

      this.loading = true;
      this.$store.dispatch(_const["default"].ctnConstants.FETCH_CTN_DATA).then(function () {
        _this.loading = false;
      })["catch"](function (response) {
        console.log('error', response);
        _this.loading = false;
      });
    }
  },
  mounted: function mounted() {
    var vm = this;
    $('#ctn-select').select2({
      dir: "rtl",
      placeholder: "اختر رقم المتابعة",
      allowClear: false
    }).on('change', function () {
      vm.$emit("ctn", this.value);
    });
  }
};
exports["default"] = _default;