import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:'CtnSelected',
    
    data() {
        return {
            loading:false,
            
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        donor_material:{
            type:Object,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        }
    },

    watch:{
        // donor_material: function(newVal, oldVal) {
        //     if(newVal!=0 || newVal!=null){
        //         console.log('donorMaterial changes',newVal);
        //        this.$store.commit(constants.ctnConstants.SET_CTN_DATA,newVal.ctns);
        //     }
        // }
      },
    computed: {
        ...mapGetters({
            ctns: constants.ctnConstants.GET_CTN_DATA,
        })
      },

    methods:{
        
        getCTNS: function(){
            this.loading = true;
            this.$store
                .dispatch(constants.ctnConstants.FETCH_CTN_DATA)
                .then(() => {this.loading = false;})
                .catch((response)=>{
                    console.log('error',response);
                    this.loading=false;
                }); 
        },
      },
    
    mounted(){
        let vm = this;
        $('#ctn-select').select2({
            dir:"rtl",
            placeholder: "اختر رقم المتابعة",
            allowClear: false
        }).on('change',function(){
            vm.$emit("ctn",this.value);
        });
      
      }
   
}

