import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:'SubBranchesSelect',
    
    data() {
        return {
            loading:true,
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        },
        dataRef:{
            type:String,
            required:false
        },
        validator:{
            type:Object,
            required:false,
        },
        branchID:{
            type:Number,
            required:false
        }
    },
   
    computed: {
        ...mapGetters({
            sub_branches: constants.SubBranchesConstants.GET_SUBBRANCHES_DATA,
            user:constants.authConstants.GET_CURRENT_USER,

        })
      },

      watch:{
        selected: function(newVal, oldVal) {
            if(newVal!=0){
               $('#'+this.dataRef).val(newVal).trigger('change');
            }
        },
        branchID:function(newVal,oldVal){
            if(newVal!=null){
                this.getSubBranches(newVal);
            }
        }
       
      },
    methods:{
        getSubBranches: function(id=null){
            this.loading = true;
            let vm = this;
            this.$store
                .dispatch(constants.SubBranchesConstants.FETCH_SUBBRANCHES_DATA,id)
                .then(() => {
                    this.loading = false;
                })
                .catch((response)=>console.log('error',response)); 
        },
      },
    
    mounted(){
        let vm = this;
        $('#'+this.dataRef).select2({
            dir:"rtl",
            placeholder: "اختر شعبة/ نقطة",
            allowClear: true
        }).on('change',function(){
            vm.$emit("subbranch",this.value);
        });
        this.getSubBranches();       
      }
   
}

