import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:'TransferSelect',
    data() {
        return {
            loading:true,
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        },
        dataRef:{
            type:String,
            required:false
        },
        validator:{
            type:Object,
            required:false,
        }
    },
   
    computed: {
        ...mapGetters({
            transfers : constants.TransfersConstants.GET_TRANSFERS_DATA
        })
      },

      watch:{
        selected: function(newVal, oldVal) {
            if(newVal!=0){
               $('#'+this.dataRef).val(newVal).trigger('change');
            }
        },
       
      },
    methods:{
        getTransfers: function(){
            this.loading = true;
            let vm = this;
            this.$store
                .dispatch(constants.TransfersConstants.FETCH_TRANSFERS_DATA)
                .then(() => {
                    this.loading = false;
                })
                .catch((response)=>console.log('error',response)); 
        },
      },
    
    mounted(){
        let vm = this;
        $('#'+this.dataRef).select2({
            dir:"rtl",
            placeholder: "اختر نوع المناقلة",
            allowClear: false
        }).on('change',function(){
            vm.$emit("transfer",this.value);
        });
        this.getTransfers();       
      }
   
}

