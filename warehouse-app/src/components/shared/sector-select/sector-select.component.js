import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:'SectorSelect',
    
    data() {
        return {
            loading:false,
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        },
        required:{
            type:Boolean,
            required:false
        },
        dataRef:{
            type:String,
            required:false
        },
    },
    emits: ['sector'],
    watch:{
        selected: function(newVal, oldVal) {
            if(newVal!=0 || newVal!=null){
                $('#'+this.dataRef).val(newVal).trigger('change');
            }
        },
      },
    computed: {
        ...mapGetters({
            sectors: constants.sectorConstants.GET_SECTORS_DATA,
        })
      },

    methods:{
        getSectors: function(){
            this.loading = true;
            this.$store
                .dispatch(constants.sectorConstants.FETCH_SECTORS_DATA)
                .then(() => {this.loading = false;})
                .catch((response)=>{
                    console.log('error',response);
                    this.loading=false;
                }); 
        },
      },
    mounted(){
        let vm = this;
        $('#'+this.dataRef).select2({
            dir:"rtl",
            placeholder: "اختر القطاع",
            allowClear: false
        }).on('change',function(){
            vm.$emit("sector",this.value);
        });
            this.getSectors();       
    
      }
   
}

