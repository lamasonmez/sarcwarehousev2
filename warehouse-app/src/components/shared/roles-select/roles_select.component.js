import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:'RolesSelect',
    
    data() {
        return {
            loading:true,
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        },
        dataRef:{
            type:String,
            required:false
        },
        validator:{
            type:Object,
            required:false,
        },
        label:{
            type:String,
            required:false
        }
    },
   
    computed: {
        ...mapGetters({
            roles: constants.RolesConstants.GET_ROLES_DATA,
        })
      },

      watch:{
        selected: function(newVal, oldVal) {
            if(newVal!=0){
               $('#'+this.dataRef).val(newVal).trigger('change');
            }
        },
       
      },
    methods:{
        getRoles: function(){
            this.loading = true;
            let vm = this;
            this.$store
                .dispatch(constants.RolesConstants.FETCH_ROLES_DATA)
                .then(() => {
                    this.loading = false;
                })
                .catch((response)=>console.log('error',response)); 
        },
      },
    
      mounted(){
        let vm = this;
        $('#'+this.dataRef).select2({
            dir:"rtl",
            placeholder: "اختر دور",
            allowClear: false
        }).on('change',function(){
            vm.$emit("role",this.value);
        });
            this.getRoles();       
      }
   
}

