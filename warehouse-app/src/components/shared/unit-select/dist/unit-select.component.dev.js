"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

require("select2");

var _const = _interopRequireDefault(require("@/store/const"));

var _vuex = require("vuex");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = {
  name: 'UnitSelect',
  data: function data() {
    return {
      loading: false
    };
  },
  props: {
    selected: {
      type: Number,
      required: false
    },
    donor_material_id: {
      type: Number,
      required: false
    },
    disabled: {
      type: Boolean,
      required: false
    }
  },
  watch: {
    selected: function selected(newVal, oldVal) {
      if (newVal != 0 || newVal != null) {
        $('#unit-select').val(newVal).trigger('change');
      }
    }
  },
  computed: _objectSpread({}, (0, _vuex.mapGetters)({
    units: _const["default"].unitConstants.GET_UNITS_DATA
  })),
  methods: {
    getUnits: function getUnits() {
      var _this = this;

      this.loading = true;
      this.$store.dispatch(_const["default"].unitConstants.FETCH_UNITS_DATA).then(function () {
        _this.loading = false;
      })["catch"](function (response) {
        console.log('error', response);
        _this.loading = false;
      });
    }
  },
  mounted: function mounted() {
    var vm = this;
    $('#unit-select').select2({
      dir: "rtl",
      placeholder: "اختر الوحدة",
      allowClear: false
    }).on('change', function () {
      vm.$emit("unit", this.value);
    });
    this.getUnits();
  }
};
exports["default"] = _default;