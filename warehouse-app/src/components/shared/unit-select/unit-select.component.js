import 'select2'
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:'UnitSelect',
    
    data() {
        return {
            loading:false,
            
        }
    },
    props:{
        selected:{
            type:Number,
            required:false,
        },
        donor_material_id:{
            type:Number,
            required:false,
        },
        disabled:{
            type:Boolean,
            required:false
        },
        label:{
            type:String,
            required:false,
            default:"الوحدات"
        },
        dataRef:{
            type:String,
            required:false
        },
    },
    emits: ['unit'],
    watch:{
        selected: function(newVal, oldVal) {
            if(newVal!=0 || newVal!=null){
                $('#'+this.dataRef).val(newVal).trigger('change');
            }
        },
      },
    computed: {
        ...mapGetters({
            units: constants.unitConstants.GET_UNITS_DATA,
        })
      },

    methods:{
        getUnits: function(){
            this.loading = true;
            this.$store
                .dispatch(constants.unitConstants.FETCH_UNITS_DATA)
                .then(() => {this.loading = false;})
                .catch((response)=>{
                    console.log('error',response);
                    this.loading=false;
                }); 
        },
      },
    
    mounted(){
        let vm = this;
        $('#'+this.dataRef).select2({
            dir:"rtl",
            placeholder: "اختر الوحدة",
            allowClear: false
        }).on('change',function(){
            vm.$emit("unit",this.value);
        });
            this.getUnits();       
    
      }
   
}

