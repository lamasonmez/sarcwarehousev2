export default {
    name:'Pagination',
    data() {
        return {
            perPage:10,
        }
    },
    props:{
        maxVisibleButtons: {
            type: Number,
            required: false,
            default: 5
        },
        totalPages: {
            type: Number,
            required: true
        },
        total: {
            type: Number,
            required: true
        },
        from: {
            type: Number,
            required: true
        },
        to: {
            type: Number,
            required: true
        },
        currentPage: {
            type: Number,
            required: true
        },
        firstPageUrl:{
            type:String,
            required:true
        },
        lastPageUrl:{
            type:String,
            required:true,
        },
        currentLink:{
            type:String,
            required:true,
        },
        links:{
            type:Array,
            required:true,
        }
    },
    watch:{
        currentPage:function(newVal,oldVal){
            console.log('current page now is' ,newVal);
        }  
    },
    computed:{
        startPage() {
            // When on the first page
            if (this.currentPage === 1) {
              return 1;
            }
            // When on the last page
            if (this.currentPage === this.totalPages) {
              return this.totalPages - this.maxVisibleButtons;
            }
            // When in between
            return this.currentPage - 1;
        },
        isInFirstPage() {
            return this.currentPage === 1;
        },
        isInLastPage() {
            return this.currentPage === this.totalPages;
        },
        activeLink(){
            return this.currentLink;
        }
       
    },
    methods: {
        onClickPage(link) {
          this.activeLink  = link;
          this.$emit('pagechanged', {'link':link,'perPage':this.perPage});
        },
        isPageActive(page) {
            return this.currentPage === page;
        },
        perPageChanges(perpage){
            this.perPage = perpage;
            this.$emit('perpagechanged',{'link':this.activeLink,'perPage':this.perPage});
        }
      }
}