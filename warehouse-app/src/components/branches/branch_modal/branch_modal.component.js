import { mapGetters } from 'vuex'
import constants from '@/store/const'
import rules from  './branch_form.validator';


export default {
    name:"BranchModal",
    data() {
        return {
            loading_store:false
        }
    },
    props:['showModal'],
    validations() {
        return rules
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            branch: constants.BranchesConstants.GET_BRANCH_ITEM,
          
        })
      },
    methods:{
        storeItem:function(branch){
            this.$v.$touch();
            if(this.$v.$error){
                toastr.options.preventDuplicates = true;
                toastr.error('Please Fill in the required fileds');
                return;
            }
            this.loading_store = true;
            let vm = this;
            this.$store.dispatch(constants.BranchesConstants.STORE_BRANCH_ITEM,branch)
            .then((response)=>{
                $("#closeBranch").click();
                this.loading_store=false;
                toastr.options.preventDuplicates = true;
                toastr.success('تم الحفظ بنجاح');
                vm.$emit('refresh-data');
            }).catch(()=>{
                this.loading_store=false;
            });
            //dispatch store action
          
            
        },
    }
}