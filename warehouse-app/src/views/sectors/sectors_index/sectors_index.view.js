import { mapGetters } from 'vuex'
import constants from '@/store/const'

import SectorsTable from '@/components/sectors/sectors_table/SectorsTable.vue'
import SectorModal from '@/components/sectors/sector_modal/SectorModal.vue'



export default {
    name:'SectorsIndex',
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        SectorsTable,
        SectorModal
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            sectors_data: constants.sectorConstants.GET_SECTORS_DATA,
        })
      },
      methods:{
        getData: function(){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.sectorConstants.FETCH_SECTORS_DATA 
                        })
                    .then(() => this.loading = false); 
        },
    },
    mounted(){
        this.getData();
    },

 
}