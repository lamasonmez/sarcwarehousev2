import { mapGetters } from 'vuex'
import constants from '@/store/const'

import SubBranchesTable from '@/components/sub_branches/subbranches_table/SubBranchesTable.vue'
import SubBranchModal from '@/components/sub_branches/subbranch_modal/SubBranchModal.vue'



export default {
    name:'SubBranchesIndex',
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        SubBranchesTable,
        SubBranchModal
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            subbranches_data: constants.SubBranchesConstants.GET_SUBBRANCHES_DATA,
        })
      },
      methods:{
        getData: function(){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.SubBranchesConstants.FETCH_SUBBRANCHES_DATA ,
                            id:null
                        })
                    .then(() => this.loading = false); 
        },
    },
    mounted(){
        this.getData();
    },

 
}