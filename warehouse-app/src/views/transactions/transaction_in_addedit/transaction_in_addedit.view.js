import { mapGetters } from 'vuex'
import constants from '@/store/const'

import TransactionInForm from '@/components/transaction_in/transaction_in_form/TransactionInForm.vue'
import TransactionDetailsTable from '@/components/transaction_details/transaction_details_table/TransactionDetailsTable.vue'
import TransactionDetailsForm from '@/components/transaction_details/transaction_details_form/TransactionDetailsForm';
import MaterialModal from '@/components/material/material_modal/MaterialModal.vue';


export default {
    name:'TransactionIns',
    data() {
        return {
            loading:true,
            transactionId:0,
            showDetailsForm:false,
            refreshDetails:false
        }
    },

    components:{
        TransactionInForm,
        TransactionDetailsTable,
        TransactionDetailsForm,
        MaterialModal
    },

    computed: {
        ...mapGetters({
            transaction:constants.transactionsConstants.GET_TRANSACTION_IN
         
        })
      },
      watch:{
        transaction: function(newVal, oldVal) {
            if(newVal!=null){
                this.transactionId = newVal.id;
            }
        }
      },
    mounted(){
        this.transactionId = this.$route.params.id;
    },

    methods:{
        showDetailForm:function(){
            if(this.transaction.id==null){
                toastr.options.progressBar = true;
                toastr.info('لا يمكنك اضافة التفاصيل قبل اضافة الحركة');

            }else{
                this.showDetailsForm = true;
                
            }
            document.getElementById('detailsForm').scrollIntoView({ behavior: 'smooth' });
           
        },
        hideDetailForm:function(){
            this.showDetailsForm = false;

        },
        transactionSaved:function(){
            this.refreshDetails= true;
           
        }
    }
}