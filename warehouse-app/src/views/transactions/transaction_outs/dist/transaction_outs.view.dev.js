"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vuex = require("vuex");

var _const = _interopRequireDefault(require("@/store/const"));

var _TransactionInFilters = _interopRequireDefault(require("@/components/transaction_in/transaction_in_filters/TransactionInFilters.vue"));

var _TransactionOutTable = _interopRequireDefault(require("@/components/transaction_out/transaction_out_table/TransactionOutTable.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = {
  name: 'TransactionOuts',
  data: function data() {
    return {
      loading: true,
      filters: null
    };
  },
  components: {
    TransactionInFilters: _TransactionInFilters["default"],
    TransactionOutTable: _TransactionOutTable["default"]
  },
  computed: _objectSpread({}, (0, _vuex.mapGetters)({
    transaction_out_data: _const["default"].transactionsConstants.GET_TRANSACTION_OUT_DATA,
    user: _const["default"].authConstants.GET_CURRENT_USER
  })),
  mounted: function mounted() {
    this.getTransactions();
  },
  methods: {
    search: function search(params) {
      this.filters = params;
      this.getTransactions();
    },
    getTransactions: function getTransactions() {
      var _this = this;

      this.loading = true;
      this.$store.dispatch(_const["default"].transactionsConstants.FETCH_TRANSACTION_OUT_DATA, this.filters).then(function () {
        return _this.loading = false;
      })["catch"](function () {
        _this.loading = false;
        toastr.options.progressBar = true;
        toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');
      });
    }
  }
};
exports["default"] = _default;