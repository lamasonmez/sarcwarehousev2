import { mapGetters } from 'vuex'
import constants from '@/store/const'

import TransactionInFilters from '@/components/transaction_in/transaction_in_filters/TransactionInFilters.vue'
import TransactionOutTable from '@/components/transaction_out/transaction_out_table/TransactionOutTable.vue'

export default {
    name:'TransactionOuts',
    data() {
        return {
            loading:true,
            filters:null,
           
        }
    },

    components:{
        TransactionInFilters,
        TransactionOutTable
    },

    computed: {
        ...mapGetters({
            transaction_out_data: constants.transactionsConstants.GET_TRANSACTION_OUT_DATA,
            user:constants.authConstants.GET_CURRENT_USER

        })
      },
    mounted(){
        this.getTransactions();
    },

    methods:{
        search: function(params) {
            this.filters = params;
            this.getTransactions();
        },
        getTransactions:function(param){
            this.loading=true;
            this.$store
                    .dispatch({
                       type: constants.transactionsConstants.FETCH_TRANSACTION_OUT_DATA,
                       filters: this.filters,
                       pagination:param
                    })
                    .then(() => {
                        this.loading = false;
                    })
                    .catch(()=>{ 
                        this.loading =false;
                        toastr.options.progressBar = true;
                        toastr.error('لقد حدث خطأ ما الرجاء المحاولة لاحقاً');}); 
        }
    }
}