import { mapGetters } from 'vuex'
import constants from '@/store/const'

import TransactionOutForm from '@/components/transaction_out/transaction_out_form/TransactionOutForm.vue'
import TransactionDetailsTable from '@/components/transaction_details/transaction_details_table/TransactionDetailsTable.vue'
import TransactionDetailsForm from '@/components/transaction_details/transaction_details_form/TransactionDetailsForm';
export default {
    name:'TransactionOutsAddedit',
    data() {
        return {
            loading:true,
            transactionId:0,
            showDetailsForm:false,
            refreshDetails:false
        }
    },

    components:{
        TransactionOutForm,
        TransactionDetailsTable,
        TransactionDetailsForm
    },

    computed: {
        ...mapGetters({
            transaction:constants.transactionsConstants.GET_TRANSACTION_OUT
         
        })
      },
      watch:{
        transaction: function(newVal, oldVal) {
            if(newVal!=null){
                this.transactionId = newVal.id;
            }
        }
      },
    mounted(){
        this.transactionId = this.$route.params.id;
    },

    methods:{
        showDetailForm:function(){
            if(this.transaction.id==null){
                toastr.options.progressBar = true;
                toastr.info('لا يمكنك اضافة التفاصيل قبل اضافة الحركة');

            }else{
                this.showDetailsForm = true;
                console.log(this.showDetailsForm);
                
            }
            document.getElementById('detailsForm').scrollIntoView({ behavior: 'smooth' });
           
        },
        hideDetailForm:function(){
            this.showDetailsForm = false;
            console.log(this.showDetailsForm);

        },
        transactionSaved:function(){
            this.refreshDetails= true;
           
        }
    }
}