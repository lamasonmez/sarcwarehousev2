"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vuex = require("vuex");

var _const = _interopRequireDefault(require("@/store/const"));

var _TransactionOutForm = _interopRequireDefault(require("@/components/transaction_out/transaction_out_form/TransactionOutForm.vue"));

var _TransactionDetailsTable = _interopRequireDefault(require("@/components/transaction_details/transaction_details_table/TransactionDetailsTable.vue"));

var _TransactionDetailsForm = _interopRequireDefault(require("@/components/transaction_details/transaction_details_form/TransactionDetailsForm"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = {
  name: 'TransactionOutsAddedit',
  data: function data() {
    return {
      loading: true,
      transactionId: 0,
      showDetailsForm: false,
      refreshDetails: false
    };
  },
  components: {
    TransactionOutForm: _TransactionOutForm["default"],
    TransactionDetailsTable: _TransactionDetailsTable["default"],
    TransactionDetailsForm: _TransactionDetailsForm["default"]
  },
  computed: _objectSpread({}, (0, _vuex.mapGetters)({
    transaction: _const["default"].transactionsConstants.GET_TRANSACTION_OUT
  })),
  watch: {
    transaction: function transaction(newVal, oldVal) {
      if (newVal != null) {
        this.transactionId = newVal.id;
      }
    }
  },
  mounted: function mounted() {
    this.transactionId = this.$route.params.id;
  },
  methods: {
    showDetailForm: function showDetailForm() {
      if (this.transaction.id == null) {
        toastr.options.progressBar = true;
        toastr.info('لا يمكنك اضافة التفاصيل قبل اضافة الحركة');
      } else {
        this.showDetailsForm = true;
        console.log(this.showDetailsForm);
      }

      document.getElementById('detailsForm').scrollIntoView({
        behavior: 'smooth'
      });
    },
    hideDetailForm: function hideDetailForm() {
      this.showDetailsForm = false;
      console.log(this.showDetailsForm);
    },
    transactionSaved: function transactionSaved() {
      this.refreshDetails = true;
    }
  }
};
exports["default"] = _default;