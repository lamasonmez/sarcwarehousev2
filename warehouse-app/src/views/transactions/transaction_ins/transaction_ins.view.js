import { mapGetters } from 'vuex'
import constants from '@/store/const'

import TransactionInTable from '@/components/transaction_in/transaction_in_table/TransactionInTable.vue'
import TransactionInFilters from '@/components/transaction_in/transaction_in_filters/TransactionInFilters.vue'

export default {
    name:'TransactionIns',
    data() {
        return {
            loading:true,
            filters:null,
        }
    },

    components:{
        TransactionInTable,
        TransactionInFilters
    },

    computed: {
        ...mapGetters({
            transaction_in_data: constants.transactionsConstants.GET_TRANSACTION_IN_DATA,
            user:constants.authConstants.GET_CURRENT_USER

        })
      },
    mounted(){
        this.getTransactions();
    },

    methods:{
        search: function(params) {
            this.filters = params;
            this.getTransactions();
        },
        getTransactions:function(param){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.transactionsConstants.FETCH_TRANSACTION_IN_DATA ,
                            filters: this.filters,
                            pagination: param 
                        })
                    .then(() => this.loading = false); 
        }
    }
}