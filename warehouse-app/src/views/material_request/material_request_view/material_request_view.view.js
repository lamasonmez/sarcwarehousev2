import {mapGetters,mapActions} from 'vuex'
import constants from '@/store/const'
import MaterialForm from '@/components/material/material_form/MaterialForm.vue'
import { toDisplayString } from 'vue'

export default {
    name:'MaterialRequestView',
    data(){
        return {
            loading:false,
            loading_approve:false,
            loading_reject:false,
            request_id:null,
            material_request:null
        }
    },
    components:{
        MaterialForm
    },
    computed:{
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            pending_material_requests: constants.materialRequestConstants.GET_MATERIAL_PENDING_REQUESTS,

        }),
    },
    methods:{
        ...mapActions({
            fetchMaterialRequest: constants.materialRequestConstants.FETCH_MATERIAL_REQUEST_ITEM,
            storMaterialRequest :constants.materialRequestConstants.STORE_MATERIAL_REQUEST_ITEM,

        }),
        getRequest:function(){
            this.loading =true;
                this.fetchMaterialRequest(this.request_id).then((response)=>{
                    this.loading =false;
                    this.material_request = response;
                }).catch(()=>this.loading=false);
        },
        ApporveRequest:function(){
            this.loading_approve=true;
            this.material_request.status="approved";
            this.storMaterialRequest(this.material_request).then(()=>{
                this.loading_approve=false;
                toastr.success('تم قبول المادة بنجاح')
            }).catch(()=>{
                this.loading_approve=false;
                toastr.error('حدث خطأ ما الرجاء المحاولة لاحقاً')
            });
        },
        RejectRequest:function(){
            this.loading_reject=true;
            this.material_request.status="rejected";
            this.storMaterialRequest(this.material_request).then(()=>{
                this.loading_reject=false;
                toastr.success('تم رفض المادة بنجاح')
            }).catch(()=>{
                this.loading_reject=false;
                toastr.error('حدث خطأ ما الرجاء المحاولة لاحقاً')
            });;

        }
    },
    created(){
        this.request_id= this.$route.params.id; 
       this.getRequest(this.$route.params.id);
    }
}