import { mapGetters } from 'vuex'
import constants from '@/store/const'

import DonorsTable from '@/components/donors/donors_table/DonorsTable.vue'
import DonorModal from '@/components/donors/donor_modal/DonorModal.vue'



export default {
    name:'DonorsIndex',
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        DonorsTable,
        DonorModal
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            donors_data: constants.donorConstants.GET_DONORS_DATA,
        })
      },
      methods:{
        getData: function(){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.donorConstants.FETCH_DONORS_DATA 
                        })
                    .then(() => this.loading = false); 
        },
    },
    mounted(){
        this.getData();
    },

 
}