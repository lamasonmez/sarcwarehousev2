import { mapGetters } from 'vuex'
import constants from '@/store/const'

import UnitsTable from '@/components/units/units_table/UnitsTable.vue'
import UnitModal from '@/components/units/unit_modal/UnitModal.vue'



export default {
    name:'UnitsIndex',
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        UnitsTable,
        UnitModal
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            units_data: constants.UnitsConstants.GET_UNITS_DATA,


        })
      },
      methods:{
        getData: function(){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.UnitsConstants.FETCH_UNITS_DATA 
                        })
                    .then(() => this.loading = false); 
        },
    },
    mounted(){
        this.getData();
    },

 
}