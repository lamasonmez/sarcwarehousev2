import { mapGetters } from 'vuex'
import constants from '@/store/const'

import MaterialForm from '@/components/material/material_form/MaterialForm.vue'

export default {
    name:'MaterialAddEdit',
    data() {
        return {
            loading:true,
            Id:0,
        }
    },

    components:{
        MaterialForm,
    },

    computed: {
        ...mapGetters({
            user:constants.UsersConstants.GET_USER_ITEM
         
        })
      },
      watch:{
        user: function(newVal, oldVal) {
            if(newVal!=null){
                this.Id = newVal.id;
            }
        }
      },
    mounted(){
        this.Id = this.$route.params.id;
    },

    methods:{
     
    }
}