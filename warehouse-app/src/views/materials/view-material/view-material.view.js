import constants from '@/store/const'
import { mapGetters } from 'vuex'
import MaterialTransaction from '@/components/material/material_transaction/MaterialTransaction.vue';

export default {
    name:'ViewMaterial',
    
    data() {
        return {
            loading:false,
            
        }
    },
    components:{
        MaterialTransaction
    },
    computed: {
        ...mapGetters({
            transaction_detail:constants.transactionsConstants.GET_TRANSACTION_DETAIL_ITEM,
            material_transactions:constants.inventorysConstants.GET_MATERIAL_TRANSACTIONS,
        })
      },

    methods:{
        getTransactionDetail:function(id){
            this.loading = true;
            this.$store
                .dispatch(constants.transactionsConstants.FETCH_TRANSACTION_DETAIL_ITEM,id)
                .then(() => {
                    this.getMaterialTransactions(this.transaction_detail.donor_material_id);
                    })
                .catch((response)=>{
                    console.log('error',response);
                    this.loading=false;
                }); 
        },
        getMaterialTransactions:function(donor_material_id){
            this.loading = true;
            this.$store.dispatch(
                constants.inventorysConstants.FETCH_MATERIAL_TRANSACTIONS,donor_material_id
            ).then(()=>{this.loading=false;})
            .catch((error)=>{
                console.log('error in getMaterialTransactions',error);
                this.loading= false;
            })
        }
       
      },
    
    mounted(){
       
        this.getTransactionDetail(this.$route.params.transaction_detail_id)
      }
   
}

