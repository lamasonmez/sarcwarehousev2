import constants from '@/store/const'
import { mapGetters } from 'vuex'
import MaterialsTable from '@/components/material/material_table/MaterialTable.vue';

export default {
    name:'MaterialIndex',
    
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        MaterialsTable
    },
    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            materails_data: constants.donorMaterialConstants.GET_DONOR_MATERIALS_DATA,

        })
      },
    methods:{
        getData: function(){
            this.loading=true;
            if(this.user&&this.user.role.length>0 && this.user.role[0]=='donor'){
                this.$store.dispatch(
                    constants.donorMaterialConstants.FETCH_DONOR_MATERIALS_DATA,
                    this.user.rolable_id
                    ).then(()=>{
                    this.loading=false;
                })
            }else{
            this.$store.dispatch(constants.donorMaterialConstants.FETCH_DONOR_MATERIALS_DATA )
            .then(()=>{
                this.loading=false;
            }) }
        },
       
      },
    
    mounted(){
       
        this.getData();
    
      }
   
}

