"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _TotalMaterialsCount = _interopRequireDefault(require("@/components/home/total_materials_count/TotalMaterialsCount.vue"));

var _TotalInCount = _interopRequireDefault(require("@/components/home/total_in_count/TotalInCount.vue"));

var _TotalOutCount = _interopRequireDefault(require("@/components/home/total_out_count/TotalOutCount.vue"));

var _MaterialInventoryTable = _interopRequireDefault(require("@/components/home/material_inventory_table/MaterialInventoryTable.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  name: 'Home',
  components: {
    TotalMaterialsCount: _TotalMaterialsCount["default"],
    TotalInCount: _TotalInCount["default"],
    TotalOutCount: _TotalOutCount["default"],
    MaterialInventoryTable: _MaterialInventoryTable["default"]
  },
  methods: {}
};
exports["default"] = _default;