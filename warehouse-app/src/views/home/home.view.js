import TotalMaterialsCount from '@/components/home/total_materials_count/TotalMaterialsCount.vue'
import TotalInCount from '@/components/home/total_in_count/TotalInCount.vue'
import TotalOutCount from '@/components/home/total_out_count/TotalOutCount.vue'
import MaterialInventoryTable from '@/components/home/material_inventory_table/MaterialInventoryTable.vue';
export default {
    name:'Home',
    components:{
        TotalMaterialsCount,
        TotalInCount,
        TotalOutCount,
        MaterialInventoryTable
    },
    methods:{
    }
}