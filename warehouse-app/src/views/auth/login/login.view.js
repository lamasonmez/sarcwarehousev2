import {ref} from 'vue';
import useVuelidate from '@vuelidate/core';
import {required,email,minLength} from '@vuelidate/validators';
import constants from '@/store/const';

const logger = v =>{
    return true;
}
export default {
    setup(){
        const password = ref('');
        const user_email = ref('');
        const rules = {
            password:{
                required,
                minLength:minLength(6),
                autoDirty:true
            },
            user_email:{
                required,
                email,
                autoDirty:true,
                logger
            }
        }
        const validator$= useVuelidate(
            rules,
            {password,user_email}
        );
        return { validator$,password,email, }
    },
    components: {
      },
    name:'Login',
    data() {
        return {
            loading:false,
            error_message:''
        }
    },
    methods: {
        login() {
            this.loading = true;
            this.$store
                .dispatch(constants.authConstants.LOGIN_ACTION, {
                    email: this.validator$.user_email.$model,
                    password: this.validator$.password.$model
                })
                .then((user) => {
                    console.log('logn then',user);
                    if(user && user.role.length>0 && user.role[0]=='donor'){
                        this.$router.push({name:"MaterialIndex"})
                    }
                    else{
                    this.$router.push({name: "home"})

                    }
                })
                .catch(()=>this.loading=false);
        }
    }
}




