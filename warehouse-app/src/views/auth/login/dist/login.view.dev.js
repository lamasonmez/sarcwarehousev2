"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = require("vue");

var _core = _interopRequireDefault(require("@vuelidate/core"));

var _validators = require("@vuelidate/validators");

var _const = _interopRequireDefault(require("@/store/const"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var logger = function logger(v) {
  console.log('logger called with : ', v);
  return true;
};

var _default = {
  setup: function setup() {
    var password = (0, _vue.ref)('');
    var user_email = (0, _vue.ref)('');
    var rules = {
      password: {
        required: _validators.required,
        minLength: (0, _validators.minLength)(6),
        autoDirty: true
      },
      user_email: {
        required: _validators.required,
        email: _validators.email,
        autoDirty: true,
        logger: logger
      }
    };
    var validator$ = (0, _core["default"])(rules, {
      password: password,
      user_email: user_email
    });
    return {
      validator$: validator$,
      password: password,
      email: _validators.email
    };
  },
  components: {},
  name: 'Login',
  data: function data() {
    return {
      loading: false,
      error_message: ''
    };
  },
  methods: {
    login: function login() {
      var _this = this;

      this.loading = true;
      this.$store.dispatch(_const["default"].authConstants.LOGIN_ACTION, {
        email: this.validator$.user_email.$model,
        password: this.validator$.password.$model
      }).then(function () {
        return _this.$router.push({
          name: "home"
        });
      })["catch"](function () {
        return _this.loading = false;
      });
    }
  }
};
exports["default"] = _default;