import { mapGetters } from 'vuex'
import constants from '@/store/const'

import UsersTable from '@/components/users/users_table/UsersTable.vue'



export default {
    name:'UsersIndex',
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        UsersTable,
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            users_data: constants.UsersConstants.GET_USERS_DATA,
        })
      },
      methods:{
        getData: function(){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.UsersConstants.FETCH_USERS_DATA 
                        })
                    .then(() => this.loading = false); 
        },
    },
    mounted(){
        this.getData();
    },

 
}