import { mapGetters } from 'vuex'
import constants from '@/store/const'

import UserForm from '@/components/users/user_form/UserForm.vue'


export default {
    name:'UserAddEdit',
    data() {
        return {
            loading:true,
            userId:0,
        }
    },

    components:{
        UserForm,
    },

    computed: {
        ...mapGetters({
            user:constants.UsersConstants.GET_USER_ITEM
         
        })
      },
      watch:{
        user: function(newVal, oldVal) {
            if(newVal!=null){
                this.userId = newVal.id;
            }
        }
      },
    mounted(){
        this.userId = this.$route.params.id;
    },

    methods:{
     
    }
}