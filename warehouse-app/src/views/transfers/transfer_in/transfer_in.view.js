import {mapActions, mapGetters } from 'vuex'
import constants from '@/store/const'

import TransactionInForm from '@/components/transaction_in/transaction_in_form/TransactionInForm.vue'
import TransactionDetailsTable from '@/components/transaction_details/transaction_details_table/TransactionDetailsTable.vue'
import MaterialModal from '@/components/material/material_modal/MaterialModal.vue';


export default {
    name:'TransferIn',
    data() {
        return {
            loading:true,
            transactionId:0,
            showDetailsForm:false,
            refreshDetails:false,
        }
    },

    components:{
        TransactionInForm,
        TransactionDetailsTable,
        MaterialModal
    },

    computed: {
        ...mapGetters({
            transaction:constants.transactionsConstants.GET_TRANSACTION_IN,
            details:constants.transactionsConstants.GET_TRANSACTION_DETAILS,
         
        })
      },
      watch:{
        transaction: function(newVal, oldVal) {
            if(newVal!=null){
                this.transactionId = newVal.id;
                this.getDetails(this.transaction.transfer.transaction_out_id);
            }
        }
      },
    mounted(){
        this.transactionId = this.$route.params.id;
    },

    methods:{
        ...mapActions({
            getDetails:constants.transactionsConstants.FETCH_TRANSACTION_OUT_DETAILS,
            storeDetail:constants.transactionsConstants.STORE_TRANSACTION_DETAIL_ITEM,
            storeTransaction:constants.transactionsConstants.STORE_TRANSACTION_IN,
            fetchPendingTransactions:constants.notificationsConstants.FETCH_PENDING_TRANSACTIONS


        }),
        transactionSaved:function(){
            this.refreshDetails= true; 
        },
        saveTransfer:function(){
            this.loading=true;
            this.transaction.status="approved";
            this.storeTransaction(this.transaction);
            for(let item of this.details){
                let data = {
                    id:null,
                    quantity:item.quantity,
                    notes:null,
                    is_convoy:item.is_convoy,
                    loss:null,
                    reason_for_loss:null,
                    reason_for_damage:null,
                    damage:null,
                    expiration_date:item.expiration_date,
                    transactionable_type:'App\\Models\\TransactionIn',
                    transactionable_id:this.transactionId,
                    transaction_type_id:2,
                    ctn_id:item.ctn_id,
                    donor_material_id:item.donor_material_id,
                    warehouse_id:this.transaction.warehouse_id,
                };
                console.log('store data',data);
                this.storeDetail(data);
            }
            this.loading=false;
            toastr.success('تم الحفظ بنجاح');
            this.fetchPendingTransactions();
            this.$router.push({name: "Income"});
        }

       
    },
    
}