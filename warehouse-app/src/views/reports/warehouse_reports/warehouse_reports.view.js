import Filters from "@/components/reports/filters/Filters.vue"
import WarehouseReportTable from "@/components/reports/warehouse-report-table/WarehouseReportTable.vue"
import constants from '@/store/const'
import { mapGetters } from 'vuex'



export default {
    name:"WarehouseReports",
    data() {
        return {
            loading:false,
            filters:null,
            warehouse_report:[]
        }
    },
    components:{
        Filters,
        WarehouseReportTable
    },
    computed: {
        ...mapGetters({
            warehouse_report: constants.warehouseConstants.GET_WAREHOUSE_REPORT,
            user:constants.authConstants.GET_CURRENT_USER

        })
    },
    methods:{
        search:function(param){
            this.loading=true;
            this.filters = param;
           // this.getWarehouseReport();
        },
        getWarehouseReport:function(){
            this.$store
            .dispatch(
                constants.warehouseConstants.FETCH_WAREHOUSE_REPORT,
                this.filters
                )
            .then(() => this.loading = false); 
        }
        
    }
}