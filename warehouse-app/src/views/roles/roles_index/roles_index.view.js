import { mapGetters } from 'vuex'
import constants from '@/store/const'

import RolesTable from '@/components/roles/roles_table/RolesTable.vue'
import RoleModal from '@/components/roles/role_modal/RoleModal.vue'



export default {
    name:'SectorsIndex',
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        RolesTable,
        RoleModal
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            roles_data: constants.RolesConstants.GET_ROLES_DATA,
        })
      },
      methods:{
        getData: function(){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.RolesConstants.FETCH_ROLES_DATA 
                        })
                    .then(() => this.loading = false); 
        },
    },
    mounted(){
        this.getData();
    },

 
}