import { mapGetters } from 'vuex'
import constants from '@/store/const'

import BranchesTable from '@/components/branches/branches_table/BranchesTable.vue'
import BranchModal from '@/components/branches/branch_modal/BranchModal.vue'



export default {
    name:'BranchesIndex',
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        BranchesTable,
        BranchModal
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            branches_data: constants.BranchesConstants.GET_BRANCHES_DATA,
        })
      },
      methods:{
        getData: function(){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.BranchesConstants.FETCH_BRANCHES_DATA 
                        })
                    .then(() => this.loading = false); 
        },
    },
    mounted(){
        this.getData();
    },

 
}