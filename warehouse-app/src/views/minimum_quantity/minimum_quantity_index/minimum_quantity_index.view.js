import { mapGetters } from 'vuex'
import constants from '@/store/const'

import MinimumQuantityTable from '@/components/minimum_quantity/minimum_quantity_table/MinimumQuantityTable.vue'
import MinimumQuantityModal from '@/components/minimum_quantity/minimum_quantity_modal/MinimumQuantityModal.vue'

export default {
    name:'MinimumQuantityIndex',
    data() {
        return {
            loading:true,
            refreshData:false,
            showModal:false,
        }
    },
    components:{
        MinimumQuantityTable,
        MinimumQuantityModal
    },

    computed: {
        ...mapGetters({
            user:constants.authConstants.GET_CURRENT_USER,
            minimum_quantity_data: constants.MinimumQuantityConstants.GET_MINIMUM_QUANTITY_DATA,

        })
      },
      methods:{
        getData: function(param){
            this.loading=true;
            this.$store
                    .dispatch(
                        {
                            type:constants.MinimumQuantityConstants.FETCH_MINIMUM_QUANTITY_DATA ,
                            pagination: param 
                        })
                    .then(() => this.loading = false); 
        },
    },
    mounted(){
        this.getData();
    },

 
}