import axios from "axios";

import JwtService from "@/services/jwt.service";
import {VUE_APP_API_URL} from "@/config";

const ApiService = {
    init(){
        axios.defaults.baseURL = VUE_APP_API_URL;
    },

    setHeader(){
        axios.defaults.headers.common = {
            "Authorization":`Bearer ${JwtService.getToken()}`,
            //LOCAL HOST CONF
            // 'X-Requested-With': 'XMLHttpRequest',
            // 'Access-Control-Allow-Origin':'*',
            // 'Access-Control-Request-Method':'*'
            //LOCAL HOST CONF
        };
    },
    setResponseType($type){
        axios.defaults.headers.responseType = $type;
        axios.responseType = $type;
    },
    get(resource, slug = "") {
        this.setHeader();
         return axios.get(`${resource}`).catch(error => {
             throw new Error(`[RWV] ApiService ${error}`);
             console.log(error);
         });
     },
    
    post(resource, params) {
        return axios.post(`${resource}`, params).catch(error => {
            throw new Error(`[RWV] ApiService ${error}`);
       
            console.log(error);
        });
        }
};

export default ApiService;