"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _jwt = _interopRequireDefault(require("@/services/jwt.service"));

var _config = require("@/config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ApiService = {
  init: function init() {
    _axios["default"].defaults.baseURL = _config.VUE_APP_API_URL;
  },
  setHeader: function setHeader() {
    _axios["default"].defaults.headers.common = {
      'X-Requested-With': 'XMLHttpRequest',
      "Authorization": "Bearer ".concat(_jwt["default"].getToken())
    };
  },
  get: function get(resource) {
    var slug = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    return _axios["default"].get("".concat(resource, "/").concat(slug))["catch"](function (error) {
      throw new Error("[RWV] ApiService ".concat(error));
    });
  },
  post: function post(resource, params) {
    return _axios["default"].post("".concat(resource), params);
  }
};
var _default = ApiService;
exports["default"] = _default;