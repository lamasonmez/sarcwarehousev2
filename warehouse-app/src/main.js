import { createApp } from 'vue'
import router from '@/router'
import App from '@/App.vue'
import ApiService from '@/api/api.service';
import store from '@/store';
import { VuelidatePlugin } from '@vuelidate/core'
import filters from './util/filters';
import VueEasyLightbox from 'vue-easy-lightbox'
window.$ = window.jQuery = require('jquery');

ApiService.init();
const app = createApp(App);
app.config.devtools = true;
app.config.productionTip = true;
app.use(store);
app.use(router);
app.use(VuelidatePlugin);
app.use(VueEasyLightbox);
app.mount('#app');

app.config.globalProperties.$filters = filters



// externale .js
