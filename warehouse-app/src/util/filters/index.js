import moment from 'moment'

export default {
    getMonthInAr(value){
        let d = new Date(value);
        return d.toLocaleString("ar-SY", { month: "long" });
    },
    getMonthInEn(value){
        let d = new Date(value);
        return d.toLocaleString("en-US", { month: "long" });
    },
    timefromNow(value){
        moment.locale('ar');
        return moment(value).fromNow()
    },
    formatDate(value){
        return moment(value).format('DD-MM-YYYY, h:mm:ss A')
    }

}

