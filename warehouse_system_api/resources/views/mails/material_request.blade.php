@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<h1 style="font-size: 38px; font-weight: 600; margin: 2;text-align:center;direction:ltr">
    New Material Request
</h1>

@endcomponent
@endslot
<tr>
    <td bgcolor="#ffffff" align="center"
        style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Cairo', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;text-align:center">
        <p style="margin: 0;text-align:left">
            Hello {{ $admin->full_name->en }}
        </p>
        <p style="margin-top:30px;text-align:left ">
            There is a new materil Request
            <br />
            <strong> From : </strong> {{ $user->full_name->en }}
            <br />
            <strong> Branch: </strong> {{ $user->branchable->name->en }} {{ $user->branchable->name->ar }}
            <br />
            <strong>Warehouse: </strong>{{ $user->warehouse->name->en }} {{  $user->branchable->name->ar }}
            <br />
            <strong> Donor - Material :</strong>{{ $material_request->donor_material->donor->code }} -
            {{ $material_request->donor_material->material->name->en }}
            {{ $material_request->donor_material->material->name->ar }}
        </p>
        <p style="margin-top:30px;text-align:center">
            To View Material Details Please Click On The Button Below
        </p>
    </td>
</tr>

@component('mail::button', ['url' =>"http://localhost:8080/material_request/".$material_request->id])
View Details
@endcomponent


{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
