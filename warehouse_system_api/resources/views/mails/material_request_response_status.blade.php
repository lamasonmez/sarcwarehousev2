@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
<h1 style="font-size: 38px; font-weight: 600; margin: 2;text-align:center;direction:ltr">
    Material Request Update
</h1>

@endcomponent
@endslot
<tr>
    <td bgcolor="#ffffff" align="center"
        style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Cairo', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;text-align:center">
        <p style="margin: 0;text-align:left">
            Hello {{ $material_request->user->full_name->en }}
        </p>
        <p style="margin-top:30px;text-align:left ">
            The HQ has responded to your material request with
            <strong style="color: {{ $material_request->status=='approved' ? 'green' : 'red' }}">
                {{ $material_request->status=='approved' ? 'apprvoal' : 'rejection'}} </strong>
            <br />
            @if($material_request->status=='rejected')
            And has specified this reason <strong>{{ $material_request->rejected_reason }}</strong>
            <br />
            @endif
            Material Details:
            <br />
            <strong> Name Arabic : </strong> {{ $material_request->donor_material->material->name->ar }}
            <br />
            <strong> Name English : </strong> {{ $material_request->donor_material->material->name->en }}
            <br />
            <strong> Donor :</strong>{{ $material_request->donor_material->donor->code }}
            <br />
            <strong> Sector : </strong> {{ $material_request->donor_material->material->sector->name->ar }}
            <br />
            <strong>Unit: </strong>{{ $material_request->donor_material->unit->name->ar  }}
            <br />
            <strong>Size: </strong>{{ $material_request->donor_material->size  }}
            <br />
            <strong>Weight: </strong>{{ $material_request->donor_material->weight  }}
            <br />
            <strong>Content: </strong>{{ $material_request->donor_material->content }}
        </p>
        <br />
        @if($material_request->status=='approved')
        You Can Now Use this material
        @endif
    </td>
</tr>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
