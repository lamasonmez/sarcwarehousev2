<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table>
    <tbody style="direction: rlt">
        <tr>
            <td colspan="8" style="border-bottom:0px solid black">
                <span>
                    <img src="{{ public_path('images/logo.jpg') }}" style="width:50px">
                </span>
            </td>
            <td colspan="21"
                style="display:flex;justify-content:space-between;align-items:center;border-bottom:0px solid black">

                <h3 style="align-items: center;font-size:30px;">
                    <strong> الهلال الأحمـــر العربي السوري</strong>
                    <br />
                    <strong> Syrian Arab Red Crescent</strong>
                    <br />
                    <strong>التقـــــــرير الدوري لمخــــزون المستودعــــــــات</strong>
                    <br />
                    <strong> Warehouse Stock Report</strong>
                </h3>
            </td>
        </tr>
        <tr>
            <th class="">
                <span>الشهر</span>
            </th>
            <th class="">
                <span>الفرع</span>
            </th>
            <th class="">
                <span>الشعبة</span>
            </th>
            <th class="">
                <span>إسم المستودع</span>
            </th>
            <th class="">
                <span>الشريك</span>
            </th>
            <th class="">
                <span>المادة</span>
            </th>
            <th class="">
                <span>القطاع</span>
            </th>
            <th class="">
                <span>الوحدة</span>
            </th>
            <th class="">
                <span>رقم المتابعة</span>
            </th>
            <th class="">
                <span>مواد أخرى</span>
            </th>
            <th class="">
                <span>الرصيد الافتتاحي</span>
            </th>
            <th class="">
                <span>إجمالي الوارد</span>
            </th>
            <th class="">
                <span>تاريخ الاستلام</span>
            </th>
            <th class="">
                <span>رقم مذكرة الإستلام</span>
            </th>
            <th class="">
                <span>إجمالي الصادر</span>
            </th>
            <th class="">
                <span>رقم مذكرة التسليم</span>
            </th>
            <th class="">
                <span>المنطقة المرسل إاليها</span>
            </th>
            <th class="">
                <span>تاريخ الإرسال</span>
            </th>
            <th class="">
                <span>الفاقد</span>
            </th>
            <th class="">
                <span>سبب الفقد</span>
            </th>
            <th class="">
                <span>التالف</span>
            </th>
            <th class="">
                <span>سبب التلف</span>
            </th>
            <th class="">
                <span>فرق الجرد</span>
            </th>
            <th class="">
                <span>الرصيد المتبقي</span>
            </th>
            <th class="">
                <span>التورزيع</span>
            </th>
            <th class="">
                <span>مرتجع</span>
            </th>
            <th class="">
                <span>مناقلة</span>
            </th>
            <th class="">
                <span>قوافل</span>
            </th>
            <th class="">
                <span>ملاحظات</span>
            </th>
        </tr>
        <tr>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Month</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Branch</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Sub-Branch</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Warehouse Name</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Partner</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Item</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Sector</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Unit</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">CTN</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Other Items</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Open. Balance</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Total IN</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Receiving Date</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Goods Received Note</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Total OUT</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Delivery Note</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Location Of Sent Item</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Sending Date</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Loss</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Reason for Loss</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Damage</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Reasons for Damage</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Inventory</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Closing Balance</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Distribution</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Return</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Transfer</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Convoys</div>
            </th>
            <th style="text-align:center">
                <div class="font-weight-bold text-muted">Notes</div>
            </th>
        </tr>
        @php
        $keys = $data->keys();
        @endphp
        @for($i=0;$i<count($keys); $i++) @foreach($data[$keys[$i]] as $key=> $item)
            <tr>
                <td>
                    {{ Carbon\Carbon::createFromFormat('Y-m-d',$item->date)->locale('ar-sy')->getTranslatedMonthName("Do
                    MMMM") }} - {{
                    Carbon\Carbon::createFromFormat('Y-m-d',$item->date)->locale('en')->getTranslatedMonthName("Do
                    MMMM") }}
                </td>
                <td>
                    @if($item->warehouse->branchable_type==App\Models\Branch::class)
                    {{ $item->warehouse->branchable->name->ar }} - {{ $item->warehouse->branchable->name->en }}
                    @else
                    {{ $item->warehouse->branchable->branch->name->ar }} - {{
                    $item->warehouse->branchable->branch->name->en }}

                    @endif
                </td>
                <td>
                    @if($item->warehouse->branchable_type==App\Models\Branch::class)
                    {{ $item->warehouse->branchable->name->ar }} - {{ $item->warehouse->branchable->name->en }}
                    @else
                    {{ $item->warehouse->branchable->name->ar }} - {{ $item->warehouse->branchable->name->en }}

                    @endif
                </td>
                <td>
                    مستودع {{ $item->warehouse->name->ar }}
                </td>
                <td>
                    {{ $item->material_donor->donor->code }}
                </td>
                <td>
                    {{ $item->material_donor->material->name->ar }} - {{ $item->material_donor->material->name->en }}
                </td>
                <td>
                    {{ $item->material_donor->material->sector->name->en }}
                </td>
                <td>
                    {{ $item->material_donor->unit->name->ar }} - {{ $item->material_donor->unit->name->en }}
                </td>
                <td>
                    {{$item->ctn ? $item->ctn->name->en :'' }}
                </td>
                <td>

                </td>
                <td>
                    @if($key==0)
                    {{ $item->opening_balance }}
                    @endif
                </td>
                <td>
                    @if($item->transactionable_type==App\Models\TransactionIn::class)
                    {{ $item->quantity }}
                    @endif
                </td>
                <td>
                    @if($item->transactionable_type==App\Models\TransactionIn::class)
                    {{ $item->date }}
                    @endif
                </td>
                <td>
                    @if($item->transactionable_type==App\Models\TransactionIn::class)
                    {{ $item->transactionable->waybill_no }}
                    @endif
                </td>
                <td>
                    @if($item->transactionable_type==App\Models\TransactionOut::class)
                    {{ $item->quantity }}
                    @endif
                </td>
                <td>
                    @if($item->transactionable_type==App\Models\TransactionOut::class)
                    {{ $item->transactionable->waybill_no }}
                    @endif
                </td>
                <td>
                    @if($item->transactionable_type==App\Models\TransactionOut::class)
                    @if($item->transactionable->toable_type==App\Models\DistributionPoint::class)
                    {{ $item->transactionable->toable->name }}
                    @else
                    {{ $item->transactionable->toable->name->ar }}
                    @endif
                    @endif
                </td>
                <td>
                    @if($item->transactionable_type==App\Models\TransactionOut::class)
                    {{ $item->date }}
                    @endif
                </td>
                <td>
                    {{$item->loss }}
                </td>
                <td>
                    {{ $item->reason_for_loss }}
                </td>
                <td>
                    {{$item->damage }}
                </td>
                <td>
                    {{ $item->reason_for_damage }}
                </td>
                <td>

                </td>
                <td>
                    @if($key==count($data[$keys[$i]])-1)
                    {{ $item->closing_balance }}
                    @endif
                </td>
                <td></td>
                <td></td>
                <td>
                    @if($item->transaction_type)
                    {{ $item->transaction_type->name->ar }} - {{ $item->transaction_type->name->en }}
                    @endif
                </td>
                <td>
                    {{ $item->isconvoy }}
                </td>
                <td>
                    {{ $item->notes }}
                </td>
            </tr>
            @endforeach
            @endfor

    </tbody>
</table>
