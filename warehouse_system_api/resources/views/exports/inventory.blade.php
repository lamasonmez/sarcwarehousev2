<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table>
    <tbody style="direction: rlt">
        <tr>
            <td colspan="5"
                style="display:flex;justify-content:space-between;align-items:center;border:1px solid black">
                <h3 style="align-items: center">منظمة الهلال الأحمر العربي السوري فرع حلب</h3>
                <img src="{{ public_path('images/Sarc.jpg') }}">
            </td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center;background-color:gray;border:1px solid black;">
                @if(\Auth::user('api')->warehouse)
                <h3>تقرير بجرد مستودع {{ \Auth::user('api')->warehouse->name->ar }}</h3>
                @elseif(\Auth::user('api')->branchable)
                <h3>تقرير بجرد مستودعات {{ \Auth::user('api')->branchable->name->ar }}</h3>
                @endif
            </td>
        </tr>
        @foreach($inventory as $element)
        <tr>
            <td colspan="5" style="background-color:gray;border:1px solid black;text-align:center">
                {{ $element[0]->transaction->material_donor->donor->code }}
            </td>
        </tr>
        <tr>
            <td style="border:1px solid black;padding:0px 10px;"><strong>المادة</strong></td>
            <td style="border:1px solid black;padding:0px 10px;"><strong> الافتتاحي</strong></td>
            <td style="border:1px solid black;padding:0px 10px;"><strong>الوارد </strong></td>
            <td style="border:1px solid black;padding:0px 10px;"><strong> الصادر</strong></td>
            <td style="border:1px solid black;padding:0px 10px;"><strong>الرصيد </strong></td>
        </tr>
        @foreach($element as $item)
        <tr>
            <td style="border:1px solid black;font-weight:900;padding:0px 10px;">
                {{ $item->transaction->material_donor->material->name->ar }}</td>
            <td style="border:1px solid black;font-weight:900;padding:0px 10px;">{{ $item->opening_balance }}</td>
            <td style="border:1px solid black;font-weight:900;padding:0px 10px;">{{ $item->total_in }}</td>
            <td style="border:1px solid black;font-weight:900;padding:0px 10px;">{{ $item->total_out }}</td>
            <td style="border:1px solid black;font-weight:900;padding:0px 10px;">{{ $item->closing_balance }}</td>
        </tr>

        @endforeach
        @endforeach
    </tbody>
</table>
