<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table>
    <tbody style="direction: rlt">
        <tr>
            <td colspan="5"
                style="display:flex;justify-content:space-between;align-items:center;border:1px solid black">
                <h3 style="align-items: center">منظمة الهلال الأحمر العربي السوري فرع حلب</h3>
                <img src="{{ public_path('images/Sarc.jpg') }}">
            </td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center;background-color:gray;border:1px solid black;">
                @if(\Auth::user('api')->warehouse)
                <h3>مستودع {{ \Auth::user('api')->warehouse->name->ar }}</h3>
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center;background-color:gray;border:1px solid black;">
                @if($data[0] && $data[0]->material_donor)
                <h3>حركة المادة {{ $data[0]->material_donor->donor->code  }} -
                    {{ $data[0]->material_donor->material->name->ar }}</h3>
                @endif
            </td>
        </tr>
        <tr>
            <th>تاريخ</th>
            <th>من/إلى</th>
            <th>وارد</th>
            <th>صادر</th>
            <th>كمية متبقية</th>
        </tr>
        @foreach($data as $element)
        <tr>
            <td style="text-align: right">{{ $element->date }}</td>
            <td>
                @if($element->transactionable_type=='App\\Models\\TransactionIn')
                {{ $element->transactionable->fromable->name->en }}
                @elseif( $element->transactionable_type=='App\\Models\\TransactionOut' &&
                $element->transactionable->toable_type=='App\\Models\\Warehosue')
                {{ $element->transactionable->toable->name->ar }}
                @elseif($element->transactionable_type=='App\\Models\\TransactionOut' &&
                $element->transactionable->toable_type=='App\\Models\\DistributionPoint')
                {{ $element->transactionable->toable->name }}
                @else

                @endif
            </td>
            <td style="text-align: center">
                @if($element->transactionable_type=='App\\Models\\TransactionIn')
                {{ $element->quantity }}
                @endif
            </td>
            <td style="text-align: center">
                @if($element->transactionable_type=='App\\Models\\TransactionOut')
                {{ $element->quantity }}
                @endif
            </td>
            <td style="text-align: center">{{ $element->closing_balance }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
