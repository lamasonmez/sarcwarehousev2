<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmin4sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin4s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->text('name');
            $table->string('slug')->unique();
            $table->string('p_code');
            $table->softDeletes();
            $table->unsignedBigInteger('admin3_id');
            $table->foreign('admin3_id')->references('id')->on('admin3s');
            $table->double('longitude');
            $table->double('latitude');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin4s');
    }
}
