<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmin5sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin5s', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('name');
            $table->string('slug')->unique();
            $table->string('p_code');
            $table->double('longitude');
            $table->double('latitude');

            $table->unsignedBigInteger('admin4_id');
            $table->foreign('admin4_id')->references('id')->on('admin4s');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin5s');
    }
}
