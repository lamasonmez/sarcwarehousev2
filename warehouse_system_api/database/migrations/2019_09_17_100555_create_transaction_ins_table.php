<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionInsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_ins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->unsignedBigInteger('fromable_id');
            $table->string('fromable_type');
            $table->date('date');
            $table->integer('waybill_no');
            $table->string('slug')->unique();
            $table->softDeletes();
            $table->unsignedBigInteger('warehouses_id');
            $table->foreign('warehouses_id')->references('id')->on('warehouses');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_ins');
    }
}
