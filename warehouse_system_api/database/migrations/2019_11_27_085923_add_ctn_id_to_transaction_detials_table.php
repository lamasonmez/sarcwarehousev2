<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCtnIdToTransactionDetialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_details', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('ctn_id')->nullable();
            $table->foreign('ctn_id')->references('id')->on('ctns');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_details', function (Blueprint $table) {
            //
            $table->dropForeign(['ctn_id']);
            $table->dropColumn('ctn_id');
        });
    }
}
