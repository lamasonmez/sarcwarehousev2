<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToWarehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouses', function (Blueprint $table) {
            //
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('notes')->nullable();
            $table->enum('warehouse_ownership',config('enums.ownerships'))->nullable();
            $table->string('total_area_m2')->nullable();
            $table->string('warehouse_specific_location')->nullable();

            $table->unsignedBigInteger('warehouse_type_id');
            $table->foreign('warehouse_type_id')->references('id')->on('warehouse_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouses', function (Blueprint $table) {
            //
            $table->dropForeign(['warehouse_type_id']);
            $table->dropColumn('warehouse_type_id');
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
            $table->dropColumn('is_active');
            $table->dropColumn('notes');
            $table->dropColumn('warehouse_ownership');
            $table->dropColumn('total_area_m2');
            $table->dropColumn('warehouse_specific_location');
        });
    }
}
