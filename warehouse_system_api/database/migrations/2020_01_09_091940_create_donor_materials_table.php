<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonorMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_materials', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('code');
            $table->string('weight')->nullable();
            $table->string('size')->nullable();
            $table->text('content')->nullable();

            $table->unsignedBigInteger('donor_id');
            $table->foreign('donor_id')->references('id')->on('donors');

            $table->unsignedBigInteger('material_id');
            $table->foreign('material_id')->references('id')->on('materials');

            $table->unsignedBigInteger('unit_id');
            $table->foreign('unit_id')->references('id')->on('units');


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donor_materials');
    }
}
