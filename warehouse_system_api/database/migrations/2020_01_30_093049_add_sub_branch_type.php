<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubBranchType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_branches', function (Blueprint $table) {
            //
            $table->enum('type',config('enums.subBranches_types'))->default('SubBranch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_branches', function (Blueprint $table) {
            //
            $table->dropColumn('type');
        });
    }
}
