<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributesToTransactionsins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_ins', function (Blueprint $table) {
            //
            $table->string('driver_name')->nullable();
            $table->string('vehicle_number')->nullable();
            $table->string('driver_national_id')->nullable();
            $table->string('transportation_company_name')->nullable();
            $table->string('donor_waybill_no')->nullable();
            $table->string('weight')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_ins', function (Blueprint $table) {
            //
            $table->dropColumn('driver_name');
            $table->dropColumn('vehicle_number');
            $table->dropColumn('driver_national_id');
            $table->dropColumn('transportation_company_name');
            $table->dropColumn('donor_waybill_no');
            $table->dropColumn('weight');
        });
    }
}
