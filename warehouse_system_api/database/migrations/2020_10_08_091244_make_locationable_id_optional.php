<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeLocationableIdOptional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('distribution_points', function (Blueprint $table) {
            $table->unsignedBigInteger('locationable_id')->nullable()->change();
            $table->string('locationable_type')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('distribution_points', function (Blueprint $table) {
            $table->unsignedBigInteger('locationable_id')->change();
            $table->string('locationable_type')->change();
        });
    }
}
