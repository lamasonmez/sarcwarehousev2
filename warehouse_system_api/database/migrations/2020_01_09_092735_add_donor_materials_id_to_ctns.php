<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDonorMaterialsIdToCtns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ctns', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('donor_material_id');
            $table->foreign('donor_material_id')->references('id')->on('donor_materials');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ctns', function (Blueprint $table) {
            //
            $table->dropForeign(['donor_material_id']);
            $table->dropColumn('donor_material_id');
        });
    }
}
