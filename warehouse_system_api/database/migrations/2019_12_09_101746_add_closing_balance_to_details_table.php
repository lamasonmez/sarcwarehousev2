<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClosingBalanceToDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_details', function (Blueprint $table) {
            //
            $table->string('reason_for_loss')->nullable();
            $table->string('reason_for_damage')->nullable();
            $table->string('closing_balance')->default('0');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_details', function (Blueprint $table) {
            //
            $table->dropColumn('reason_for_loss');
            $table->dropColumn('reason_for_damage');
            $table->dropColumn('closing_balance');
        });
    }
}
