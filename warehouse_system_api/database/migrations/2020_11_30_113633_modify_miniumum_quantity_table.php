<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyMiniumumQuantityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('minimum_quantities', function (Blueprint $table) {
            $table->dropColumn('material_id');
            $table->dropColumn('donor_id');

            $table->unsignedBigInteger('donor_material_id');
            $table->foreign('donor_material_id')->references('id')->on('donor_materials');

            $table->unsignedBigInteger('warehouse_id');
            $table->foreign('warehouse_id')->references('id')->on('warehouses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('minimum_quantities', function (Blueprint $table) {
            $table->unsignedBigInteger('material_id');
            $table->unsignedBigInteger('donor_id');
            $table->dropForeign(['donor_mateiral_id']);
            $table->dropColumn('donor_mateiral_id');
            $table->dropForeign(['warehouse_id']);
            $table->dropColumn('warehouse_id');
        });
    }
}
