<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameForeignKeyInTransactionInsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_ins', function (Blueprint $table) {
            //
            $table->renameColumn('warehouses_id', 'warehouse_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_ins', function (Blueprint $table) {
            //
            $table->renameColumn('warehouse_id', 'warehouses_id');

        });
    }
}
