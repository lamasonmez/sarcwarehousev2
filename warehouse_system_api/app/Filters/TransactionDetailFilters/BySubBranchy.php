<?php

namespace App\Filters\TransactionDetailFilters;

use Illuminate\Database\Eloquent\Builder;
use App\Filters\Filter;
use App\Helpers\WarehouseHelper;
use App\Models\SubBranch;

class BySubBranch implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        $warehouses_ids = WarehouseHelper::getWarehousesForSubBranch(SubBranch::where('id',$value)->first())->pluck('id');
        return $builder->whereIn('warehouse_id', $warehouses_ids);
    }
}
