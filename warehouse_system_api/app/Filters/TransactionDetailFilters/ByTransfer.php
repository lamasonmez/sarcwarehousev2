<?php

namespace App\Filters\TransactionDetailFilters;

use Illuminate\Database\Eloquent\Builder;
use App\Filters\Filter;
use App\Helpers\WarehouseHelper;
use App\Models\Branch;
use App\Models\DonorMaterial;

class ByTransfer implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        if($value=="all"){
          return  $builder->whereNotNull('transaction_type_id');
        }
        return $builder->whereIn('transaction_type_id', $value);
    }
}
