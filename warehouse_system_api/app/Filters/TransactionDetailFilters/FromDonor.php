<?php

namespace App\Filters\TransactionDetailFilters;

use Illuminate\Database\Eloquent\Builder;
use App\Filters\Filter;
use App\Models\DonorMaterial;

class FromDonor implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        $donor_materials_ids = DonorMaterial::where('donor_id',$value)->pluck('id');
        return $builder->whereIn('donor_material_id', $donor_materials_ids);
    }
}
