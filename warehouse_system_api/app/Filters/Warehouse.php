<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class Warehouse implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->where('warehouse_id', $value);
    }
}
