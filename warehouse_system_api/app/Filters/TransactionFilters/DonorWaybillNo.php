<?php

namespace App\Filters\TransactionFilters;

use Illuminate\Database\Eloquent\Builder;
use App\Filters\Filter;

class DonorWaybillNo implements Filter
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder->where('donor_waybill_no', $value);
    }
}
