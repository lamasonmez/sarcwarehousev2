<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Admin4 extends Model
{
    use SoftDeletes,Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en',
                'onUpdate'=> true,
            ]
        ];
    }


    protected $fillable=[
        'name','p_code','Longitude','Latitude'
    ];
    protected $dates=['deleted_at'];



    public function setNameAttribute($value) {
        $this->attributes['name'] = json_encode($value,JSON_UNESCAPED_UNICODE);
     }
    public function getNameAttribute($value){
        return json_decode($value);
    }

 public function Admin3()
 {
     return $this->belongsTo('App\Models\Admin3');
 }
 public function Admin5s()
 {
     return $this->hasMany('App\Models\Admin5');
 }

}
