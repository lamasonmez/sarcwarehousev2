<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;


class Material extends BaseModel
{
    use SoftDeletes,Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en',
                'onUpdate'=> true,
            ]
        ];
    }
    protected $fillable=[
        'name','code','sector_id' ];

    protected $dates=['deleted_at'];

    protected $with=['sector'];
    public function setNameAttribute($value) {
        $this->attributes['name'] = json_encode($value,JSON_UNESCAPED_UNICODE);
    }
    public function getNameAttribute($value){
        return json_decode($value);
    }

    public function sector()
    {
        return $this->belongsTo('App\Models\Sector');
    }
    public function donors(){
        return $this->hasMany('App\Models\DonorMaterial');
    }

}
