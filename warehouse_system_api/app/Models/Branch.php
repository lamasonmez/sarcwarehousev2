<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Branch extends BaseModel
{
    //
    use SoftDeletes,Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en',
                'onUpdate'=> true,
            ]
        ];
    }


    protected $fillable=[
        'name','code'
    ];
    protected $dates=['deleted_at'];


    public function setNameAttribute($value) {
        $this->attributes['name'] = json_encode($value,JSON_UNESCAPED_UNICODE);
     }
    public function getNameAttribute($value){
        return json_decode($value);
    }

    public function subBranches()
    {
        return $this->hasMany('App\Models\SubBranch');
    }
    public function users()
    {
        return $this->morphMany('App\User', 'branchable');
    }

    public function warehouses()
    {
        return $this->morphMany('App\Models\Warehouse', 'branchable');
    }

    public function all_warehouses()
    {
        $warehouses =$this->warehouses();
        $subBranchs = $this->subBranches();

        foreach($subBranchs as $item){
            foreach($item->warehouses as $warehouse){
                array_push($warehouses,$warehouse);
            }
        }
        return $warehouses;
        //return $this->hasManyThrough("App\Models\Warehouse","App\Models\SubBranch","branch_id","branchable_id");

    }

}
