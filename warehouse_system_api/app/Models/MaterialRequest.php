<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaterialRequest extends BaseModel
{
    protected $fillable=["donor_material_id","status","user_id","rejected_reason"];

    protected $with = ['user','donor_material'];

    public function donor_material(){
        return $this->belongsTo(DonorMaterial::class);
    }
    public function user(){
      return  $this->belongsTo(User::class);
    }
}
