<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


class TransactionOut extends BaseModel implements HasMedia
{
    use SoftDeletes,InteractsWithMedia;

    protected $fillable=[
        'date',
        'waybill_no',
        'toable_type',
        'toable_id',
        'warehouse_id',
        'status',
        'weight',
        'donor_id'
    ];
    protected $dates=['deleted_at'];
    protected $appends = ['image','src'];
    protected $with = ['toable','warehouse'];


    public function registerMediaCollections():void
    {
        $this->addMediaCollection('waybills_out')
            ->singleFile();
    }
    public function getImageAttribute()
    {
        return $this->getFirstMedia('waybills_out');
    }
    public function getSrcAttribute()
    {
        if($this->getFirstMedia('waybills_out')!=null)
        return $this->getFirstMedia('waybills_out')->getFullUrl();
    }

    public function toable()
    {
        return $this->morphTo();
    }
    public function from(){
        return $this->belongsTo('App\Models\Warehouse','warehouse_id');
    }
    public function details()
    {
        return $this->morphMany('App\Models\TransactionDetail', 'transactionable');
    }
    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }

}
