<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Warehouse extends BaseModel
{
    use SoftDeletes, Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name.ar',
                'onUpdate' => true,
            ]
        ];
    }

    protected $fillable = [
        'name', 'longitude',
        'latitude', 'is_active', 'notes'
        , 'warehouse_ownership',
        'total_area_m2', 'warehouse_specific_location',
        'keeper_name', 'keeper_phone'];

    protected $dates = ['deleted_at'];
    protected $with = ['branchable'];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public function getNameAttribute($value)
    {
        return json_decode($value);
    }

    public function branchable()
    {
        return $this->morphTo();
    }

    public function transactionsIn()
    {
        return $this->morphMany('App\Models\TransactionIn', 'fromable');
    }

    public function transactionsOut()
    {
        return $this->morphMany('App\Models\TransactionOut', 'toable');
    }

    public function warehouse_type()
    {
        return $this->belongsTo('App\Models\WarehouseType');
    }

    public function details()
    {
        return $this->hasMany('App\Models\TransactionDetails', 'warehouse_id');
    }

    public function users()
    {
        return $this->morphMany('App\User', 'rolable');
    }
}

