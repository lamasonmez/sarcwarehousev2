<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;


class Donor extends BaseModel
{
    use SoftDeletes, Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en',
                'onUpdate' => true,
            ]
        ];
    }

    protected $fillable = [
        'name', 'code'];
    protected $dates = ['deleted_at'];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public function getNameAttribute($value)
    {
        return json_decode($value);
    }

    public function transactionsIn()
    {
        return $this->morphMany('App\Models\TransactionIn', 'fromable_id');
    }

    public function materials()
    {
        return $this->hasMany('App\Models\DonorMaterial');
    }


    public function users()
    {
        return $this->morphMany('App\User', 'rolable');
    }

}
