<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class BaseModel extends Model
{
    //
    use LogsActivity;
    protected static $logFillable = true;
    protected static $submitEmptyLogs = false;
}
