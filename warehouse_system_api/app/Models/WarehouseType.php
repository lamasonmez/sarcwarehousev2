<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class WarehouseType extends BaseModel
{
    //
    use SoftDeletes,Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en',
                'onUpdate'=> true,
            ]
        ];
    }
    protected $fillable=['name'];
        protected $dates=['deleted_at'];

        public function setNameAttribute($value) {
            $this->attributes['name'] = json_encode($value,JSON_UNESCAPED_UNICODE);
         }
        public function getNameAttribute($value){
            return json_decode($value);
        }

        public function warehouses()
        { 
            return $this->hasMany('App\Models\Warehouse');
        }
}
