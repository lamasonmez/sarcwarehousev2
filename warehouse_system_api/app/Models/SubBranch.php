<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class SubBranch extends BaseModel
{
    //
    use SoftDeletes,Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en',
                'onUpdate'=> true,
            ]
        ];
    }
    protected $fillable=[
        'name','code','type','branch_id'
    ];
    protected $dates=['deleted_at'];

    protected $with=['branch'];

    public function setNameAttribute($value) {
        $this->attributes['name'] = json_encode($value,JSON_UNESCAPED_UNICODE);
     }
    public function getNameAttribute($value){
        return json_decode($value);
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }
    public function users()
    {
        return $this->morphMany('App\User', 'branchable');
    }
    public function warehouses()
    {
        return $this->morphMany('App\Models\Warehouse', 'branchable');
    }
}
