<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class DistributionPoint extends BaseModel
{
    //
    use SoftDeletes;
 
    protected $fillable=[
        'name',
        ];
        protected $dates=['deleted_at'];

        public function locationable()
        {
            return $this->morphTo();
        }

}
