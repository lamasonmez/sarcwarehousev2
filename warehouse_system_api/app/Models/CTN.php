<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CTN extends BaseModel
{
    //
    use SoftDeletes;
    protected $table="ctns";

    protected $fillable=["name"];
    protected $dates=["delete_at"];

    public function transaction_details(){
     return   $this->hasMany("App\Models\TransactionDetail");
    }

    public function donor_material(){
        return $this->belongsTo('App\Models\DonorMaterial');
    }
    

}
