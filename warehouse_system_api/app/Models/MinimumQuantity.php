<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Material;
use App\Models\Donor;

class MinimumQuantity extends BaseModel
{
    //
    use SoftDeletes;
    protected $fillable= [
        'donor_material_id','minimum_quantity','warehouse_id'
    ];

    protected $dates=['deleted_at'];
    protected $with = ['warehouse','donor_material'];

    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }

    public function donor_material(){
        return $this->belongsTo(DonorMaterial::class);
    }


}
