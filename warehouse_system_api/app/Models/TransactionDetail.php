<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\CTN;
use App\Helpers\TransactionHelper;
use Illuminate\Support\Facades\DB;


class TransactionDetail extends BaseModel
{
    //
    use SoftDeletes;
    protected $fillable=[
        'quantity',
        'notes',
        'is_convoy',
        'loss',
        'damage',
        'expiration_date',
        'reason_for_loss',
        'reason_for_damage',
        'transactionable_type',
        'transaction_type_id',
        'donor_material_id',
        'warehouse_id',
        'transactionable_id',
        'ctn_id',
        'date'
    ];
    protected $dates=['deleted_at'];

    protected $appends=['opening_balance','closing_balance','in_count','out_count'];
    protected $with = ['transactionable','material_donor','ctn','warehouse','transaction_type'];


    public function transaction_type()
    {
        return $this->belongsTo('App\Models\TransactionType');
    }

    // for transaction In table or Transaction out table
    public function transactionable()
    {
        return $this->morphTo()->latest('date');
    }
    public function ctn(){
        return $this->belongsTo(CTN::class,'ctn_id','id');
    }
    public function material_donor(){
        return $this->belongsTo("App\Models\DonorMaterial",'donor_material_id');
    }
    public function warehouse(){
        return $this->belongsTo("App\Models\Warehouse",'warehouse_id');
    }


    public function getOpeningBalanceAttribute(){
        $conditions = [
            ['donor_material_id','=',$this->donor_material_id],
            ['warehouse_id','=',$this->warehouse_id],
            ['id','!=',(int)$this->id],
        ];

        $Inconditions = array_merge($conditions,array(['transactionable_type','=','App\\Models\\TransactionIn']));
        $Outconditions = array_merge($conditions,array(['transactionable_type','=','App\\Models\\TransactionOut']));

        $sum_of_previous_ins_quantity =self::query()->where($Inconditions)->whereDate('date','<',$this->date)->sum('quantity') ;
        $sum_of_previous_ins_loss =self::query()->where($Inconditions)->whereDate('date','<',$this->date)->sum('loss') ;
        $sum_of_previous_ins_damage =self::query()->where($Inconditions)->whereDate('date','<',$this->date)->sum('damage') ;
        $sum_of_previous_ins = $sum_of_previous_ins_quantity - ($sum_of_previous_ins_damage+$sum_of_previous_ins_loss);

        $sum_of_previous_outs =self::query()->where($Outconditions)->whereDate('date','<',$this->date)->sum('quantity') ;


        $open_balance =  $sum_of_previous_ins - $sum_of_previous_outs;

        $sum_of_previous_same_date_ins = 0;
        $sum_of_previous_same_date_outs = 0;
        $previous_same_date_ins =self::query()->where($Inconditions)->whereDate('date','=',$this->date)->get() ;
        $previous_same_date_outs =self::query()->where($Outconditions)->whereDate('date','=',$this->date)->get() ;

        foreach($previous_same_date_ins as $in_item){
            if($in_item->id <$this->id){
                $sum_of_previous_same_date_ins += $in_item->quantity - ($in_item->damage + $in_item->loss);
            }
        }

        foreach($previous_same_date_outs as $in_item){
            if($in_item->id <$this->id){
                $sum_of_previous_same_date_outs += $in_item->quantity ;
            }
        }
        $open_balance_same_date = $sum_of_previous_same_date_ins - $sum_of_previous_same_date_outs;
        $open_balance +=$open_balance_same_date;
        // foreach($sameDateTransactions as $item){
        //     $open_balance =+$item->quantity;
        // }
        return  $open_balance;


    }
    public function getClosingBalanceAttribute(){
        return $this->transactionable instanceof TransactionIn ? $this->opening_balance + $this->quantity :$this->opening_balance - $this->quantity ;
    }
    public function getInCountAttribute(){
        $query = DB::table('transaction_details')
            ->select(DB::raw("COUNT(DISTINCT(transactionable_id)) as in_count"))
            ->where('transactionable_type','App\\Models\\TransactionIn')
            ->whereNull('transaction_details.deleted_at')
            ->where('transaction_details.donor_material_id',$this->donor_material_id)
            ->where('warehouse_id',$this->warehouse_id)->first();
        return $query->in_count;
        }
    public function getOutCountAttribute(){
        $query = DB::table('transaction_details')
            ->select(DB::raw("COUNT(DISTINCT(transactionable_id)) as out_count"))
            ->where('transactionable_type','App\\Models\\TransactionOut')
            ->whereNull('transaction_details.deleted_at')
            ->where('transaction_details.donor_material_id',$this->donor_material_id)
            ->where('warehouse_id',$this->warehouse_id)->first();
        return $query->out_count;
    }

}
