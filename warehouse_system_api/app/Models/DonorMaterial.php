<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class DonorMaterial extends BaseModel
{
    //
    use SoftDeletes;


    protected $fillable=["weight",'size','content','code','donor_id','material_id','unit_id'];
    protected $dates=["delete_at"];
    protected $with=["donor","material","unit",'ctns'];

    public function donor(){
        return $this->belongsTo('App\Models\Donor','donor_id');
    }
    public function material(){
        return $this->belongsTo('App\Models\Material');
    }
    public function unit(){
        return $this->belongsTo('App\Models\Unit');
    }

    public function ctns(){
        return $this->hasMany('App\Models\CTN');
    }
    public function transactions(){
        return $this->hasMany('App\Models\TransactionDetail');
    }
    public function requests(){
        return $this->hasMany(MaterialRequest::class);
    }

}
