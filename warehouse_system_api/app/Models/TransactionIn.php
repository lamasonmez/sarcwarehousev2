<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class TransactionIn extends BaseModel implements HasMedia
{
    use SoftDeletes,InteractsWithMedia;

    protected $fillable=[
        'date',
        'waybill_no',
        'fromable_type',
        'fromable_id',
        'warehouse_id',
        'driver_name',
        'vehicle_number',
        'driver_national_id',
        'transportation_company_name',
        'donor_waybill_no',
        'weight',
        'status'
    ];
    protected $dates=['deleted_at'];
    protected $appends = ['image','src'];
    protected $with = ['fromable','to','transfer'];

    public function registerMediaCollections():void
    {
        $this->addMediaCollection('waybills_in')
            ->singleFile();
    }
    public function getImageAttribute()
    {
        return $this->getFirstMedia('waybills_in');

    }
    public function getSrcAttribute()
    {
        if($this->getFirstMedia('waybills_in')!=null)
        return $this->getFirstMedia('waybills_in')->getFullUrl();
    }

    public function fromable()
    {
        return $this->morphTo();
    }

    public function to()
    {
        return $this->belongsTo('App\Models\Warehouse','warehouse_id');
    }
    public function details()
    {
        return $this->morphMany('App\Models\TransactionDetail', 'transactionable');
    }
    public function transfer(){
        return $this->hasOne(Transfer::class,'transaction_in_id');
    }
    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }
}
