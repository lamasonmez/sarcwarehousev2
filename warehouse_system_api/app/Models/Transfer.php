<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Models\TransactionIn;
use App\Models\TransactionOut;
use App\Models\TransactionType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transfer extends BaseModel
{

    protected $fillable=['transaction_in_id','transaction_out_id'];

    public function transaction_in(){
        return $this->belongsTo(TransactionIn::class,'transaction_in_id');
    }

    public function transaction_out(){
        return $this->belongsTo(TransactionOut::class,'transaction_out_id');
    }


}
