<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Admin0 extends Model
{
    //
    use SoftDeletes,Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en',
                'onUpdate'=> true,
            ]
        ];
    }


    protected $fillable=[
        'name','p_code'
    ];
    protected $dates=['deleted_at'];



    public function setNameAttribute($value) {
        $this->attributes['name'] = json_encode($value,JSON_UNESCAPED_UNICODE);
     }
    public function getNameAttribute($value){
        return json_decode($value);
    }
    public function Admin1s()
    {
        return $this->hasMany('App\Models\Admin1');
    }
}
