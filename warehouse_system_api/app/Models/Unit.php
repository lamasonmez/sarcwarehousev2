<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Unit extends BaseModel
{
    use SoftDeletes,Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en',
                'onUpdate'=> true,
            ]
        ];
    }
    protected $fillable=[
        'name','code'    ];
        protected $dates=['deleted_at'];

        public function setNameAttribute($value) {
            $this->attributes['name'] = json_encode($value,JSON_UNESCAPED_UNICODE);
         }
        public function getNameAttribute($value){
            return json_decode($value);
        }
        public function transactions(){
            return $this->hasMany('App\Models\TransactionDetails');
        }    

}
