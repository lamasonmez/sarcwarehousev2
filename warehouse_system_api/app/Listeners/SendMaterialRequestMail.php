<?php

namespace App\Listeners;

use App\Events\MaterialRequestCreated;
use App\Mail\MaterialRequestMail;
use App\Models\MaterialRequest;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
class SendMaterialRequestMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MaterialRequestCreated  $event
     * @return void
     */
    public function handle(MaterialRequestCreated $event)
    {

        //fetch all admins
        $admins = User::where('is_approved',true)->whereHas('roles',function($query){
            $query->where('name','admin');
        })->get();
        foreach($admins as $admin){
              Mail::to($admin->email)->send(new MaterialRequestMail($event->material_request,$admin,$event->material_request->user));
        }

    }
}
