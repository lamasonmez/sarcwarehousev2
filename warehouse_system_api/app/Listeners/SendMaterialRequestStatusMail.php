<?php

namespace App\Listeners;

use App\Events\MaterialRequestStatusChanged;
use App\Mail\MaterialRequestStatusMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
class SendMaterialRequestStatusMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MaterialRequestStatusChanged  $event
     * @return void
     */
    public function handle(MaterialRequestStatusChanged $event)
    {
        Mail::to($event->material_request->user->email)->send(new MaterialRequestStatusMail($event->material_request));

    }
}
