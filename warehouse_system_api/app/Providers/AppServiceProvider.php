<?php

namespace App\Providers;

use App\Models\MaterialRequest;
use Illuminate\Support\ServiceProvider;

use App\Services\JWT\IJWTService;
use App\Services\JWT\JWTService;

use App\Models\TransactionIn;
use App\Models\TransactionOut;
use App\Observers\MaterialRequestObserver;
use App\Observers\TransactionInObserver;
use App\Observers\TransactionOutObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            IJWTService::class,
            JWTService::class
        );
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        TransactionIn::observe(TransactionInObserver::class);
        TransactionOut::observe(TransactionOutObserver::class);
        MaterialRequest::observe(MaterialRequestObserver::class);
    }
}
