<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\UserRepository;
use App\User;
use App\Repositories\DonorMaterialRepository;
use App\Models\DonorMaterial;

use App\Repositories\TransactionDetailRepository;
use App\Models\TransactionDetail;

use App\Repositories\TransactionInRepository;
use App\Models\TransactionIn;

use App\Repositories\TransactionOutRepository;
use App\Models\TransactionOut;

use App\Repositories\MinimumQuantityRepository;
use App\Models\MinimumQuantity;

use App\Repositories\BranchRepository;
use App\Models\Branch;

use App\Repositories\SubBranchRepository;
use App\Models\SubBranch;

use App\Repositories\WarehouseRepository;
use App\Models\Warehouse;

use App\Repositories\TransactionTypeRepository;
use App\Models\TransactionType;

use App\Repositories\MaterialRepository;
use App\Models\Material;

use App\Repositories\MaterialRequestRepository;
use App\Models\MaterialRequest;

use App\Repositories\UnitRepository;
use App\Models\Unit;

use App\Repositories\SectorRepository;
use App\Models\Sector;

use App\Repositories\DonorRepository;
use App\Models\Donor;

//namespaces


















class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\Repositories\UserRepository', function (Application $app) {
            return new UserRepository(
                $app->make(User::class)
            );
        });


        $this->app->bind('App\Repositories\DonorMaterialRepository', function (Application $app) {
            return new DonorMaterialRepository(
                $app->make(DonorMaterial::class)
            );
        });

            $this->app->bind('App\Repositories\TransactionDetailRepository', function (Application $app) {
            return new TransactionDetailRepository(
                $app->make(TransactionDetail::class)
            );
    });

        $this->app->bind('App\Repositories\TransactionInRepository', function (Application $app) {
            return new TransactionInRepository(
                $app->make(TransactionIn::class)
            );
    });

        $this->app->bind('App\Repositories\TransactionOutRepository', function (Application $app) {
            return new TransactionOutRepository(
                $app->make(TransactionOut::class)
            );
    });

        $this->app->bind('App\Repositories\MinimumQuantityRepository', function (Application $app) {
            return new MinimumQuantityRepository(
                $app->make(MinimumQuantity::class)
            );
    });

        $this->app->bind('App\Repositories\BranchRepository', function (Application $app) {
            return new BranchRepository(
                $app->make(Branch::class)
            );
    });

        $this->app->bind('App\Repositories\SubBranchRepository', function (Application $app) {
            return new SubBranchRepository(
                $app->make(SubBranch::class)
            );
    });

        $this->app->bind('App\Repositories\WarehouseRepository', function (Application $app) {
            return new WarehouseRepository(
                $app->make(Warehouse::class)
            );
    });

        $this->app->bind('App\Repositories\TransactionTypeRepository', function (Application $app) {
            return new TransactionTypeRepository(
                $app->make(TransactionType::class)
            );
    });

        $this->app->bind('App\Repositories\MaterialRepository', function (Application $app) {
            return new MaterialRepository(
                $app->make(Material::class)
            );
    });

        $this->app->bind('App\Repositories\MaterialRequestRepository', function (Application $app) {
            return new MaterialRequestRepository(
                $app->make(MaterialRequest::class)
            );
    });

        $this->app->bind('App\Repositories\UnitRepository', function (Application $app) {
            return new UnitRepository(
                $app->make(Unit::class)
            );
    });

        $this->app->bind('App\Repositories\SectorRepository', function (Application $app) {
            return new SectorRepository(
                $app->make(Sector::class)
            );
    });

        $this->app->bind('App\Repositories\DonorRepository', function (Application $app) {
            return new DonorRepository(
                $app->make(Donor::class)
            );
    });

    //add bindings















    }
}
