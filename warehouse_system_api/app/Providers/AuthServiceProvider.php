<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Branch' => 'App\Policies\BranchPolicy',
        'App\Models\SubBranch' => 'App\Policies\SubBranchPolicy',
        'App\Models\Donor' => 'App\Policies\DonorPolicy',
        'App\Models\Sector' => 'App\Policies\SectorPolicy',
        'App\Models\Material' => 'App\Policies\MaterialPolicy',
        'App\Models\Unit' => 'App\Policies\UnitPolicy',
        'App\Models\MinimumQuantity' => 'App\Policies\MinimumQuantityPolicy',
        'App\Models\TransactionType' => 'App\Policies\TransactionTypePolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'Spatie\Permission\Models\Role'=>'App\Policies\RolePolicy',
        'Spatie\Permission\Models\Permission'=>'App\Policies\PermissionPolicy',
        'App\Models\Admin0' => 'App\Policies\GeoPolicy',
        'App\Models\Admin1' => 'App\Policies\GeoPolicy',
        'App\Models\Admin2' => 'App\Policies\GeoPolicy',
        'App\Models\Admin3' => 'App\Policies\GeoPolicy',
        'App\Models\Admin4' => 'App\Policies\GeoPolicy',
        'App\Models\Admin5' => 'App\Policies\GeoPolicy',
        'App\Models\Warehouse' => 'App\Policies\WarehousePolicy',
        'App\Models\CTN' => 'App\Policies\CTNPolicy',
        'App\Models\WarehouseType' => 'App\Policies\WarehouseTypePolicy',

       // Branch::class => BranchPolicy::class,
        //SubBranch::class => SubBranchPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(function ($router) {
            $router->forAccessTokens();
        });
        Passport::tokensExpireIn(now()->addDays(config('auth.passport.token.expire')));
        Passport::refreshTokensExpireIn(now()->addDays(config('auth.passport.token.refresh')));
        Passport::tokensExpireIn(now()->addDays(config('auth.passport.token.expire')));
        Passport::refreshTokensExpireIn(now()->addDays(config('auth.passport.token.expire')));

    }
}
