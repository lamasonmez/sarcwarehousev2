<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\DonorMaterial\IDonorMaterialService;
use App\Services\DonorMaterial\DonorMaterialService;


use App\Services\TransactionDetail\ITransactionDetailService;
use App\Services\TransactionDetail\TransactionDetailService;


use App\Services\TransactionIn\ITransactionInService;
use App\Services\TransactionIn\TransactionInService;


use App\Services\TransactionOut\ITransactionOutService;
use App\Services\TransactionOut\TransactionOutService;


use App\Services\MinimumQuantity\IMinimumQuantityService;
use App\Services\MinimumQuantity\MinimumQuantityService;


use App\Services\Branch\IBranchService;
use App\Services\Branch\BranchService;


use App\Services\SubBranch\ISubBranchService;
use App\Services\SubBranch\SubBranchService;


use App\Services\Warehouse\IWarehouseService;
use App\Services\Warehouse\WarehouseService;


use App\Services\TransactionType\ITransactionTypeService;
use App\Services\TransactionType\TransactionTypeService;


use App\Services\Material\IMaterialService;
use App\Services\Material\MaterialService;


use App\Services\MaterialRequest\IMaterialRequestService;
use App\Services\MaterialRequest\MaterialRequestService;


use App\Services\Unit\IUnitService;
use App\Services\Unit\UnitService;


use App\Services\Sector\ISectorService;
use App\Services\Sector\SectorService;


use App\Services\Donor\IDonorService;
use App\Services\Donor\DonorService;
use App\Services\User\IUserService;
use App\Services\User\UserService;

//namespaces















class ServiceLayerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            IDonorMaterialService::class,
            DonorMaterialService::class,
        );

    $this->app->bind(
    ITransactionDetailService::class,
    TransactionDetailService::class
);

$this->app->bind(
    ITransactionInService::class,
    TransactionInService::class
);

$this->app->bind(
    ITransactionOutService::class,
    TransactionOutService::class
);

$this->app->bind(
    IMinimumQuantityService::class,
    MinimumQuantityService::class
);

$this->app->bind(
    IBranchService::class,
    BranchService::class
);

$this->app->bind(
    ISubBranchService::class,
    SubBranchService::class
);

$this->app->bind(
    IWarehouseService::class,
    WarehouseService::class
);

$this->app->bind(
    ITransactionTypeService::class,
    TransactionTypeService::class
);

$this->app->bind(
    IMaterialService::class,
    MaterialService::class
);

$this->app->bind(
    IMaterialRequestService::class,
    MaterialRequestService::class
);

$this->app->bind(
    IUnitService::class,
    UnitService::class
);

$this->app->bind(
    ISectorService::class,
    SectorService::class
);

$this->app->bind(
    IDonorService::class,
    DonorService::class
);

$this->app->bind(
    IUserService::class,
    UserService::class
);
//add bindings















    }
}
