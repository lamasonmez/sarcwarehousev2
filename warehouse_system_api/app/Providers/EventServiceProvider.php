<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\MaterialRequestCreated;
use App\Events\MaterialRequestApproved;
use App\Events\MaterialRequestRejected;
use App\Events\MaterialRequestStatusChanged;
use App\Listeners\SendMaterialRequestMail;
use App\Listeners\SendMaterialRequestStatusMail;
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        MaterialRequestCreated::class =>[
            SendMaterialRequestMail::class
        ],
        MaterialRequestStatusChanged::class=>[
            SendMaterialRequestStatusMail::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
