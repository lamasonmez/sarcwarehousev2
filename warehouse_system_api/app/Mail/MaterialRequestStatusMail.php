<?php

namespace App\Mail;

use App\Models\MaterialRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MaterialRequestStatusMail extends Mailable
{
    use Queueable, SerializesModels;
    public $material_request;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MaterialRequest $material_request)
    {
        $this->material_request = $material_request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Material Request  Update | SWIS ')
        ->markdown('mails.material_request_response_status')
        ->with('material_request',$this->material_request);
    }
}
