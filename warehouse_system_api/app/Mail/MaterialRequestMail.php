<?php

namespace App\Mail;

use App\Models\MaterialRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
class MaterialRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $material_request,$admin,$user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MaterialRequest $material_request,User $admin,User $user)
    {
        $this->material_request = $material_request;
        $this->user = $user;
        $this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('New Material  Request | SWIS ')
                    ->markdown('mails.material_request')
                    ->with('user',$this->user)
                    ->with('admin',$this->admin)
                    ->with('material_request',$this->material_request);
    }
}
