<?php

namespace App\Observers;

use App\Models\TransactionOut;
use App\Models\DonorMaterial;
use App\Models\Donor;
use App\Models\Transfer;
use App\Models\TransactionDetail;
use App\Models\TransactionIn;

class TransactionOutObserver
{
    /**
     * Handle the transaction in "created" event.
     *
     * @param  \App\Models\TransactionOut  $TransactionOut
     * @return void
     */
    public function created(TransactionOut $TransactionOut)
    {
         if($TransactionOut->toable_type=="App\Models\Warehouse" ){
            //create new transaction in with pending status
           $transaction =  TransactionIn::create([
                'waybill_no'=>'pending_number',
                "date"=>$TransactionOut->created_at,
                "fromable_id"=>$TransactionOut->warehouse_id,
                "fromable_type"=>'App\Models\Warehouse',
                'warehouse_id'=>$TransactionOut->toable_id,
                'status'=>"pending"

            ]);
            Transfer::create([
                'transaction_out_id'=>$TransactionOut->id,
                'transaction_in_id'=>$transaction->id
            ]);
            // $details = TransactionDetail::where('transactionable_type','App\Models\TransactionOut')->where('transactionable_id',$TransactionOut->id)->get();
            // foreach($details as $item){
            //     TransactionDetail::create([
            //         'quantity'=>$item->quantity,
            //         'notes'=>'',
            //         'is_convoy'=>$item->is_convoy,
            //         'loss'=>"",
            //         'damage'=>"",
            //         'expiration_date'=>$item->expiration_date,
            //         'reason_for_loss'=>"",
            //         'reason_for_damage'=>"",
            //         'transactionable_type'=>'App\Models\TransactionIn',
            //         'transaction_type_id'=>2,
            //         'donor_material_id'=>$item->donor_material_id,
            //         'warehouse_id'=>$transaction->warehouse_id,
            //         'transactionable_id'=>$transaction->id,
            //         'ctn_id'=>$item->ctn_id,
            //         'date'=>$item->date
            //     ]);
            // }

         }
    }

    /**
     * Handle the transaction in "updated" event.
     *
     * @param  \App\Models\TransactionOut  $TransactionOut
     * @return void
     */
    public function updated(TransactionOut $TransactionOut)
    {
        $date = $TransactionOut->date;
        foreach($TransactionOut->details as $element){
            $element->date =$date ;
            $element->save();
        }
    }

   /**
     * Handle the transaction in "deleted" event.
     *
     * @param  \App\Models\TransactionOut  $transactionOut
     * @return void
     */
    public function deleted(TransactionOut $transactionOut)
    {
        if($transactionOut->image){
            $transactionOut->image->delete();
        }
        foreach($transactionOut->details as $item){
            $item->delete();
        }
    }

    /**
     * Handle the transaction in "restored" event.
     *
     * @param  \App\Models\TransactionOut  $TransactionOut
     * @return void
     */
    public function restored(TransactionOut $TransactionOut)
    {
        //
    }

    /**
     * Handle the transaction in "force deleted" event.
     *
     * @param  \App\Models\TransactionOut  $TransactionOut
     * @return void
     */
    public function forceDeleted(TransactionOut $TransactionOut)
    {
        //
    }
}
