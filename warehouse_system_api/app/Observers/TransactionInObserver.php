<?php

namespace App\Observers;

use App\Models\TransactionIn;
use App\Models\DonorMaterial;
use App\Models\Donor;

class TransactionInObserver
{
    /**
     * Handle the transaction in "created" event.
     *
     * @param  \App\Models\TransactionIn  $transactionIn
     * @return void
     */
    public function created(TransactionIn $transactionIn)
    {
        //
    }

    /**
     * Handle the transaction in "updated" event.
     *
     * @param  \App\Models\TransactionIn  $transactionIn
     * @return void
     */
    public function updated(TransactionIn $transactionIn)
    {
        foreach($transactionIn->details as $element){
            if($transactionIn->fromable instanceof Donor){
                $donor_id = $transactionIn->fromable_id;
                if($element->material_donor->donor_id != $donor_id){
                    //make sure that the added donor has the same item
                        $material_donor = DonorMaterial::where('donor_id',$donor_id)
                                                        ->where('material_id',$element->material_donor->material_id)->first();
                        if($material_donor!=null){
                            $element->update(['donor_material_id'=>$material_donor->id]);
                        }
                        else{
                            $element->delete();
                        }
                }

            }
            $element->date = $transactionIn->date;
            $element->save();
        }
    }

    /**
     * Handle the transaction in "deleted" event.
     *
     * @param  \App\Models\TransactionIn  $transactionIn
     * @return void
     */
    public function deleted(TransactionIn $transactionIn)
    {
        if($transactionIn->image){
            $transactionIn->image->delete();
        }
        foreach($transactionIn->details as $item){
            $item->delete();
        }
    }

    /**
     * Handle the transaction in "restored" event.
     *
     * @param  \App\Models\TransactionIn  $transactionIn
     * @return void
     */
    public function restored(TransactionIn $transactionIn)
    {
        //
    }

    /**
     * Handle the transaction in "force deleted" event.
     *
     * @param  \App\Models\TransactionIn  $transactionIn
     * @return void
     */
    public function forceDeleted(TransactionIn $transactionIn)
    {
        //
    }
}
