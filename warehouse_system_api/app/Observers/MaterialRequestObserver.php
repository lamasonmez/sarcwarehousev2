<?php

namespace App\Observers;

use App\Constants\MaterialStatus;
use App\Events\MaterialRequestStatusChanged;
use App\Events\MaterialRequestCreated;
use App\Models\MaterialRequest;

class MaterialRequestObserver
{
    /**
     * Handle the MaterialRequest "created" event.
     *
     * @param  \App\Models\MaterialRequest  $materialRequest
     * @return void
     */
    public function created(MaterialRequest $materialRequest)
    {
        //
       // event(new MaterialRequestCreated($materialRequest));
    }

    /**
     * Handle the MaterialRequest "updated" event.
     *
     * @param  \App\Models\MaterialRequest  $materialRequest
     * @return void
     */
    public function updated(MaterialRequest $materialRequest)
    {
        //
        if($materialRequest->status==MaterialStatus::APPROVED || $materialRequest->status==MaterialStatus::REJECTED){
           // event(new MaterialRequestStatusChanged($materialRequest));
        }

    }

    /**
     * Handle the MaterialRequest "deleted" event.
     *
     * @param  \App\Models\MaterialRequest  $materialRequest
     * @return void
     */
    public function deleted(MaterialRequest $materialRequest)
    {
        //
    }

    /**
     * Handle the MaterialRequest "restored" event.
     *
     * @param  \App\Models\MaterialRequest  $materialRequest
     * @return void
     */
    public function restored(MaterialRequest $materialRequest)
    {
        //
    }

    /**
     * Handle the MaterialRequest "force deleted" event.
     *
     * @param  \App\Models\MaterialRequest  $materialRequest
     * @return void
     */
    public function forceDeleted(MaterialRequest $materialRequest)
    {
        //
    }
}
