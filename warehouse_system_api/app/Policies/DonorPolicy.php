<?php

namespace App\Policies;

use App\User;
use App\Models\Donor;
use Illuminate\Auth\Access\HandlesAuthorization;

class DonorPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any donors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if($user->hasDirectPermission('viewAny_donors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the donor.
     *
     * @param  \App\User  $user
     * @param  \App\Donor  $donor
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_donors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create donors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_donors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the donor.
     *
     * @param  \App\User  $user
     * @param  \App\Donor  $donor
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('view_donors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the donor.
     *
     * @param  \App\User  $user
     * @param  \App\Donor  $donor
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_donors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the donor.
     *
     * @param  \App\User  $user
     * @param  \App\Donor  $donor
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_donors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the donor.
     *
     * @param  \App\User  $user
     * @param  \App\Donor  $donor
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if($user->hasDirectPermission('forceDelete_donors')){
            return true;
        }
        return false;
    }
}
