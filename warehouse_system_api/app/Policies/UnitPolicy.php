<?php

namespace App\Policies;

use App\User;
use App\Models\Unit;
use Illuminate\Auth\Access\HandlesAuthorization;

class UnitPolicy
{
    use HandlesAuthorization;
     /**
     * Determine whether the user can view any units.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if($user->hasDirectPermission('viewAny_units')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the unit.
     *
     * @param  \App\User  $user
     * @param  \App\Unit  $unit
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_units')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create units.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_units')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the unit.
     *
     * @param  \App\User  $user
     * @param  \App\Unit  $unit
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('view_units')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the unit.
     *
     * @param  \App\User  $user
     * @param  \App\Unit  $unit
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_units')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the unit.
     *
     * @param  \App\User  $user
     * @param  \App\Unit  $unit
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_units')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the unit.
     *
     * @param  \App\User  $user
     * @param  \App\Unit  $unit
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if($user->hasDirectPermission('forceDelete_units')){
            return true;
        }
        return false;
    }
}
