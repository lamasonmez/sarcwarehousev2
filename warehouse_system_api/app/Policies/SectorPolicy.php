<?php

namespace App\Policies;

use App\User;
use App\Models\Sector;
use Illuminate\Auth\Access\HandlesAuthorization;

class SectorPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any sectors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if($user->hasDirectPermission('viewAny_sectors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the sector.
     *
     * @param  \App\User  $user
     * @param  \App\Sector  $sector
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_sectors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create sectors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_sectors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the sector.
     *
     * @param  \App\User  $user
     * @param  \App\Sector  $sector
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('view_sectors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the sector.
     *
     * @param  \App\User  $user
     * @param  \App\Sector  $sector
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_sectors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the sector.
     *
     * @param  \App\User  $user
     * @param  \App\Sector  $sector
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_sectors')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the sector.
     *
     * @param  \App\User  $user
     * @param  \App\Sector  $sector
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if($user->hasDirectPermission('forceDelete_sectors')){
            return true;
        }
        return false;
    }
}
