<?php

namespace App\Policies;

use App\User;
use App\Models\MinimumQuantity;
use Illuminate\Auth\Access\HandlesAuthorization;

class MinimumQuantityPolicy
{
    use HandlesAuthorization;
   /**
     * Determine whether the user can view any minimumquantities.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if($user->hasDirectPermission('viewAny_minimumquantities')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the minimumquantity.
     *
     * @param  \App\User  $user
     * @param  \App\MinimumQuantity  $minimumquantity
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_minimumquantities')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create minimumquantities.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_minimumquantities')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the minimumquantity.
     *
     * @param  \App\User  $user
     * @param  \App\MinimumQuantity  $minimumquantity
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('view_minimumquantities')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the minimumquantity.
     *
     * @param  \App\User  $user
     * @param  \App\MinimumQuantity  $minimumquantity
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_minimumquantities')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the minimumquantity.
     *
     * @param  \App\User  $user
     * @param  \App\MinimumQuantity  $minimumquantity
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_minimumquantities')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the minimumquantity.
     *
     * @param  \App\User  $user
     * @param  \App\MinimumQuantity  $minimumquantity
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if($user->hasDirectPermission('forceDelete_minimumquantities')){
            return true;
        }
        return false;
    }
}
