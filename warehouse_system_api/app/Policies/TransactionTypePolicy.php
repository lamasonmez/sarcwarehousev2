<?php

namespace App\Policies;

use App\User;
use App\Models\TransactionType;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionTypePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any = transaction types.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if($user->hasDirectPermission('viewAny_transactiontypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the = transaction type.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_transactiontypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create = transaction types.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_transactiontypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the = transaction type.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('update_transactiontypes')){
            return true;
        }
        return false;
        
    }

    /**
     * Determine whether the user can delete the = transaction type.
     *
     * @param  \App\User  $user
     
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_transactiontypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the = transaction type.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_transactionTypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the = transaction type.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if($user->hasDirectPermission('forceDelete_transactionTypes')){
            return true;
        }
        return false;

    }
}
