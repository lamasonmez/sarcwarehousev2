<?php

namespace App\Policies;

use App\User;
use App\Models\SubBranch;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubBranchPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any sub branches.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //

        
        if($user->hasDirectPermission('viewAny_subbranches')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the sub branch.
     *
     * @param  \App\User  $user
     * @param  \App\SubBranch  $subBranch
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_subbranches')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create sub branches.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_subbranches')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the sub branch.
     *
     * @param  \App\User  $user
     * @param  \App\SubBranch  $subBranch
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('update_subbranches')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the sub branch.
     *
     * @param  \App\User  $user
     * @param  \App\SubBranch  $subBranch
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_subbranches')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the sub branch.
     *
     * @param  \App\User  $user
     * @param  \App\SubBranch  $subBranch
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_subbranches')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the sub branch.
     *
     * @param  \App\User  $user
     * @param  \App\SubBranch  $subBranch
     * @return mixed
     */
    public function forceDelete(User $user, SubBranch $subBranch)
    {
        //
        if($user->hasDirectPermission('foreDelete_subbranches')){
            return true;
        }
        return false;
    }
}
