<?php

namespace App\Policies;

use App\User;
use App\Models\WarehouseType;
use Illuminate\Auth\Access\HandlesAuthorization;

class WarehouseTypePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any warehousetypes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if($user->hasDirectPermission('viewAny_warehousetypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the WarehouseType.
     *
     * @param  \App\User  $user
     * @param  \App\WarehouseType  $WarehouseType
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_warehousetypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create warehousetypes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_warehousetypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the WarehouseType.
     *
     * @param  \App\User  $user
     * @param  \App\WarehouseType  $WarehouseType
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('view_warehousetypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the WarehouseType.
     *
     * @param  \App\User  $user
     * @param  \App\WarehouseType  $WarehouseType
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_warehousetypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the WarehouseType.
     *
     * @param  \App\User  $user
     * @param  \App\WarehouseType  $WarehouseType
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_warehousetypes')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the WarehouseType.
     *
     * @param  \App\User  $user
     * @param  \App\WarehouseType  $WarehouseType
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if($user->hasDirectPermission('forceDelete_warehousetypes')){
            return true;
        }
        return false;
    }
}
