<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GeoPolicy
{
    use HandlesAuthorization;

    /**
    * Determine whether the user can view any branches.
    *
    * @param  \App\User  $user
    * @return mixed
    */
   public function viewAny(User $user)
   {
       //
       if($user->hasDirectPermission('viewAny_geos')){
           return true;
       }
       return false;
   }

   /**
    * Determine whether the user can view the branch.
    *
    * @param  \App\User  $user
    * @return mixed
    */
   public function view(User $user)
   {
       //
       if($user->hasDirectPermission('view_geos')){
           return true;
       }
       return false;
   }

   /**
    * Determine whether the user can create branches.
    *
    * @param  \App\User  $user
    * @return mixed
    */
   public function create(User $user)
   {
       //
       if($user->hasDirectPermission('create_geos')){
           return true;
       }
       return false;
   }

   /**
    * Determine whether the user can update the branch.
    *
    * @param  \App\User  $user
    * @return mixed
    */
   public function update(User $user)
   {
       //
       if($user->hasDirectPermission('update_geos')){
           return true;
       }
       return false;
   }

   /**
    * Determine whether the user can delete the branch.
    *
    * @param  \App\User  $user
    * @return mixed
    */
   public function delete(User $user)
   {
       //
       if($user->hasDirectPermission('delete_geos')){
           return true;
       }
       return false;
   }

   /**
    * Determine whether the user can restore the branch.
    *
    * @param  \App\User  $user
    * @return mixed
    */
   public function restore(User $user)
   {
       //
       if($user->hasDirectPermission('restore_geos')){
           return true;
       }
       return false;
   }

   /**
    * Determine whether the user can permanently delete the branch.
    *
    * @param  \App\User  $user
    * @return mixed
    */
   public function forceDelete(User $user)
   {
       //
       if($user->hasDirectPermission('foreDelete_geos')){
           return true;
       }
       return false;
   }
}
