<?php

namespace App\Policies;

use App\User;
use App\Material;
use Illuminate\Auth\Access\HandlesAuthorization;

class MaterialPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any materials.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if($user->hasDirectPermission('viewAny_materials')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the Material.
     *
     * @param  \App\User  $user
     * @param  \App\Material  $material
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_materials')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create materials.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_materials')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the Material.
     *
     * @param  \App\User  $user
     * @param  \App\Material  $material
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('view_materials')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the Material.
     *
     * @param  \App\User  $user
     * @param  \App\Material  $material
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_materials')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the Material.
     *
     * @param  \App\User  $user
     * @param  \App\Material  $material
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_materials')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the Material.
     *
     * @param  \App\User  $user
     * @param  \App\Material  $material
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if($user->hasDirectPermission('forceDelete_materials')){
            return true;
        }
        return false;
    }
}
