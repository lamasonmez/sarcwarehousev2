<?php

namespace App\Policies;

use App\User;
use App\Model\CTN;
use Illuminate\Auth\Access\HandlesAuthorization;

class CTNPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any ctns.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        if($user->hasDirectPermission('viewAny_ctns')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the CTN.
     *
     * @param  \App\User  $user
     * @param  \App\CTN  $CTN
     * @return mixed
     */
    public function view(User $user)
    {
        //
        if($user->hasDirectPermission('view_ctns')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create ctns.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if($user->hasDirectPermission('create_ctns')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the CTN.
     *
     * @param  \App\User  $user
     * @param  \App\CTN  $CTN
     * @return mixed
     */
    public function update(User $user)
    {
        //
        if($user->hasDirectPermission('update_ctns')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the CTN.
     *
     * @param  \App\User  $user
     * @param  \App\CTN  $CTN
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        if($user->hasDirectPermission('delete_ctns')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the CTN.
     *
     * @param  \App\User  $user
     * @param  \App\CTN  $CTN
     * @return mixed
     */
    public function restore(User $user)
    {
        //
        if($user->hasDirectPermission('restore_ctns')){
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can permanently delete the CTN.
     *
     * @param  \App\User  $user
     * @param  \App\CTN  $CTN
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
        if($user->hasDirectPermission('foreDelete_ctns')){
            return true;
        }
        return false;
    }
}
