<?php

namespace App\Repositories;


use App\Repositories\Contracts\BaseRepository;
use App\Models\DonorMaterial;

class DonorMaterialRepository extends BaseRepository
{
    public function __construct(DonorMaterial $donor_material)
    {
        parent::__construct($donor_material);
    }
}
