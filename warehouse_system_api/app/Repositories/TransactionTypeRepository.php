<?php

namespace  App\Repositories;

use App\Repositories\Contracts\BaseRepository;
use App\Models\TransactionType;

class TransactionTypeRepository extends BaseRepository
{
     public function __construct(TransactionType $transactiontype)
    {
        parent::__construct($transactiontype);
    }
}





