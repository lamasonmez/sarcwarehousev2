<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\Warehouse;
use Laravel\Passport\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens, Notifiable , HasRoles , LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','full_name','phone','is_approved','warehouse_id',
        'branchable_id','branchable_type','rolable_id','rolable_type'
    ];
    protected $with = ['warehouse','branchable'];

    protected $appends=['role'];

    public function setFullNameAttribute($value) {
        $this->attributes['full_name'] = json_encode($value,JSON_UNESCAPED_UNICODE);
    }

     public function getFullNameAttribute($value){
        return json_decode($value);
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function branchable()
    {
        return $this->morphTo();
    }
    public function donorable()
    {
        return $this->morphTo();
    }
    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }
    public function getRoleAttribute(){
        // if($this->getRoleNames() && )
        // return $this->getRoleNames()[0];
        // else return '';
        return $this->getRoleNames();
    }
}
