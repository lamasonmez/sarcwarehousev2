<?php


namespace App\Services\TransactionOut;


use App\Services\Contracts\IBaseService;
use Illuminate\Http\Request;
use App\User;
/**
 * Interface ITransactionOutService
 * @package App\Services\TransactionOut
 */
interface ITransactionOutService extends IBaseService
{

    /** gets all out transactions according to user
     * @param User $user
     * @return mixed
     */
    public function getTransactions(User $user);

    /**
     * @param Request $filters
     * @return builder
     */
    public function getTransactionsByFilter(User $user,Request $filters);


}
