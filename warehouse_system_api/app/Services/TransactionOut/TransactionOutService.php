<?php


namespace App\Services\TransactionOut;


use App\Repositories\TransactionOutRepository;
use App\Services\Contracts\BaseService;

use App\Models\TransactionOut;
use Illuminate\Http\Request;
use App\User;
use App\Models\DistributionPoint;

use App\Helpers\WarehouseHelper;
use App\Filters\TransactionFilters\WaybillNo;
use App\Filters\TransactionFilters\TransactionOutFilter;
use App\Filters\TransactionFilters\FromWarehouse;
use App\Filters\TransactionFilters\ToWarehouse;
use App\Filters\Warehouse;
use App\Filters\DateRange;
use App\Models\TransactionIn;

/**
 * Class TransactionOutServiceTra
 * @package App\Services\TransactionOut
 */
class TransactionOutService extends BaseService implements ITransactionOutService
{
    /**
     * TransactionOutService constructor.
     * @param TransactionOutRepository $repository
     */
    public function __construct(TransactionOutRepository $repository)
    {
        parent::__construct($repository);
    }

    public function store(Request $request)
    {
        $status = null;
         //TODO this solution is Temp
        if($request->toable_type == "App\Models\DistributionPoint" && $request->toable_id==null){
            $request->toable_id = DistributionPoint::create(["name"=>$request->distribution])->id;
        }
        // else if($request->toable_type=="App\Models\Warehouse"){
        //     $status ='pending';
        // }
        $status='';
        $transaction = $this->repository->create(
            [
                "waybill_no"=>$request->waybill_no,
                "date"=>$request->date,
                "toable_id"=>$request->toable_id,
                "toable_type"=>$request->toable_type,
                "warehouse_id"=>$request->warehouse_id,
                "status"=>$status,
                "weight"=>$request->weight,
                "donor_id"=>$request->donor_id
            ]
        );
        if($request->waybill_image){
            $extension = '.jpeg';
            $mdf5 = md5($request->waybill_no.'_'.time()).'.'.$extension;
            $transaction->addMediaFromBase64($request->image)->usingFileName($mdf5)->withResponsiveImages()->toMediaCollection('waybills_out');

        }
        $transaction = $this->repository->find($transaction->id);
        return $transaction;
    }

    public function update(Request $request, $id)
    {
        $status = null;
        $transaction = $this->repository->find($id);
        //TODO this solution is Temp
        if($request->toable_type == "App\Models\DistributionPoint" && $request->toable_id==null){
            $request->toable_id = DistributionPoint::create(["name"=>$request->distribution])->id;
        }
        else if(
            $request->toable_type == "App\Models\DistributionPoint" &&
            $request->toable_id!=null &&
            $transaction->toable && $request['toable'] &&
            $transaction->toable->name != $request['toable']['name']
            ){
            $toable = DistributionPoint::find($request->toable_id);
            $toable->name = $request['toable']['name'];
            $toable->save();
        }
        // else if($request->toable_type=="App\Models\Warehouse" ){
        //     if($transaction->status=='approved'){
        //         $status ='edited';
        //     }
        //     else{
        //         $status ='pending';
        //     }
        // }

        $result = $this->repository->update(
        [
                "waybill_no"=>$request->waybill_no,
                "date"=>$request->date,
                "toable_id"=>$request->toable_id,
                "toable_type"=>$request->toable_type,
                "warehouse_id"=>$request->warehouse_id,
                "status"=>$status,
                "weight"=>$request->weight,
                "donor_id"=>$request->donor_id

            ]
        ,$id);
        if($request->waybill_image!=null &&  $request->waybill_image!= $transaction->src){
            $extension = '.jpeg';
            $mdf5 = md5($request->waybill_no.'_'.time()).'.'.$extension;
            $transaction->addMediaFromBase64($request->image)->usingFileName($mdf5)->withResponsiveImages()->toMediaCollection('waybills_out');
        }
        elseif($request->waybill_image==null && $transaction->image){
            $transaction->image->delete();
        }
        return $transaction;

    }

    public function getTransactions(User $user){
        $warehosues= WarehouseHelper::getWarehouses($user)->pluck('id');
        $result = TransactionOut::whereIn('warehouse_id',$warehosues)
                                ->latest('date')
                                ->whereHas('from')
                                ->with(['from','toable']);
        return $result;
    }

    public function getTransactionsByFilter(User $user,Request $filters){
        $query = $this->getTransactions($user);
        if($filters->filled('waybill_no')){
            $query = WaybillNo::apply($query,$filters->waybill_no);
        }
        if($filters->filled('warehouse')){
            $query = ToWarehouse::apply($query,$filters->warehouse);
        }
        if($filters->filled('date_range')){
            $query = DateRange::apply($query,$filters->date_range);
        }
        if($filters->filled('donor')){
            $query = TransactionOutFilter::apply($query,$filters->donor);
        }
        return $query;

    }

}
