<?php


namespace App\Services\TransactionDetail;

use App\Models\DonorMaterial;
use App\Models\Material;
use App\Repositories\TransactionDetailRepository;
use App\Services\Contracts\BaseService;
use App\Helpers\WarehouseHelper;
use App\Helpers\TransactionHelper;
use App\User;
use App\Models\TransactionDetail;
use App\Models\TransactionIn;
use App\Models\TransactionOut;
use FontLib\Table\Type\name;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Exceptions\InsufficientBalance;
use App\Filters\DateRange;
use App\Filters\Warehouse as WarehouseFiler;
use App\Filters\TransactionDetailFilters\ByUnit;
use App\Filters\TransactionDetailFilters\ByBranch;
use App\Filters\TransactionDetailFilters\ByMaterial;
use App\Filters\TransactionDetailFilters\ByMaterialName;

use App\Filters\TransactionDetailFilters\FromDonor;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Null_;

//namespaces

/**
 * Class TransactionDetailService
 * @package App\Services\User
 */
class TransactionDetailService extends BaseService implements ITransactionDetailService
{
    /**
     * TransactionDetailService constructor.
     * @param TransactionDetailRepository $repository
     */
    public function __construct(TransactionDetailRepository $repository)
    {
        parent::__construct($repository);
    }

    public function getMaterialsCount(User $user)
    {
        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');
        $query = DB::table('transaction_details')
            ->select(DB::raw("COUNT(DISTINCT(donor_material_id)) as material_count"))
            ->where('deleted_at', null)
            ->whereIn('warehouse_id', $warehosues)->get();
        return $query;

    }

    public function getTransactionInCount(User $user)
    {
        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');
        $query = DB::table('transaction_details')
            ->select(DB::raw("COUNT(DISTINCT(transactionable_id)) as in_count"))
            ->where('transactionable_type', 'App\\Models\\TransactionIn')
            ->where('transaction_details.deleted_at', null)
            ->whereIn('warehouse_id', $warehosues)->get();
        return $query;
    }

    public function getTransactionOutCount(User $user)
    {
        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');
        $query = DB::table('transaction_details')
            ->select(DB::raw("COUNT(DISTINCT(transactionable_id)) as out_count"))
            ->where('transactionable_type', 'App\\Models\\TransactionOut')
            ->whereNull('transaction_details.deleted_at')
            ->whereIn('warehouse_id', $warehosues)->get();
        return $query;
    }

    public function getInventory(User $user, Request $request, $export = false)
    {


        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');

        $materials = DB::table('transaction_details')
            ->join('donor_materials', 'donor_materials.id', 'transaction_details.donor_material_id')
            ->select(DB::raw("DISTINCT(donor_material_id)"))
            ->where('transaction_details.deleted_at', null)
            ->whereIn('warehouse_id', $warehosues);
        $data = [];
        $total = count($materials->get());
        if ($request->page && !$export) {
            $materials = $materials->paginate($request->perPage, ['*'], 'page', $request->page);
            $data = $materials->items();
        } else {
            $data = $materials->get();
        }
        $result = collect();
        foreach ($data as $item) {
            $query = TransactionDetail::query()
                ->where('donor_material_id', $item->donor_material_id)
                ->whereIn('warehouse_id', $warehosues)
                ->latest('date')
                ->latest('id')->first();

            $opening_balance = TransactionDetail::query()
                ->where('donor_material_id', $item->donor_material_id)
                ->whereIn('warehouse_id', $warehosues)
                ->first('date')
                ->first('id')->first();

            $total_in = TransactionDetail::query()
                ->where('donor_material_id', $item->donor_material_id)
                ->whereIn('warehouse_id', $warehosues)
                ->where('transactionable_type', 'App\\Models\\TransactionIn')
                ->sum('quantity');

            $total_out = TransactionDetail::query()
                ->where('donor_material_id', $item->donor_material_id)
                ->whereIn('warehouse_id', $warehosues)
                ->where('transactionable_type', 'App\\Models\\TransactionOut')
                ->sum('quantity');

            $detail = [
                'transaction' => $query,
                'closing_balance' => $query->closing_balance,
                'opening_balance' => $opening_balance->opening_balance,
                'total_in' => $total_in,
                'total_out' => $total_out
            ];

            $result = $result->merge([$detail]);

        }
        if ($request->page) {
            return [
                'current_page' => $materials->currentPage(),
                'data' => $result,
                'first_page_url' => $materials->url(1),
                'from' => $materials->firstItem(),
                'last_page' => $materials->lastPage(),
                'last_page_url' => $materials->url($materials->lastPage()),
                'next_page_url' => $materials->nextPageUrl(),
                'path' => $materials->path(),
                'per_page' => $materials->perPage(),
                'prev_page_url' => $materials->previousPageUrl(),
                'to' => $materials->lastItem(),
                'total' => $total,
                "links" => $materials->toArray()["links"],
            ];
        } else {
            return $result;
        }


    }

    public function filterInventory(User $user, $filters, Request $request, $export = false)
    {
        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');
        $start = null;
        $end = null;
        if ($filters['dateRange'] != "") {
            $start = $filters['dateRange'][0];
            $end = $filters['dateRange'][1];
        }
        $materials = DB::table('transaction_details')
            ->join('donor_materials', 'donor_materials.id', 'transaction_details.donor_material_id')
            ->select(DB::raw("DISTINCT(donor_material_id)"))
            ->where('transaction_details.deleted_at', null)
            ->whereIn('warehouse_id', $warehosues);

        if ($filters['keyword'] != "") {

            $material_id = Material::where('name', 'like', '%' . $filters['keyword'] . '%')->pluck('id');
            $donor_material_ids = DonorMaterial::whereIn('material_id', $material_id)->pluck('id');
            $materials = $materials->whereIn('donor_material_id', $donor_material_ids);
        }
//        if ($start != null && $end != null) {
//
//            $materials = $materials->whereDate('date', '<=', $end);
//        }

        $data = [];
        $total = count($materials->get());

        if ($request->page && !$export) {
            $materials = $materials->paginate($request->perPage, ['*'], 'page', $request->page);
            $data = $materials->items();
        } else {
            $data = $materials->get();
        }

        $result = collect();
        foreach ($data as $item) {

            $query = TransactionDetail::query()
                ->where('donor_material_id', $item->donor_material_id)
                ->whereIn('warehouse_id', $warehosues)
                ->latest('id');

            $first_transaction = TransactionDetail::query()
                ->where('donor_material_id', $item->donor_material_id)
                ->whereIn('warehouse_id', $warehosues);
            $total_in = TransactionDetail::query()
                ->where('donor_material_id', $item->donor_material_id)
                ->whereIn('warehouse_id', $warehosues)
                ->where('transactionable_type', 'App\\Models\\TransactionIn');
            $total_out = TransactionDetail::query()
                ->where('donor_material_id', $item->donor_material_id)
                ->whereIn('warehouse_id', $warehosues)
                ->where('transactionable_type', 'App\\Models\\TransactionOut');

            if ($start != null && $end != null) {
                $query = $query->whereDate('date', '<=', $end)
                    ->orderBy('date', 'desc');
                $first_transaction = $first_transaction->whereDate('date', '>=', $start)
                    ->orderBy('date', 'asc');
                $total_in = $total_in->whereBetween('date', $filters['dateRange']);
                $total_out = $total_out->whereBetween('date', $filters['dateRange']);
            }
            $query = $query->first();
            $first_transaction = $first_transaction->first();
            $total_in = $total_in->sum('quantity');
            $total_out = $total_out->sum('quantity');

            $detail = [
                'transaction' => $query,
                'closing_balance' => $query->closing_balance,
                'opening_balance' => $first_transaction ? $first_transaction->opening_balance : $query->closing_balance,
                'total_in' => $total_in,
                'total_out' => $total_out
            ];

            $result = $result->merge([$detail]);

        }
        if ($request->page) {
            return [
                'current_page' => $materials->currentPage(),
                'data' => $result,
                'first_page_url' => $materials->url(1),
                'from' => $materials->firstItem(),
                'last_page' => floor($total / $materials->perPage()),
                'last_page_url' => $materials->url(floor($total / $materials->perPage())),
                'next_page_url' => $materials->nextPageUrl(),
                'path' => $materials->path(),
                'per_page' => $materials->perPage(),
                'prev_page_url' => $materials->previousPageUrl(),
                'to' => $total,
                'total' => $total,
                "links" => $materials->toArray()["links"],
            ];
        } else {
            return $result;
        }


    }

    public
    function getTransactionDetails(int $transactionable_id, string $transactionable_type)
    {
        return TransactionDetail::where('transactionable_id', $transactionable_id)
            ->where('transactionable_type', $transactionable_type);
    }


    public
    function store(Request $request)
    {


        $transaction = null;
        $data = null;
        if ($request['transactionable_type'] == "App\Models\TransactionOut") {
            $data = TransactionOut::query()->findOrFail($request->transactionable_id);
        }

        $prev_closing = TransactionHelper::previousClosingBalance($request, $data);
        $closing_balance = ($request['transactionable_type'] == "App\Models\TransactionOut") ? $prev_closing - $request['quantity'] : $prev_closing + $request['quantity'];
        if ($closing_balance >= 0) {
            $transaction = $this->repository->create($request->all());
            $transaction->date = $transaction->transactionable->date;
            if ($request['transactionable_type'] == "App\Models\TransactionOut") {
                if ($transaction->transactionable->toable_type == "App\Models\Warehouse") {
                    $transaction->transaction_type_id = 1;// مناقلة صادر
                }
            }
            $transaction->save();
        } else {
            throw new InsufficientBalance('لا يوجد لديك رصيد كافي لهذه المادة');
        }
        return $transaction;
    }

    public
    function update(Request $request, $id)
    {
        $result = $this->repository->update(
            [
                'quantity' => $request->quantity,
                'notes' => $request->notes,
                'is_convoy' => $request->is_convoy,
                'loss' => $request->loss,
                'reason_for_loss' => $request->reason_for_loss,
                'damage' => $request->damage,
                'reason_for_damage' => $request->reason_for_damage,
                'expiration_date' => $request->expiration_date,
                'transactionable_type' => $request->transactionable_type,
                'transaction_type_id' => $request->transaction_type,
                'donor_material_id' => $request->donor_material_id,
                'warehouse_id' => $request->warehouse_id,
                'transactionable_id' => $request->transactionable_id,
                'ctn_id' => $request->ctn_id
            ]
            , $id);
        $transaction_detail = $this->repository->find($id);
        return $transaction_detail;

    }

    /** gets transaction details By Filters
     * @param Request $filters
     * @param User $user
     * @return builder builder
     */
    public
    function getTransactionDetailsByFilter(Request $filters)
    {
        $query = TransactionDetail::query();
        if ($filters->filled('branch')) {
            $query = ByBranch::apply($query, $filters->branch);
        }
        if ($filters->filled('subbranch')) {
            $query = ByBranch::apply($query, $filters->subbranch);
        }
        if ($filters->filled('warehouse')) {
            $query = WarehouseFiler::apply($query, $filters->warehouse);
        }
        if ($filters->filled('donor')) {
            $query = FromDonor::apply($query, $filters->donor);
        }
        if ($filters->filled('material')) {
            $query = ByMaterial::apply($query, $filters->material);
        }
        if ($filters->filled('unit')) {
            $query = ByUnit::apply($query, $filters->unit);
        }
        if ($filters->filled('transfer')) {
            $query = ByUnit::apply($query, $filters->transfer);
        }
        if ($filters->filled('date_range')) {
            $query = DateRange::apply($query, $filters->date_range);
        }
        return $query;
    }


}
