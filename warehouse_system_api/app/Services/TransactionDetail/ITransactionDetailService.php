<?php


namespace App\Services\TransactionDetail;


use App\Services\Contracts\IBaseService;
use App\User;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * Interface ITransactionDetailService
 * @package App\Services\TransactionDetail
 */
interface ITransactionDetailService extends IBaseService
{

    /** gets materials count according to user
    * @param App\User $user
    * @return int number
    */
    public function getMaterialsCount(User $user);

    /** gets transaction in count according to user
    * @param App\User $user
    * @return int number
    */
    public function getTransactionInCount(User $user);

    /** gets transaction out count according to user
    * @param App\User $user
    * @return int number
    */
    public function getTransactionOutCount(User $user);

    /** gets inventory data according to user
    * @param App\User $user
    * @return Collection inventory
    */
    public function getInventory(User $user,Request $request,$export);


    /** filter inventory data according to user
    * @param App\User $user
    * @param $filters collection of filters
    * @return Collection inventory
    */
    public function filterInventory(User $user,$filters,Request $request,$export);

     /** gets transaction details
    * @param int $transactionable_id
    * @param string $transactionable_type
    * @return Collection details
    */
    public function getTransactionDetails(int $transactionable_id,string $transactionable_type);



    /** gets transaction details By Filters
    * @param Request $filters
    * @param User $user
    * @return builder builder
    */
    public function getTransactionDetailsByFilter(Request $filters);
}
