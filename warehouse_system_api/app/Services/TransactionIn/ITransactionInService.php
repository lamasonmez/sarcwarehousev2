<?php


namespace App\Services\TransactionIn;


use App\Services\Contracts\IBaseService;
use App\User;
use Illuminate\Http\Request;

/**
 * Interface ITransactionInService
 * @package App\Services\TransactionIn
 */
interface ITransactionInService extends IBaseService
{

    /** gets all in transactions according to user
     * @param User $user
     * @return mixed
     */
    public function getTransactions(User $user);

    /**
     * @param Request $filters
     * @return builder
     */
    public function getTransactionsByFilter(User $user,Request $filters);

    /** get't pending Transaction Ins from warehoues
     * @param App\User $user the user that recieved the transaction
     * @return collection
     */
    public function getPendingTransactions(User $user);
}
