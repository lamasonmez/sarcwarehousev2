<?php


namespace App\Services\TransactionIn;


use App\Repositories\TransactionInRepository;
use App\Services\Contracts\BaseService;

use App\Models\TransactionIn;
use App\Helpers\WarehouseHelper;
use App\User;
use Illuminate\Http\Request;
use App\Filters\TransactionFilters\WaybillNo;
use App\Filters\TransactionFilters\FromDonor;
use App\Filters\TransactionFilters\FromWarehouse;
use App\Filters\TransactionFilters\ToWarehouse;
use App\Filters\Warehouse;
use App\Filters\DateRange;
use App\Filters\TransactionFilters\DonorWaybillNo;

/**
 * Class TransactionInService
 * @package App\Services\TransactionIn
 */
class TransactionInService extends BaseService implements ITransactionInService
{

    /**
     * TransactionInService constructor.
     * @param TransactionInRepository $repository
     */
    public function __construct(TransactionInRepository $repository)
    {
        parent::__construct($repository);
    }


    public function store(Request $request)
    {
        $transaction = $this->repository->create($request->all());
        if($request->waybill_image){
            $extension = '.jpeg';
            $mdf5 = md5($request->waybill_no.'_'.time()).'.'.$extension;
            $transaction->addMediaFromBase64($request->image)->usingFileName($mdf5)->withResponsiveImages()->toMediaCollection('waybills_in');

        }
        return $transaction;
    }

    public function update(Request $request, $id)
    {
        $result = $this->repository->update(
            [
                "waybill_no"=>$request->waybill_no,
                "date"=>$request->date,
                "fromable_id"=>$request->fromable_id,
                "fromable_type"=>$request->fromable_type,
                "warehouse_id"=>$request->warehouse_id,
                "status"=>$request->status,
            ]
        ,$id);
        $transaction = $this->repository->find($id);
        if($request->waybill_image!=null &&  $request->waybill_image!= $transaction->src){
            $extension = '.jpeg';
            $mdf5 = md5($request->waybill_no.'_'.time()).'.'.$extension;
            $transaction->addMediaFromBase64($request->image)->usingFileName($mdf5)->withResponsiveImages()->toMediaCollection('waybills_in');
        }
        elseif($request->waybill_image==null && $transaction->image){
            $transaction->image->delete();
        }
        return $transaction;

    }

    public function getTransactions(User $user){
        $warehosues= WarehouseHelper::getWarehouses($user)->pluck('id');
        $result = TransactionIn::whereIn('warehouse_id',$warehosues)
                                ->latest('date')
                                ->with(['to','fromable']);
        return $result;
    }

    public function getTransactionsByFilter(User $user,Request $filters){
        $query = $this->getTransactions($user);
        if($filters->filled('waybill_no')){
            $query = WaybillNo::apply($query,$filters->waybill_no);
        }
        if($filters->filled('donor_waybill_no')){
            $query = DonorWaybillNo::apply($query,$filters->donor_waybill_no);
        }
        if($filters->filled('warehouse')){
            $query = ToWarehouse::apply($query,$filters->warehouse);
        }
        if($filters->filled('date_range')){
            $query = DateRange::apply($query,$filters->date_range);
        }
        if($filters->filled('donor')){
            $query = FromDonor::apply($query,$filters->donor);
        }
        return $query;

    }


    /** get't pending Transaction Ins from warehoues
     * @param App\User $user the user that recieved the transaction
     * @return collection
     */
    public function getPendingTransactions(User $user){
        return TransactionIn::where('warehouse_id',$user->warehouse_id)
                            ->where('status','pending')
                            ->with(['warehouse','transfer','transfer.transaction_out','transfer.transaction_out.details'])
                            ->get();
    }


}
