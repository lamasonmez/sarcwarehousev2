<?php


namespace App\Services\Unit;


use App\Services\Contracts\IBaseService;

/**
 * Interface IUnitService
 * @package App\Services\Unit
 */
interface IUnitService extends IBaseService
{

}
