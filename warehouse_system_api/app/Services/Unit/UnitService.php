<?php


namespace App\Services\Unit;


use App\Repositories\UnitRepository;
use App\Services\Contracts\BaseService;

use App\Models\Unit;

/**
 * Class UnitService
 * @package App\Services\Unit
 */
class UnitService extends BaseService implements IUnitService
{
    /**
     * UnitService constructor.
     * @param UnitRepository $repository
     */
    public function __construct(UnitRepository $repository)
    {
        parent::__construct($repository);
    }

}
