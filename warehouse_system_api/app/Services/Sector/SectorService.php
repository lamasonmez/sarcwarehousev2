<?php


namespace App\Services\Sector;


use App\Repositories\SectorRepository;
use App\Services\Contracts\BaseService;

use App\Models\Sector;

/**
 * Class SectorService
 * @package App\Services\Sector
 */
class SectorService extends BaseService implements ISectorService
{
    /**
     * SectorService constructor.
     * @param SectorRepository $repository
     */
    public function __construct(SectorRepository $repository)
    {
        parent::__construct($repository);
    }

}
