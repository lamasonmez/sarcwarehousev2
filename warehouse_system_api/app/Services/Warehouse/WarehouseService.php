<?php


namespace App\Services\Warehouse;

use App\Helpers\WarehouseHelper;
use App\Models\SubBranch;
use App\Models\Branch;
use App\Repositories\WarehouseRepository;
use App\Services\Contracts\BaseService;

use App\Models\Warehouse;

/**
 * Class WarehouseService
 * @package App\Services\Warehouse
 */
class WarehouseService extends BaseService implements IWarehouseService
{
    /**
     * WarehouseService constructor.
     * @param WarehouseRepository $repository
     */
    public function __construct(WarehouseRepository $repository)
    {
        parent::__construct($repository);
    }

     /** gets warehouses for a branchable type
     * @param string $type
     * @param int $id
     */
    public function getWarehousesForBranchable(string $type,int $id){
        if($type=="branch"){
           $branch =  Branch::where('id',$id)->first();
           return WarehouseHelper::getWarehousesForBranch($branch);

        }
        return Warehouse::where('branchable_type',SubBranch::class)
                ->where('branchable_id',$id)->get();
    }

}
