<?php


namespace App\Services\Warehouse;


use App\Services\Contracts\IBaseService;

/**
 * Interface IWarehouseService
 * @package App\Services\Warehouse
 */
interface IWarehouseService extends IBaseService
{
    /** gets warehouses for a branchable type
     * @param string $type
     * @param int $id
     */
    public function getWarehousesForBranchable(string $type,int $id);
}
