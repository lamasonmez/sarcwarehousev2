<?php

namespace App\Services\JWT;

use Illuminate\Http\Response;
/**
 * Interface IJWTService JSON Web Token
 * @package App\Services\JWT
 */
interface IJWTService
{
    /**
     * @param Request
     * @return response()
     */
    public function getAuthenticatedUser();

    /** creates JWT token by using credentials (email ,passowrd) and grant_type of password for passport service
     * @param string $email
     * @param string $password
     * @return Object TokenObject
     */
    public function createTokenByCredentials(string $email, string $password): Object;
}
