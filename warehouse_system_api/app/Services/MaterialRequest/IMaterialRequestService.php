<?php


namespace App\Services\MaterialRequest;


use App\Services\Contracts\IBaseService;

/**
 * Interface IMaterialRequestService
 * @package App\Services\MaterialRequest
 */
interface IMaterialRequestService extends IBaseService
{

    /** get all requests or with specific status
     * @param $status
     * @return mixed
     */
    public function getRequests($status="");
}
