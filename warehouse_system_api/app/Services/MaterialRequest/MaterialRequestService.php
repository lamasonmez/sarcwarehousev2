<?php


namespace App\Services\MaterialRequest;


use App\Repositories\MaterialRequestRepository;
use App\Services\Contracts\BaseService;

use App\Models\MaterialRequest;

/**
 * Class MaterialRequestService
 * @package App\Services\MaterialRequest
 */
class MaterialRequestService extends BaseService implements IMaterialRequestService
{
    /**
     * MaterialRequestService constructor.
     * @param MaterialRequestRepository $repository
     */
    public function __construct(MaterialRequestRepository $repository)
    {
        parent::__construct($repository);
    }
      /** get all requests or with specific status
     * @param $status
     * @return mixed
     */
    public function getRequests($status=""){
        if($status==""){
            return MaterialRequest::cursor();
        }
        else{
            return MaterialRequest::where('status',$status);
        }
    }

}
