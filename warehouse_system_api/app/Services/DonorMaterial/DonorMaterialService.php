<?php


namespace App\Services\DonorMaterial;

use App\Constants\MaterialStatus;

use App\Repositories\DonorMaterialRepository;
use App\Services\Contracts\BaseService;

use App\Models\DonorMaterial;
use App\Models\TransactionDetail;
use App\Helpers\WarehouseHelper;
use Illuminate\Support\Facades\DB;
use App\User;

/**
 * Class DonorMaterialService
 * @package App\Services\User
 */
class DonorMaterialService extends BaseService implements IDonorMaterialService
{
    /**
     * DonorMaterialService constructor.
     * @param DonorMaterialRepository $repository
     */
    public function __construct(DonorMaterialRepository $repository)
    {
        parent::__construct($repository);
    }

    public function getDonorMaterials(int $donor_id)
    {

        return DonorMaterial::where('donor_id', $donor_id)->where(function ($query) {
            $query->doesnthave('requests')->orWhereHas('requests', function ($q) {
                $q->where('status', MaterialStatus::APPROVED);
            });
        });
    }


    public function getTransactionsForDonorMaterial(User $user, int $donor_material_id)
    {
        ini_set('max_execution_time', 300);
        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');
        $query = TransactionDetail::
        where('donor_material_id', $donor_material_id)
            ->where('deleted_at', null)
            ->whereIn('warehouse_id', $warehosues)
            ->orderBy('date')
            ->orderBy('created_at')
            ->get();

        return $query;

    }

    public function filterTransactionsForDonorMaterial(User $user, int $donor_material_id, $filters)
    {
        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');
        $query = TransactionDetail::
        where('donor_material_id', $donor_material_id)
            ->where('deleted_at', null)
            ->WhereBetween('date', $filters->dateRange)
            ->whereIn('warehouse_id', $warehosues)
            ->orderBy('date')
            ->orderBy('created_at')
            ->get();

        return $query;
    }

    /*TODO:lamaKhamis*/
    public function getWarehouseDonor(User $user)
    {


        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');

        $query = DB::table('transaction_details')
            ->join('donor_materials', 'donor_materials.id', 'transaction_details.donor_material_id')
            ->join('donors', 'donors.id', 'donor_materials.donor_id')
            ->select(DB::raw("DISTINCT(donor_material_id)"), "donors.*")
            ->where('transaction_details.deleted_at', null)
            ->whereIn('warehouse_id', $warehosues)->get()->groupBy('id')->map(function ($query, $id) {
                return [
                    'id' => $id,
                    'count_donor_materials' => $query->count(),
                    'data' => $query
                ];
            })->values();
        return $query;


    }

    public function exportDonorMaterial(User $user, int $donor_id)
    {
        $warehosues = WarehouseHelper::getWarehouses($user)->pluck('id');
        $query = DB::table('transaction_details')
            ->join('donor_materials', 'donor_materials.id', 'transaction_details.donor_material_id')
            ->join('donors', 'donors.id', 'donor_materials.donor_id')
            ->select(DB::raw("transaction_details.*"), "donors.*")
            ->where('transaction_details.deleted_at', null)
            ->whereIn('warehouse_id', $warehosues)->get()
            ->filter(function ($item) use ($donor_id) {
                return $item->id == $donor_id;
            })->values();
        return $query;


    }

}
