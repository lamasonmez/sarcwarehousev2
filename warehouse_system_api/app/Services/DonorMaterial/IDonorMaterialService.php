<?php


namespace App\Services\DonorMaterial;

use App\Services\Contracts\IBaseService;
use App\User;

/**
 * Interface IDonorMaterialService
 * @package App\Services\DonorMaterial
 */
interface IDonorMaterialService extends IBaseService
{

    /** gets all materials for a selected donor
     * @param int $donor_id
     * @return Collection of donor_materials
     */
    public function getDonorMaterials(int $donor_id);

    /** gets all transactions for a selected donor mateiral
     *@param int $donor_material_id
     *@return Collection of TransactionDetails
     */
    public function getTransactionsForDonorMaterial(User $user ,int $donor_material_id);



    /** gets all filtered transactions for a selected donor mateiral
     *@param int $donor_material_id
     *@param filters array of filters
     *@return Collection of TransactionDetails
     */
    public function filterTransactionsForDonorMaterial(User $user ,int $donor_material_id,$filters);


    public function getWarehouseDonor(User $user);
    public function exportDonorMaterial(User $user,int $donor_id);
}
