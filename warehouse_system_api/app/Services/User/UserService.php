<?php


namespace App\Services\User;


use App\Repositories\UserRepository;
use App\Services\Contracts\BaseService;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 * @package App\Services\User
 */
class UserService extends BaseService implements IUserService
{
    /**
     * UserService constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        parent::__construct($repository);
    }

    public function store(Request $request){

        $user = User::create([
            'name' =>$request->name,
            'email' =>$request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'full_name' => ['en' => $request->full_name['en'], 'ar' => $request->full_name['ar']],
            'branchable_id' => $request->branchable_id,
            'branchable_type' => $request->branchable_type,
            'warehouse_id' => $request->rolable_type=='App\\Models\\Warehouse'? $request->rolable_id: $request->warehouse_id,
            'rolable_id' => $request->rolable_id,
            'rolable_type' => $request->rolable_type,
        ]);
        if ($request->role_id){
            $role = Role::findorFail($request->role_id);
            if ($role) {
                $user->roles()->sync($role->id);
                $user->syncPermissions($role->permissions->pluck('name')->toArray());
            }
        }
        return $user;
    }

    public function update(Request $request, $id)
    {
        $this->repository->update($request->all(), $id);
        $user = $this->repository->find($id);
        if ($user && $request->role_id) {
            $role = Role::findorFail($request->role_id);
            if ($role){
                $user->roles()->sync($role->id);
                $user->syncPermissions($role->permissions->pluck('name')->toArray());
            }
        }
        return $user;

    }

}
