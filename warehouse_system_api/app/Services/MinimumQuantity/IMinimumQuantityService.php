<?php


namespace App\Services\MinimumQuantity;


use App\Services\Contracts\IBaseService;
use App\User;
/**
 * Interface IMinimumQuantityService
 * @package App\Services\MinimumQuantity
 */
interface IMinimumQuantityService extends IBaseService
{

    /** returns MinimumQuantity Data for a user
     * if the user is Admin it will fetch all data for all warehouse
     * if the user is responsible for just one warehouse it will fetch data for that warehouse
     * if the user is a reporter and has access on multiple warehouse it will fetch data for all (his/her) warehouses
     * @param $user
     * @return builder
     */
    public function getMinimumQuantityData(User $user);

     /** returns MinimumQuantityNoficiations Data for a the givin inventory
     * @param Collection $inventory
     * @return Collection
     */
    public function getMinimumQuantityNotifications($inventory);
}
