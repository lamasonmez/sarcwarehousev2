<?php


namespace App\Services\MinimumQuantity;


use App\Repositories\MinimumQuantityRepository;
use App\Services\Contracts\BaseService;
use App\Helpers\WarehouseHelper;
use App\Models\MinimumQuantity;
use App\User;
use Illuminate\Http\Request;

/**
 * Class MinimumQuantityService
 * @package App\Services\MinimumQuantity
 */
class MinimumQuantityService extends BaseService implements IMinimumQuantityService
{
    /**
     * MinimumQuantityService constructor.
     * @param MinimumQuantityRepository $repository
     */
    public function __construct(MinimumQuantityRepository $repository)
    {
        parent::__construct($repository);
    }

    /** returns MinimumQuantity Data for a user
     * if the user is Admin it will fetch all data for all warehouse
     * if the user is responsible for just one warehouse it will fetch data for that warehouse
     * if the user is a reporter and has access on multiple warehouse it will fetch data for all (his/her) warehouses
     * @param $user
     * @return builder
     */
    public function getMinimumQuantityData(User $user){
        $warehosues= WarehouseHelper::getWarehouses($user)->pluck('id');
        $result = MinimumQuantity::whereIn('warehouse_id',$warehosues)
                                ->latest();
        return $result;
    }
     /** returns MinimumQuantityNoficiations Data for a the givin inventory
     * @param Collection $inventory
     * @return Collection
     */
    public function getMinimumQuantityNotifications($inventory){
        //available minimum quantity data for the inventory
        $minimum_quantities = MinimumQuantity::whereIn('warehouse_id',$inventory->pluck('warehouse_id'))
                                ->whereIn('donor_material_id',$inventory->pluck('donor_material_id'))
                                ->distinct('donor_material_id')->get();
        $result = collect();
        foreach($minimum_quantities as $item){
            $matched_item = $inventory->where('donor_material_id',$item->donor_material_id)
                                      ->where('warehouse_id',$item->warehouse_id)->first();
            if($matched_item->closing_balance <= $item->minimum_quantity){
                //add it to notifications
                $result=$result->merge([
                  [
                    "minimum_quantity"=>$item->minimum_quantity,
                    "inventory_item"=>$matched_item
                  ]
                ]);
            }
        }
    return $result->toArray();
    }

    public function store(Request $request){
        return MinimumQuantity::updateOrCreate(
            ['donor_material_id' => $request->donor_material_id, 'warehouse_id' => $request->warehouse_id],
            ['minimum_quantity' => $request->minimum_quantity]
        );
    }


}
