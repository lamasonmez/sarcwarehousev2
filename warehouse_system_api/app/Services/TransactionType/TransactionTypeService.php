<?php


namespace App\Services\TransactionType;


use App\Repositories\TransactionTypeRepository;
use App\Services\Contracts\BaseService;

use App\Models\TransactionType;

/**
 * Class TransactionTypeService
 * @package App\Services\TransactionType
 */
class TransactionTypeService extends BaseService implements ITransactionTypeService
{
    /**
     * TransactionTypeService constructor.
     * @param TransactionTypeRepository $repository
     */
    public function __construct(TransactionTypeRepository $repository)
    {
        parent::__construct($repository);
    }

}
