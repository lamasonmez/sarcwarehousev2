<?php


namespace App\Services\TransactionType;


use App\Services\Contracts\IBaseService;

/**
 * Interface ITransactionTypeService
 * @package App\Services\TransactionType
 */
interface ITransactionTypeService extends IBaseService
{

}
