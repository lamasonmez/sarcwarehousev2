<?php


namespace App\Services\Material;


use App\Repositories\MaterialRepository;
use App\Services\Contracts\BaseService;

use App\Models\Material;
use Illuminate\Http\Request;

/**
 * Class MaterialService
 * @package App\Services\Material
 */
class MaterialService extends BaseService implements IMaterialService
{
    /**
     * MaterialService constructor.
     * @param MaterialRepository $repository
     */
    public function __construct(MaterialRepository $repository)
    {
        parent::__construct($repository);
    }
}
