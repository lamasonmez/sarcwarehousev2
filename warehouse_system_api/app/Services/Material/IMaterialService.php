<?php


namespace App\Services\Material;


use App\Services\Contracts\IBaseService;

/**
 * Interface IMaterialService
 * @package App\Services\Material
 */
interface IMaterialService extends IBaseService
{

}
