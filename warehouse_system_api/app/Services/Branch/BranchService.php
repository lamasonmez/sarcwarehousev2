<?php


namespace App\Services\Branch;


use App\Repositories\BranchRepository;
use App\Services\Contracts\BaseService;

use App\Models\Branch;

/**
 * Class BranchService
 * @package App\Services\Branch
 */
class BranchService extends BaseService implements IBranchService
{
    /**
     * BranchService constructor.
     * @param BranchRepository $repository
     */
    public function __construct(BranchRepository $repository)
    {
        parent::__construct($repository);
    }

}
