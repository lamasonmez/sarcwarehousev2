<?php


namespace App\Services\SubBranch;

use App\Models\Branch;
use App\Repositories\SubBranchRepository;
use App\Services\Contracts\BaseService;
use App\User;
use App\Models\SubBranch;

/**
 * Class SubBranchService
 * @package App\Services\SubBranch
 */
class SubBranchService extends BaseService implements ISubBranchService
{
    /**
     * SubBranchService constructor.
     * @param SubBranchRepository $repository
     */
    public function __construct(SubBranchRepository $repository)
    {
        parent::__construct($repository);
    }

    /** Gets All sub branches for a branch
     * @param int $branch_id
     * @return mixed
     */
    public function getSubBranchesForBranch(int $branch_id){
        return SubBranch::where('branch_id',$branch_id)->get();
    }

    /** Gets All sub branches according to user role
     * @param User $user
     * @return mixed
     */
    public function getSubBranchesForUser(User $user){
        if($user->hasRole('Admin')){
            return $this->index();
        }
        if($user->branchable_type==Branch::class){
                return $this->getSubBranchesForBranch($user->branchable_id);
        }
        else if($user->branchable_type==SubBranch::class){
            return SubBranch::where('id',$user->branchable_id)->get();
        }
        return $this->index();
    }

}
