<?php


namespace App\Services\SubBranch;


use App\Services\Contracts\IBaseService;
use App\User;
/**
 * Interface ISubBranchService
 * @package App\Services\SubBranch
 */
interface ISubBranchService extends IBaseService
{

    /** Gets All sub branches for a branch
     * @param int $branch_id
     * @return mixed
     */
    public function getSubBranchesForBranch(int $branch_id);

    /** Gets All sub branches according to user role
     * @param User $user
     * @return mixed
     */
    public function getSubBranchesForUser(User $user);
}
