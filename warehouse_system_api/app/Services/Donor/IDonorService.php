<?php


namespace App\Services\Donor;


use App\Services\Contracts\IBaseService;

/**
 * Interface IDonorService
 * @package App\Services\Donor
 */
interface IDonorService extends IBaseService
{

}
