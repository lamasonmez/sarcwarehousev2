<?php


namespace App\Services\Donor;


use App\Repositories\DonorRepository;
use App\Services\Contracts\BaseService;

use App\Models\Donor;

/**
 * Class DonorService
 * @package App\Services\Donor
 */
class DonorService extends BaseService implements IDonorService
{
    /**
     * DonorService constructor.
     * @param DonorRepository $repository
     */
    public function __construct(DonorRepository $repository)
    {
        parent::__construct($repository);
    }

}
