<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Services\TransactionDetail\ITransactionDetailService;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class InventoryExport implements FromView , WithEvents
{

    protected $data;
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }


    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setRightToLeft(true);
            },
        ];
    }

    /**
    * @return Illuminate\Contracts\View\View
    */
    public function view():View
    {   $user = \Auth::user('api');
        $uncategorized_data = json_decode($this->data);

        // $data =$this->service->getInventory($user)->groupBy('material_donor.donor_id');
         $data=collect($uncategorized_data)->groupBy('transaction.material_donor.donor_id');
         return view('exports.inventory', [
            'inventory' =>$data
        ]);

    }
}
