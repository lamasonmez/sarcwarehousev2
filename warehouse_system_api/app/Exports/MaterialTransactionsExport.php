<?php

namespace App\Exports;

use App\Services\DonorMaterial\IDonorMaterialService;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\Services\TransactionDetail\ITransactionDetailService;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class MaterialTransactionsExport implements FromView , WithEvents
{

    protected $service,$donor_material_id;
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IDonorMaterialService $service,$donor_material_id)
    {
        $this->service = $service;
        $this->donor_material_id = $donor_material_id;
    }


    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setRightToLeft(true);
            },
        ];
    }

    /**
    * @return Illuminate\Contracts\View\View
    */
    public function view():View
    {   $user = \Auth::user('api');
        $data =$this->service->getTransactionsForDonorMaterial($user,$this->donor_material_id);

        return view('exports.material_transactions', [
            'data' => $data
        ]);

    }
}
