<?php

namespace App\Exports;

use App\Services\TransactionDetail\ITransactionDetailService;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;
class WarehouseReportExport implements FromView , WithEvents
{
    private $transaction_detail_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ITransactionDetailService $transactionDetailService)
    {
        $this->transaction_detail_service = $transactionDetailService;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setRightToLeft(true);
            },
        ];
    }

    /**
    * @return Illuminate\Contracts\View\View
    */
    public function view():View
    {

        $data = $this->transaction_detail_service->getTransactionDetailsByFilter(request())->orderBy('date','asc')->orderBy('transactionable_type')->get()->groupBy(function($item,$key){
            return   Carbon::parse($item['date'])->format('m')."-".Carbon::parse($item['date'])->format('Y')."-".$item["donor_material_id"];
        });
        return view('exports.warehouse-report', [
            'data' => $data
        ]);

    }
}
