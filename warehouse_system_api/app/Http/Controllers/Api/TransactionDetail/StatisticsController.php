<?php

namespace App\Http\Controllers\Api\TransactionDetail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TransactionDetail\ITransactionDetailService;
class StatisticsController extends Controller
{
    protected $transactionDetailService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ITransactionDetailService $transactionDetailService)
    {
        $this->transactionDetailService = $transactionDetailService;
        $this->middleware('auth');
    }

    public function getDonorMaterialsCount(){
        $user = \Auth::user('api');
        return  $this->transactionDetailService->getMaterialsCount($user);
    }
    public function getTotalInCount(){
        $user = \Auth::user('api');
        return  $this->transactionDetailService->getTransactionInCount($user);
    }
    public function getTotalOutCount(){
        $user = \Auth::user('api');
        return  $this->transactionDetailService->getTransactionOutCount($user);
    }
}
