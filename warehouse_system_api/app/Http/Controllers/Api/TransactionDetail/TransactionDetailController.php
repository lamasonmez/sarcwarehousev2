<?php

namespace App\Http\Controllers\Api\TransactionDetail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TransactionDetail\ITransactionDetailService;
use App\Exceptions\InsufficientBalance;


class TransactionDetailController extends Controller
{
    protected $transactionDetailService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ITransactionDetailService $transactionDetailService)
    {
        $this->transactionDetailService = $transactionDetailService;
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        try {
            if ($request->id == null) {
                return $this->transactionDetailService->store($request);
            } else {
                return response()->json($this->transactionDetailService->update($request, $request->id));
            }
        } catch (InsufficientBalance $ex) {
            return response()->json([
                "error" => $ex->getMessage(),
            ]);
        }

    }

    public function delete(int $id)
    {
        return $this->transactionDetailService->delete($id);
    }

    public function getTransactionDetail(int $id)
    {
        return $this->transactionDetailService->view($id);
    }



}
