<?php

namespace App\Http\Controllers\Api\Donor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Donor;
use App\Services\Donor\IDonorService;

class DonorController extends Controller
{
    private $donor_service;

    public function __construct(IDonorService $donor_service)
    {
        $this->donor_service = $donor_service;
        $this->middleware('auth');
    }

    //TODO: Refactor use services
    public function getDonors(){
        return $this->donor_service->index();
    }

    public function store(Request $request){
        if($request->id==null){
            return $this->donor_service->store($request);
        }
        else{
            return $this->donor_service->update($request,$request->id);
        }
    }
    
    public function delete(int $id){
        return $this->donor_service->delete($id);
    }
}
