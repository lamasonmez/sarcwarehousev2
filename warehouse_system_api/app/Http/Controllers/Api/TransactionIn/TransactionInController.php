<?php

namespace App\Http\Controllers\Api\TransactionIn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TransactionIn\ITransactionInService;
use App\Services\TransactionDetail\ITransactionDetailService;

class TransactionInController extends Controller
{
    private $transaction_in_service,$transaction_detail_service;
    public function __construct(ITransactionInService $transaction_in_service,
                                ITransactionDetailService $transaction_detail_service)
    {
        $this->transaction_in_service = $transaction_in_service;
        $this->transaction_detail_service = $transaction_detail_service;
        $this->middleware('auth');
    }

    public function store(Request $request){
        if($request->id==null){
            return $this->transaction_in_service->store($request);
        }
        else{
            return $this->transaction_in_service->update($request,$request->id);
        }
    }

    public function getTransactionIns(Request $request){
        //return $request->page;
        $user = \Auth::user('api');
        return $this->transaction_in_service->getTransactionsByFilter($user,$request)
                                            ->paginate($request->perPage,['*'],'page',$request->page);
    }

    public function getTransactionInData(int $id){
        return $this->transaction_in_service->view($id);
    }
    public function getTransactionInDetails(int $id){
        return $this->transaction_detail_service->getTransactionDetails($id,'App\\Models\\TransactionIn')->get();
    }
    public function delete(int $id){
        return $this->transaction_in_service->delete($id);
    }
}
