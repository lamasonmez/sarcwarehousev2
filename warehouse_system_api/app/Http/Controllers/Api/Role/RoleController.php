<?php

namespace App\Http\Controllers\Api\Role;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    //

    public function getRoles(){
        return Role::all();
    }
}
