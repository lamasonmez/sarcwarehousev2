<?php

namespace App\Http\Controllers\Api\DonorMaterial;

use App\Exports\MaterialTransactionsExport;

use App\Http\Controllers\Controller;
use App\Models\DonorMaterial;
use Illuminate\Http\Request;
use App\Services\DonorMaterial\IDonorMaterialService;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Utilities;


class DonorMaterialController extends Controller
{
    private $donor_marerial_service;

    public function __construct(IDonorMaterialService $donor_marerial_service)
    {
        $this->donor_material_service = $donor_marerial_service;
    }

    public function getMaterialsDonorsAll()
    {
        return $this->donor_material_service->index();
    }

    public function getMaterialsForDonor($donor_id)
    {
        return $this->donor_material_service->getDonorMaterials($donor_id)->get();
    }

    public function getDonorMaterial($donor_material_id)
    {
        return $this->donor_material_service->view($donor_material_id);
    }

    public function store(Request $request)
    {
        if ($request->id) {
            return $this->donor_material_service->update($request, $request->id);
        } else {
            return $this->donor_material_service->store($request);
        }
    }

    public function getTransactionsForDM($donor_material_id)
    {

        return $this->donor_material_service->getTransactionsForDonorMaterial($user, $donor_material_id);
    }

    public function filterTransactionsForDM($donor_material_id, Request $request)
    {

        $user = \Auth::user('api');
        return $this->donor_material_service->filterTransactionsForDonorMaterial($user, $donor_material_id, $request);
    }

    public function delete(int $id)
    {
        return $this->donor_material_service->delete($id);
    }

    public function exportMaterialTransactions($donor_material_id, $type)
    {
        $filename = 'material_transaction_' . md5(time()) . '.' . $type;
        $filepath = '/public/exports/' . $filename;
        \Excel::store(new MaterialTransactionsExport($this->donor_material_service, $donor_material_id), $filepath, 'local', config('excel.extension_detector.' . $type));
        $download_path = asset('storage/exports/' . $filename);
        return response()->json([
            "path" => $download_path
        ]);

    }



    public function getWarehouseDonor()
    {
        $user = \Auth::user('api');

        return $this->donor_material_service->getWarehouseDonor($user);
    }

    public function exportDonorMaterial($donor_id)
    {

        $user = \Auth::user('api');

        $query = $this->donor_material_service->exportDonorMaterial($user, $donor_id);

        foreach ($query as $item) {

            $material_name_ar = DonorMaterial::where('id', $item->donor_material_id)->first()->material->name->ar;
            $material_name = str_replace('*', '-', $material_name_ar);
            $folderName = $item->code;
            Storage::disk('public')->makeDirectory($folderName);
            $filepath = $folderName . "/" . $material_name . '.xlsx';
            \Excel::store(new MaterialTransactionsExport($this->donor_material_service, $item->donor_material_id), $filepath,'public', config('excel.extension_detector.' . '.xlsx'));
        }
        return Utilities::ZipDownload($folderName);

    }


}
