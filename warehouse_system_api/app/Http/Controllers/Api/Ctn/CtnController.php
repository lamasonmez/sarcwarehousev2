<?php

namespace App\Http\Controllers\Api\Ctn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CTN;
class CtnController extends Controller
{
    //TOODO REactor this controller
    public function getCtns(int $donor_material_id=null){
        return CTN::when($donor_material_id!=null,function($q) use($donor_material_id){
            return $q->where('donor_material_id',$donor_material_id);
        })->get();
    }
}
