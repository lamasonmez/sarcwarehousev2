<?php

namespace App\Http\Controllers\Api\TransactionOut;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TransactionOut\ITransactionOutService;
use App\Services\TransactionDetail\ITransactionDetailService;

class TransactionOutController extends Controller
{
    private $transaction_out_service,$transaction_detail_service;
    public function __construct(ITransactionOutService $transaction_out_service,
                                ITransactionDetailService $transaction_detail_service)
    {
        $this->transaction_out_service = $transaction_out_service;
        $this->transaction_detail_service = $transaction_detail_service;
        $this->middleware('auth');
    }

    public function store(Request $request){
        if($request->id==null){
            return $this->transaction_out_service->store($request);
        }
        else{
            return $this->transaction_out_service->update($request,$request->id);
        }
    }

    public function getTransactionOuts(Request $request){
        $user = \Auth::user('api');
        return $this->transaction_out_service->getTransactionsByFilter($user,$request)
                                            ->paginate($request->perPage,['*'],'page',$request->page);

    }

    public function getTransactionOutData(int $id){
        return $this->transaction_out_service->view($id);
    }
    public function getTransactionOutDetails(int $id){
        return $this->transaction_detail_service->getTransactionDetails($id,'App\\Models\\TransactionOut')->get();
    }


    public function delete(int $id){
        return $this->transaction_out_service->delete($id);
    }
}
