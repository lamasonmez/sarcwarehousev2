<?php

namespace App\Http\Controllers\Api\Warehouse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\WarehouseHelper;
use App\Models\Warehouse;
use App\Services\Warehouse\IWarehouseService;

class WarehouseController extends Controller
{
    private $warehouse_service;

    public function __construct(IWarehouseService $warehouse_service)
    {
        $this->warehouse_service= $warehouse_service;
    }

    public function getWarehouses(){
        $warehosues= Warehouse::query()->with('branchable')->get();
        return $warehosues;
    }
    public function getWarehousesForBranchable($type,$id){
        return $this->warehouse_service->getWarehousesForBranchable($type,$id);
    }

}
