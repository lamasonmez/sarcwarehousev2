<?php

namespace App\Http\Controllers\Api\Warehouse;

use App\Exports\WarehouseReportExport;
use App\Http\Controllers\Controller;
use App\Services\TransactionDetail\ITransactionDetailService;
use Illuminate\Http\Request;
use Carbon\Carbon;
class WarehouseReportController extends Controller
{
    protected $transactionDetailService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ITransactionDetailService $transactionDetailService)
    {
        $this->transactionDetailService = $transactionDetailService;

    }

    public function export($type,Request $request)
    {
        $filename= 'warehouse_report_'.md5(time()).'.'.$type;
        $filepath= '/public/exports/'.$filename;
        \Excel::store(new WarehouseReportExport($this->transactionDetailService),$filepath,'local',config('excel.extension_detector.'.$type));
        $download_path = asset('storage/exports/'.$filename);
        return response()->json([
            "path"=>$download_path
        ]);

    }
    public function getReport(Request $request)
    {
        return   $this->transactionDetailService->getTransactionDetailsByFilter($request)->orderBy('transactionable_type')->get()->groupBy(function($item,$key){
            return   Carbon::parse($item['date'])->format('m')."-".Carbon::parse($item['date'])->format('Y')."-".$item["donor_material_id"];
        });
    }

    public function test(Request $request)
    {
        $data = $this->transactionDetailService->getTransactionDetailsByFilter($request)->orderBy('date')->orderBy('transactionable_type')->get()->groupBy(function($item,$key){
            return Carbon::parse($item['date'])->format('m')."-".Carbon::parse($item['date'])->format('Y')."-".$item["donor_material_id"];
        });

        return $data;

    }

}
