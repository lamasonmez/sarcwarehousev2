<?php

namespace App\Http\Controllers\Api\Notification;

use App\Http\Controllers\Controller;
use App\Services\MinimumQuantity\IMinimumQuantityService;
use App\Services\TransactionDetail\ITransactionDetailService;
use App\Services\TransactionIn\ITransactionInService;
use App\Services\TransactionOut\ITransactionOutService;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    //
    private $transaction_details_service,$minimum_quantity_servicem,$transaction_in_service;
    public function __construct(ITransactionDetailService $transaction_details_service,
                                IMinimumQuantityService $minimum_quantity_service,
                                ITransactionInService $transaction_in_service)
    {
        $this->transaction_details_service = $transaction_details_service;
        $this->minimum_quantity_service = $minimum_quantity_service;
        $this->transaction_in_service = $transaction_in_service;
        $this->middleware('auth');
    }
    public function getMinimumQuantity(Request $request){
        $user = \Auth::user('api');
       $inventory =  $this->transaction_details_service->getInventory($user,$request);
       $notifications = $this->minimum_quantity_service->getMinimumQuantityNotifications($inventory);
       return $notifications;
    }

    public function getPendingTransactions(){
        $user = \Auth::user('api');
        return $this->transaction_in_service->getPendingTransactions($user);

    }
}
