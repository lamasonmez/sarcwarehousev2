<?php

namespace App\Http\Controllers\Api\Branch;

use App\Http\Controllers\Controller;
use App\Services\Branch\IBranchService;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    private $branch_service;

    public function __construct(IBranchService $branch_service)
    {
        $this->branch_service = $branch_service;
    }


    public function getBranches(){
        return $this->branch_service->index();
    }


    public function store(Request $request){
        if($request->id==null){
            return $this->branch_service->store($request);
        }
        else{
            return $this->branch_service->update($request,$request->id);
        }
    }

    public function delete(int $id){
        return $this->branch_service->delete($id);
    }

}
