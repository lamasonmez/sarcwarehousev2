<?php

namespace App\Http\Controllers\Api\SubBranch;

use App\Http\Controllers\Controller;
use App\Services\SubBranch\ISubBranchService;
use Illuminate\Http\Request;

class SubBranchController extends Controller
{

    private $subbranch_service;

    public function __construct(ISubBranchService $subbranch_service)
    {
        $this->subbranch_service = $subbranch_service;
    }

    public function getSubBranches(){
        $user = \Auth::user('api');
        return $this->subbranch_service->getSubBranchesForUser($user);
    }
    public function getSubBranchesForBranch(int $branch_id){

        return $this->subbranch_service->getSubBranchesForBranch($branch_id);
    }
    public function store(Request $request){
        if($request->id==null){
            return $this->subbranch_service->store($request);
        }
        else{
            return $this->subbranch_service->update($request,$request->id);
        }
    }

    public function delete(int $id){
        return $this->subbranch_service->delete($id);
    }
}
