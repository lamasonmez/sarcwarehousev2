<?php

namespace App\Http\Controllers\Api\MinimumQuantity;

use App\Http\Controllers\Controller;
use App\Services\MinimumQuantity\IMinimumQuantityService;
use Illuminate\Http\Request;

class MinimumQuantitynController extends Controller
{
    private $minimum_quantity_service;
    public function __construct(IMinimumQuantityService $minimum_quantity_service)
    {
        $this->minimum_quantity_service = $minimum_quantity_service;
        $this->middleware('auth');
    }

    public function getMinimumQuantityData(Request $request){
        $user = \Auth::user('api');
        return $this->minimum_quantity_service->getMinimumQuantityData($user)
                                            ->paginate($request->perPage,['*'],'page',$request->page);
    }

    public function store(Request $request){
        if($request->id==null){
            return $this->minimum_quantity_service->store($request);
        }
        else{
            return $this->minimum_quantity_service->update($request,$request->id);
        }
    }

    public function delete(int $id){
        return $this->minimum_quantity_service->delete($id);
    }
}
