<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Services\User\IUserService;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    private $user_service;
    public function __construct(IUserService $user_service)
    {
        $this->user_service = $user_service;
        $this->middleware('auth');
    }

    public function getUsers(){

        return $this->user_service->index();
    }

    public function store(Request $request){
        if($request->id==null){
            return $this->user_service->store($request);
        }
        else{
            return $this->user_service->update($request,$request->id);
        }
    }

    public function delete(int $id){
        return $this->user_service->delete($id);
    }


}
