<?php

namespace App\Http\Controllers\Api\MaterialRequest;

use App\Constants\MaterialStatus;
use App\Http\Controllers\Controller;
use App\Services\MaterialRequest\IMaterialRequestService;
use Illuminate\Http\Request;

class MaterialRequestController extends Controller
{
    //
    private $service;

    public function __construct(IMaterialRequestService $service)
    {
        $this->service = $service;
    }

    public function pendingRequests(){
        return $this->service->getRequests(MaterialStatus::PENDING)->with('user','donor_material')->get();
    }
    public function getRequest($id){
        return $this->service->view($id);
    }
    public function store(Request $request){
        if($request->id){
           return $this->service->update($request,$request->id);
        }
        else{
            $request['status']=MaterialStatus::PENDING;
           return $this->service->store($request);
        }
    }
}
