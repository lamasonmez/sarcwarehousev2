<?php

namespace App\Http\Controllers\Api\Material;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaterialFormRequest;
use App\Services\Material\IMaterialService;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    //
    private $material_service;

    public function __construct(IMaterialService $material_service)
    {
        $this->material_service = $material_service;
    }
    public function getMaterials(){
        return $this->material_service->index();
    }


    public function store(MaterialFormRequest $request){
        if($request->id){
          return  $this->material_service->update($request,$request->id);
        }
        else{
          return $this->material_service->store($request);
        }
    }
}
