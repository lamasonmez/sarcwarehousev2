<?php

namespace App\Http\Controllers\Api\Sector;

use App\Http\Controllers\Controller;
use App\Models\Sector;
use App\Services\Sector\ISectorService;
use Illuminate\Http\Request;

class SectorController extends Controller
{
    //
    private $sector_service;
    public function __construct(ISectorService $sector_service)
    {
        $this->sector_service = $sector_service;
        $this->middleware('auth');
    }
    //TOOD make service
    public function getSectors(){
        return $this->sector_service->index();
    }

    public function store(Request $request){
        if($request->id==null){
            return $this->sector_service->store($request);
        }
        else{
            return $this->sector_service->update($request,$request->id);
        }
    }

    public function delete(int $id){
        return $this->sector_service->delete($id);
    }
}
