<?php

namespace App\Http\Controllers\Api\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\TransactionOut\ITransactionOutService;
use App\Services\TransactionIn\ITransactionInService;
class TransactionController extends Controller
{
    private $transaction_out_service,$transaction_in_service;

    public function __construct(ITransactionOutService $transaction_out_service,
                                ITransactionInService $transaction_in_service)
    {
        $this->transaction_out_service = $transaction_out_service;
        $this->transaction_in_service = $transaction_in_service;
        $this->middleware('auth');
    }

    public function checkWaybill(Request $request){
        $user = \Auth::user('api');

        $in =  $this->transaction_in_service->getTransactionsByFilter($user,$request)->first();
        $out =  $this->transaction_out_service->getTransactionsByFilter($user,$request)->first();

        if($in){
            return response()->json([
                "status"=>"500",
                "transaction"=>"in",
                "message"=>"رقم المذكرة يشير إلى حركة وارد"
            ]);
        }
        elseif($out){

            return response()->json([
                "status"=>"500",
                "transaction"=>"out",
                "message"=>"رقم المذكرة يشير الى حركة صادر"
            ]);
       }
       else{
        return response()->json([
            "status"=>"200",
            "transaction"=>"",
            "message"=>"رقم المذكرة غير مكرر"
        ]);
       }
    }
}
