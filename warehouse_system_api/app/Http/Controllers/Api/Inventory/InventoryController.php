<?php

namespace App\Http\Controllers\Api\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\InventoryExport;
use Excel;
use App\Services\TransactionDetail\ITransactionDetailService;

class InventoryController extends Controller
{
    protected $transactionDetailService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ITransactionDetailService $transactionDetailService)
    {

        $this->transactionDetailService = $transactionDetailService;

    }

    public function getInventory(Request $request)
    {

        $user = \Auth::user('api');
        return $this->transactionDetailService->getInventory($user, $request,false);

    }

    public function filterInventory(Request $request)
    {

        $user = \Auth::user('api');
        return $this->transactionDetailService->filterInventory($user, $request->filters,$request,false);
    }

    public function export($type, Request $request)
    {
        ini_set('max_execution_time', 700);
        $user = \Auth::user('api');
        $filename = 'inventory_' . md5(time()) . '.' . $type;
        $filepath = '/public/exports/' . $filename;
        if ($request->filters) {
            $data = $this->transactionDetailService->filterInventory($user, $request->filters,$request,true);
            \Excel::store(new InventoryExport($data), $filepath, 'local', config('excel.extension_detector.' . $type));
        } else {
            $data = $this->transactionDetailService->getInventory($user, $request,true);
            \Excel::store(new InventoryExport($data), $filepath, 'local', config('excel.extension_detector.' . $type));
        }

        $download_path = asset('storage/exports/' . $filename);
        return response()->json([
            "path" => $download_path
        ]);

    }


}
