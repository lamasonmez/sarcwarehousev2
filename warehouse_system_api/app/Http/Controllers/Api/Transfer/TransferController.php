<?php

namespace App\Http\Controllers\Api\Transfer;

use App\Http\Controllers\Controller;
use App\Services\TransactionType\ITransactionTypeService;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    //
    private $transaction_type;

    public function __construct(ITransactionTypeService $transaction_type)
    {
        $this->transaction_type = $transaction_type;
    }

    public function getTransfers(){
        return $this->transaction_type->index();
    }
}
