<?php

namespace App\Http\Controllers\Api\Unit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Unit;
use App\Services\Unit\IUnitService;

class UnitController extends Controller
{
    private $unit_service;
    public function __construct(IUnitService $unit_service)
    {
        $this->unit_service = $unit_service;
        $this->middleware('auth');
    }
    public function getUnits(){
        return $this->unit_service->index();
    }

    public function store(Request $request){
        if($request->id==null){
            return $this->unit_service->store($request);
        }
        else{
            return $this->unit_service->update($request,$request->id);
        }
    }

    public function delete(int $id){
        return $this->unit_service->delete($id);
    }
}
