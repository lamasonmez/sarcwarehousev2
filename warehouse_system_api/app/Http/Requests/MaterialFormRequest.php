<?php

namespace App\Http\Requests;

use FontLib\Table\Type\name;
use Illuminate\Foundation\Http\FormRequest;

class MaterialFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name.ar'=>'required',
            'name.en'=>'required',
            'sector_id'=>'required'
        ];
    }
    /**
     * Get custom messages for validator errors.
     *
     * @return array
    */
    public function messages()
    {
        return [
            'name.ar.required'=>'الرجاء ادخال اسم المادة بالعربي',
            'name.en.required'=>'الرجاء ادخال اسم المادة بالانكليزي',
            'sector_id.required'=>'الرجاء اختيار القطاع',
        ];
    }
}
