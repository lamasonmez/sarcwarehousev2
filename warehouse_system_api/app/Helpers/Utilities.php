<?php

namespace App\Helpers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;
use ZipArchive;


class Utilities
{

    public static function paginate($items, $perPage = 15, $page = null, $options = [])
        {
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            $items = $items instanceof Collection ? $items : Collection::make($items);
            return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        }

    public static function ZipDownload($folderName)
        {

            $zip = new ZipArchive;
            $path=storage_path('app/public/' . $folderName). '.zip';
            if ($zip->open($path, ZipArchive::CREATE) | ZipArchive::OVERWRITE) {
                $files = File::files(storage_path('app/public/' . $folderName));
                foreach ($files as $key => $value) {
                    $relativeNameZipFile = basename($value);
                    $zip->addFile($value, $relativeNameZipFile);

                }

                $zip->close();

            }
           
            return response()->download($path);


        }


}
