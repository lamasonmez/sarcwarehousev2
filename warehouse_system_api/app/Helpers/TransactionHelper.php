<?php

namespace App\Helpers;


use App\Models\TransactionDetail;


class TransactionHelper
{

    public static function previousClosingBalance($data, $transaction)
    {
        $conditions = [
            ['donor_material_id', '=', $data['donor_material_id']],
            ['warehouse_id', '=', $data['warehouse_id']],
        ];


        if ($transaction == null) {

            return TransactionDetail::query()
                    ->where($conditions)
                    ->orderBy('date', 'desc')
                    ->orderBy('created_at')
                    ->first()->closing_balance
                ?? 0;
        } else {

            return TransactionDetail::query()
                    ->where($conditions)
                    ->where('date', "<=", $transaction->date)
                    ->orderBy('date', 'desc')
                    ->orderBy('created_at')
                    ->first()->closing_balance
                ?? 0;
        }

    }
}
