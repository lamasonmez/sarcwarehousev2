<?php

namespace App\Helpers;


use App\Models\Warehouse;
use App\Models\SubBranch;
use Carbon\Carbon;
use App\User;
use App\Models\Branch;
use Illuminate\Support\Arr;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Collection;

class WarehouseHelper
{
    /** get all warehouses according to use account and role
     * primarly if the user hass viewandy warehouse permission this will return all warehouses
     * if the user belongs to a warehouse this will return just that warehouse
     * else warehouses that belongs to the user's branch/subbranch
     * @param User  $user the authenticated user
     * @param boolean  $withNonActive by default the value is false , set it to true to include non active warehosues too
     * @return array of warehouses
     */
    public static function getWarehouses(User $user,$withNonActive=false){
        $result = [];
        if ($user->can('viewAny', Warehouse::class)) {
            $result= Warehouse::query()
                            ->when($withNonActive,function($q){
                                return $q->where('is_active',false);
                            })
                            ->with(['branchable','warehouse_type']);
            $result = $result->get();
        }
        else
        {
            if($user->warehouse_id!=null){
                $result = Warehouse::query()
                                    ->when($withNonActive,function($q){
                                        return $q->where('is_active',false);
                                    })
                                    ->where('id',$user->warehouse_id)
                                    ->with(['branchable','warehouse_type']);
                $result = $result->get();
            }
            else{
                if($user->branchable!=null){
                    if($user->branchable instanceof Branch){
                       $result= self::getWarehousesForBranch($user->branchable,$withNonActive);
                    }
                    else if($user->branchable instanceof SubBranch){
                        $result = self::getWarehousesForSubBranch($user->branchable,$withNonActive);
                    }
                }
            }
        }
       return $result;
    }

    public static function getWarehousesForBranch(Branch $branch,$withNonActive=false){
        $branch_warehouses =  $branch->warehouses()->when($withNonActive,function($q){return $q->where('is_active',false);})->get();
        $sub_branchs_warehouses = collect();
        $warehouses = collect();
        foreach($branch->subBranches as $sub_branch){
            $sub_branchs_warehouses = $sub_branchs_warehouses->merge(self::getWarehousesForSubBranch($sub_branch,$withNonActive));
        }
        $warehouses = $warehouses->merge($branch_warehouses);
        $warehouses = $warehouses->merge($sub_branchs_warehouses);
        return $warehouses;

    }
    public static function getWarehousesForSubBranch(SubBranch $sub_branch,$withNonActive=false){
        return $sub_branch->warehouses()->when($withNonActive,function($q){return $q->where('is_active',false);})->get();
    }

}
