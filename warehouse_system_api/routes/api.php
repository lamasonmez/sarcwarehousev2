<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['cors', 'json.response']], function () {
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('forgot-password');
});

Route::group(['middleware' => ['auth:api', 'cors', 'json.response']], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('user.logout');
    Route::get('/token/validate', 'Auth\AuthenticationController@validateToken')->name('passport.token.validate');

    Route::get('/statistics/materials/count', 'TransactionDetail\StatisticsController@getDonorMaterialsCount');
    Route::get('/statistics/total_in/count', 'TransactionDetail\StatisticsController@getTotalInCount');
    Route::get('/statistics/total_out/count', 'TransactionDetail\StatisticsController@getTotalOutCount');

    Route::get('/inventory', 'Inventory\InventoryController@getInventory');
     Route::post('/inventory/filter', 'Inventory\InventoryController@filterInventory');

    Route::get('/inventory/material_transactions/{donor_material_id}', 'DonorMaterial\DonorMaterialController@getTransactionsForDM');
    Route::post('/inventory/material_transactions/filter/{donor_material_id}', 'DonorMaterial\DonorMaterialController@filterTransactionsForDM');

    Route::post('/transactions/in/{page?}', 'TransactionIn\TransactionInController@getTransactionIns');
    Route::get('/transactions/in/{id}', 'TransactionIn\TransactionInController@getTransactionInData');
    Route::get('/transactions_in/details/{id}', 'TransactionIn\TransactionInController@getTransactionInDetails');
    Route::post('/transactions_in/store', 'TransactionIn\TransactionInController@store');
    Route::get('/transactions_in/delete/{id}', 'TransactionIn\TransactionInController@delete');

    Route::post('/transactions/out', 'TransactionOut\TransactionOutController@getTransactionOuts');
    Route::get('/transactions/out/{id}', 'TransactionOut\TransactionOutController@getTransactionOutData');
    Route::get('/transactions_out/details/{id}', 'TransactionOut\TransactionOutController@getTransactionOutDetails');
    Route::post('/transactions_out/store', 'TransactionOut\TransactionOutController@store');
    Route::get('/transactions_out/delete/{id}', 'TransactionOut\TransactionOutController@delete');


    Route::get('/transaction_details/{id}', 'TransactionDetail\TransactionDetailController@getTransactionDetail');


    Route::post('/transaction_details/store', 'TransactionDetail\TransactionDetailController@store');
    Route::get('/transaction_details/delete/{id}', 'TransactionDetail\TransactionDetailController@delete');

    Route::post('/transactions/check-waybill', 'Transaction\TransactionController@checkWaybill');

    Route::get('/media/download/{media}', 'DownloadMediaController@show');

    Route::get('/warehouses', 'Warehouse\WarehouseController@getWarehouses');
    Route::get('/warehouses/{type}/{id}', 'Warehouse\WarehouseController@getWarehousesForBranchable');
    Route::post('warehouse-report', 'Warehouse\WarehouseReportController@getReport');

    Route::get('/donors', 'Donor\DonorController@getDonors');
    Route::post('/donors/store', 'Donor\DonorController@store');
    Route::get('/donors/delete/{id}', 'Donor\DonorController@delete');

    Route::get('/donor/materials', 'DonorMaterial\DonorMaterialController@getMaterialsDonorsAll');
    Route::get('/donor/materials/{donor_id}', 'DonorMaterial\DonorMaterialController@getMaterialsForDonor');
    /*TODO:lamakhamis*/
    Route::get('/donor/getWarehouseDonor', 'DonorMaterial\DonorMaterialController@getWarehouseDonor');


    Route::get('/donor_materials/{donor_material_id}', 'DonorMaterial\DonorMaterialController@getDonorMaterial');
    Route::post('/donor_materials/store', 'DonorMaterial\DonorMaterialController@store');
    Route::get('/donor_materials/delete/{id}', 'DonorMaterial\DonorMaterialController@delete');

    Route::get('/units', 'Unit\UnitController@getUnits');
    Route::post('/units/store', 'Unit\UnitController@store');
    Route::get('/units/delete/{id}', 'Unit\UnitController@delete');

    Route::get('/ctns/{donor_material_id?}', 'Ctn\CtnController@getCtns');

    Route::get('/sectors', 'Sector\SectorController@getSectors');
    Route::post('/sectors/store', 'Sector\SectorController@store');
    Route::get('/sectors/delete/{id}', 'Sector\SectorController@delete');


    Route::post('/minimum_quantities/{page?}', 'MinimumQuantity\MinimumQuantitynController@getMinimumQuantityData');
    Route::post('/miniumu_quantities/store', 'MinimumQuantity\MinimumQuantitynController@store');
    Route::get('/miniumu_quantities/delete/{id}', 'MinimumQuantity\MinimumQuantitynController@delete');

    Route::get('/branches', 'Branch\BranchController@getBranches');
    Route::post('/branches/store', 'Branch\BranchController@store');
    Route::get('/branches/delete/{id}', 'Branch\BranchController@delete');

    Route::get('/sub-branches', 'SubBranch\SubBranchController@getSubBranches');
    Route::get('/sub-branches/{branch_id}', 'SubBranch\SubBranchController@getSubBranchesForBranch');
    Route::post('/sub-branches/store', 'SubBranch\SubBranchController@store');
    Route::get('/sub-branches/delete/{id}', 'SubBranch\SubBranchController@delete');

    Route::get('/transfers', 'Transfer\TransferController@getTransfers');

    Route::get('/materials', 'Material\MaterialController@getMaterials');
    Route::post('/materials/store', 'Material\MaterialController@store');

    Route::get('/material_request/pending', 'MaterialRequest\MaterialRequestController@pendingRequests');
    Route::get('/material_request/{id}', 'MaterialRequest\MaterialRequestController@getRequest');
    Route::post('/material_request/store', 'MaterialRequest\MaterialRequestController@store');

    Route::get('/notifications/minimum_quantities', 'Notification\NotificationController@getMinimumQuantity');
    Route::get('/notifications/pending_transactions', 'Notification\NotificationController@getPendingTransactions');

    Route::get('/roles', 'Role\RoleController@getRoles');

    Route::get('/users', 'User\UserController@getUsers');
    Route::post('/users/store', 'User\UserController@store');
    Route::get('/users/delete/{id}', 'User\UserController@delete');


});
//this group dosn't required json middleware
Route::group(['middleware' => ['auth:api']], function () {
    Route::post('/exports/inventory/{type}', "Inventory\InventoryController@export");
    Route::get('/exports/material_transactions/{donor_material_id}/{type}', 'DonorMaterial\DonorMaterialController@exportMaterialTransactions');
    Route::post('/donor/exportDonorMaterial/{donor_id}', 'DonorMaterial\DonorMaterialController@exportDonorMaterial');

});

//Route::post('/donor/exportDonorMaterial/{donor_id}', 'DonorMaterial\DonorMaterialController@exportDonorMaterial');
Route::post('/exports/warehouse-report/{type}', "Warehouse\WarehouseReportController@export");

Route::get('/testreport', 'Warehouse\WarehouseReportController@test');

Route::get('/migratedb',function(){
	/* php artisan migrate */

    \Artisan::call('migrate');

    dd(" Migrate Done");
});



Route::get('/fixdata',function(){
    ini_set('max_execution_time', 1000);
    $not_solved = [];
    $transaction_details = \App\Models\TransactionDetail::all();
    foreach($transaction_details as $key=>$item){
        if($item->date && $item->transactionable->date){
            $item->date = $item->transactionable->date;
            $item->save();
        }
        else{
            array_push($not_solved ,$item->id);
        }
    }

    dd('Done Fixing data',$not_solved);
});


