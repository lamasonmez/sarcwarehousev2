<?php

use App\Models\TransactionDetail;
use App\Models\TransactionIn;
use App\Models\TransactionOut;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/fix-data',function(){
    $dataToFix = TransactionDetail::whereNull('date')->get();
    dd($dataToFix[count($dataToFix)-1]);
    foreach($dataToFix as $item){
        if($item->transactionable_type=='App\\Models\\TransactionOut');
        {
            $transaction = TransactionOut::where('id',$item->transactionable_id)->first();
            dd('out',$transaction,$item->transactionable_id);
        }
        if($item->transactionable_type =='App\\Models\\TransactionIn'){
            $transaction = TransactionIn::where('id',$item->transactionable_id)->first();
            dd('in',$transaction);
        }
        dd($item->transactionable);
    }
});
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
